import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity("DATABASECHANGELOG")
export class DATABASECHANGELOG extends BaseEntity {

    @PrimaryGeneratedColumn({
        name: "ID",
        type: "int"
        })
    public ID: string;

    @Column("varchar", {
        name: "AUTHOR",
        nullable: false
        })
    public AUTHOR: string;

    @Column("varchar", {
        name: "FILENAME",
        nullable: false
        })
    public FILENAME: string;

    @Column("datetime", {
        name: "DATEEXECUTED",
        nullable: false
        })
    public DATEEXECUTED: Date;

    @Column("int", {
        name: "ORDEREXECUTED",
        nullable: false
        })
    public ORDEREXECUTED: number;

    @Column("varchar", {
        length: 10,
        name: "EXECTYPE",
        nullable: false
        })
    public EXECTYPE: string;

    @Column("varchar", {
        length: 35,
        name: "MD5SUM",
        nullable: true
        })
    public MD5SUM: string | null;

    @Column("varchar", {
        name: "DESCRIPTION",
        nullable: true
        })
    public DESCRIPTION: string | null;

    @Column("varchar", {
        name: "COMMENTS",
        nullable: true
        })
    public COMMENTS: string | null;

    @Column("varchar", {
        name: "TAG",
        nullable: true
        })
    public TAG: string | null;

    @Column("varchar", {
        length: 20,
        name: "LIQUIBASE",
        nullable: true
        })
    public LIQUIBASE: string | null;

    @Column("varchar", {
        name: "CONTEXTS",
        nullable: true
        })
    public CONTEXTS: string | null;

    @Column("varchar", {
        name: "LABELS",
        nullable: true
        })
    public LABELS: string | null;

    @Column("varchar", {
        length: 10,
        name: "DEPLOYMENT_ID",
        nullable: true
        })
    public DEPLOYMENT_ID: string | null;

    constructor(init?: Partial<DATABASECHANGELOG>) {
        super();
        Object.assign(this, init);
    }

}
