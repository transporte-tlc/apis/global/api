import {BaseEntity, Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import { BoolBitTransformer } from "../../../database/utils/BoolBitTransformer";
import { HandedOvers } from "../../../models";
import {DatabaseDeliveries} from "./DatabaseDeliveries";
import {DatabaseLocations} from "./DatabaseLocations";

@Entity("HandedOvers")
@Index("FK_HandedOvers_Location", ["idLocation"])
@Index("FK_HandedOvers_Delivery", ["idDelivery"])
export class DatabaseHandedOvers extends BaseEntity {

    @PrimaryGeneratedColumn({
        name: "id",
        type: "int"
        })
    public id: number;

    @ManyToOne((type) => DatabaseDeliveries, (deliveries) => deliveries.handedOvers
        , {  nullable: false, onDelete: "RESTRICT", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "idDelivery"})
    public idDelivery: DatabaseDeliveries | null;

    @ManyToOne((type) => DatabaseLocations, (locations) => locations.handedOvers
        , {  nullable: false, onDelete: "RESTRICT", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "idLocation"})
    public idLocation: DatabaseLocations | null;

    @Column("datetime", {
        name: "IssuedAt",
        nullable: false
        })
    public IssuedAt: Date;

    @Column("varchar", {
        length: 50,
        name: "IssuedTo",
        nullable: true
        })
    public IssuedTo: string | null;

    @Column("int", {
        name: "Quantity",
        nullable: true
        })
    public Quantity: number | null;

    @Column("float", {
        name: "Weight",
        nullable: true
        })
    public Weight: number | null;

    @Column("float", {
        name: "Volume",
        nullable: true
        })
    public Volume: number | null;

    @Column("float", {
        name: "Value",
        nullable: true
        })
    public Value: number | null;

    @Column("varchar", {
        name: "Comment",
        nullable: true
        })
    public Comment: string | null;

    @Column("bit", {
        default: () => "'b'0''",
        name: "deleted",
        nullable: false,
        transformer: new BoolBitTransformer()
        })
    public deleted: boolean;

    @Column("timestamp", {
        default: () => "CURRENT_TIMESTAMP",
        name: "createdAt",
        nullable: false
        })
    public createdAt: Date;

    @Column("datetime", {
        name: "updatedAt",
        nullable: true
        })
    public updatedAt: Date | null;

    constructor(init?: Partial<DatabaseHandedOvers>, model?: HandedOvers) {
        super();
        if (init) {
            Object.assign(this, init);
        } else if (model) {
            this.Comment = model.comment;
            this.IssuedAt = model.issuedAt;
            this.IssuedTo = model.issuedTo;
            this.Quantity = model.quantity;
            this.Value = model.value;
            this.Volume = model.volume;
            this.Weight = model.weight;
            this.deleted = model.deleted;
            this.id = model.id;
            if (model.delivery) {
                this.idDelivery =  new DatabaseDeliveries(null, model.delivery);
            } else if (model.idDelivery) {
                this.idDelivery = new DatabaseDeliveries({ id: model.idDelivery });
            }
            if (model.location) {
                this.idLocation =  new DatabaseLocations(null, model.location);
            } else if (model.idLocation) {
                this.idLocation = new DatabaseLocations({ id: model.idLocation });
            }
        }
    }

}
