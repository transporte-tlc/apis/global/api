import { DatabaseAccountEmails } from "./DatabaseAccountEmails";
import { DatabaseAccounts } from "./DatabaseAccounts";
import { DatabaseCarriers } from "./DatabaseCarriers";
import { DatabaseDeliveries } from "./DatabaseDeliveries";
import { DatabaseHandedOvers } from "./DatabaseHandedOvers";
import { DatabaseJourneyDeliveries } from "./DatabaseJourneyDeliveries";
import { DatabaseJourneys } from "./DatabaseJourneys";
import { DatabaseLocations } from "./DatabaseLocations";
import { DatabaseMemberships } from "./DatabaseMemberships";
import { DatabaseUsers } from "./DatabaseUsers";

export * from "./DatabaseAccountEmails";
export * from "./DatabaseAccounts";
export * from "./DatabaseCarriers";
export * from "./DatabaseDeliveries";
export * from "./DatabaseHandedOvers";
export * from "./DatabaseJourneyDeliveries";
export * from "./DatabaseJourneys";
export * from "./DatabaseLocations";
export * from "./DatabaseMemberships";
export * from "./DatabaseUsers";

export let typeMap: {[index: string]: any} = {
    DatabaseAccountEmails,
    DatabaseAccounts,
    DatabaseCarriers,
    DatabaseDeliveries,
    DatabaseHandedOvers,
    DatabaseJourneyDeliveries,
    DatabaseJourneys,
    DatabaseLocations,
    DatabaseMemberships,
    DatabaseUsers
};
