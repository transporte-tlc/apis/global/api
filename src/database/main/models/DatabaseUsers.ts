import * as bcrypt from "bcrypt";
import {BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import { BoolBitTransformer } from "../../../database/utils/BoolBitTransformer";
import { Users } from "../../../models";
import {DatabaseMemberships} from "./DatabaseMemberships";

@Entity("Users")
export class DatabaseUsers extends BaseEntity {

    @PrimaryGeneratedColumn({
        name: "id",
        type: "int"
        })
    public id: number;

    @Column("varchar", {
        length: 50,
        name: "Name",
        nullable: false
        })
    public Name: string;

    @Column("varchar", {
        length: 50,
        name: "Identification",
        nullable: true
        })
    public Identification: string | null;

    @Column("varchar", {
        length: 50,
        name: "Email",
        nullable: false
        })
    public Email: string;

    @Column("bit", {
        default: () => "'b'0''",
        name: "verifiedEmail",
        nullable: false,
        transformer: new BoolBitTransformer()
        })
    public verifiedEmail: boolean;

    @Column("varchar", {
        length: 50,
        name: "PasswordSalt",
        nullable: true
        })
    public PasswordSalt: string | null;

    @Column("varchar", {
        name: "PasswordHash",
        nullable: true
        })
    public PasswordHash: string | null;

    @Column("bit", {
        default: () => "'b'0''",
        name: "resetPassword",
        nullable: false,
        transformer: new BoolBitTransformer()
        })
    public resetPassword: boolean;

    @Column("varchar", {
        length: 50,
        name: "Type",
        nullable: false
        })
    public Type: string;

    @Column("bit", {
        default: () => "'b'0''",
        name: "deleted",
        nullable: false,
        transformer: new BoolBitTransformer()
        })
    public deleted: boolean;

    @Column("timestamp", {
        default: () => "CURRENT_TIMESTAMP",
        name: "createdAt",
        nullable: false
        })
    public createdAt: Date;

    @Column("datetime", {
        name: "updatedAt",
        nullable: true
        })
    public updatedAt: Date | null;

    @OneToMany((type) => DatabaseMemberships, (memberships) => memberships.idUser
        , { onDelete: "RESTRICT" , onUpdate: "RESTRICT" })
    public memberships: Array<DatabaseMemberships>;

    constructor(init?: Partial<DatabaseUsers>, model?: Partial<Users>) {
        super();
        if (init) {
            Object.assign(this, init);
        } else if (model) {
            this.Email = model.email;
            this.Identification = model.identification;
            this.Name = model.name;
            if (model.role) {
                this.Type = model.role.toString();
            }
            this.id = model.id;
            this.verifiedEmail = model.emailVerified;
            this.resetPassword = model.passwordReset;
            this.deleted = model.deleted;
            if (model.password) {
                this.PasswordSalt = bcrypt.genSaltSync(10);
                this.PasswordHash = bcrypt.hashSync(model.password, this.PasswordSalt);
            }
            if (model.memberships) {
                this.memberships = [];
                for (const membership of model.memberships) {
                    this.memberships.push(new DatabaseMemberships(null, membership));
                }
            }
        }
    }

    public isSamePassword(password: string) {
        return bcrypt.compareSync(password, this.PasswordHash);
    }
}
