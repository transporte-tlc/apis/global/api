import {BaseEntity, Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import { BoolBitTransformer } from "../../../database/utils/BoolBitTransformer";
import { AccountEmails } from "../../../models";
import {DatabaseAccounts} from "./DatabaseAccounts";

@Entity("AccountEmails")
@Index("FK_AccountEmails_Account", ["idAccount"])
export class DatabaseAccountEmails extends BaseEntity {

    @PrimaryGeneratedColumn({
        name: "id",
        type: "int"
        })
    public id: number;

    @ManyToOne((type) => DatabaseAccounts, (accounts) => accounts.accountEmails
        , {  nullable: false, onDelete: "RESTRICT", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "idAccount"})
    public idAccount: DatabaseAccounts | null;

    @Column("varchar", {
        length: 50,
        name: "Name",
        nullable: false
        })
    public Name: string;

    @Column("varchar", {
        name: "Email",
        nullable: false
        })
    public Email: string;

    @Column("bit", {
        default: () => "'b'0''",
        name: "deleted",
        nullable: false,
        transformer: new BoolBitTransformer()
        })
    public deleted: boolean;

    @Column("timestamp", {
        default: () => "CURRENT_TIMESTAMP",
        name: "createdAt",
        nullable: false
        })
    public createdAt: Date;

    @Column("datetime", {
        name: "updatedAt",
        nullable: true
        })
    public updatedAt: Date | null;

    constructor(init?: Partial<DatabaseAccountEmails>, model?: AccountEmails) {
        super();
        if (init) {
            Object.assign(this, init);
        } else if (model) {
            this.Email = model.email;
            this.Name = model.name;
            this.id = model.id;
            if (model.account) {
                this.idAccount = new DatabaseAccounts(null, model.account);
            } else {
                this.idAccount = new DatabaseAccounts({ id: model.idAccount });
            }
            this.deleted = model.deleted;
        }
    }

}
