import {BaseEntity, Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import { BoolBitTransformer } from "../../../database/utils/BoolBitTransformer";
import { Deliveries } from "../../../models";
import {DatabaseAccounts} from "./DatabaseAccounts";
import {DatabaseHandedOvers} from "./DatabaseHandedOvers";
import {DatabaseJourneyDeliveries} from "./DatabaseJourneyDeliveries";
import {DatabaseLocations} from "./DatabaseLocations";

@Entity("Deliveries")
@Index("FK_Deliveries_Location", ["idLocation"])
@Index("FK_Deliveries_Account", ["idAccount"])
export class DatabaseDeliveries extends BaseEntity {

    @PrimaryGeneratedColumn({
        name: "id",
        type: "int"
        })
    public id: number;

    @ManyToOne((type) => DatabaseAccounts, (accounts) => accounts.deliveries
        , {  nullable: false, onDelete: "RESTRICT", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "idAccount"})
    public idAccount: DatabaseAccounts | null;

    @ManyToOne((type) => DatabaseLocations, (locations) => locations.deliveries
        , {  nullable: false, onDelete: "RESTRICT", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "idLocation"})
    public idLocation: DatabaseLocations | null;

    @Column("datetime", {
        name: "IssuedAt",
        nullable: false
        })
    public IssuedAt: Date;

    @Column("varchar", {
        length: 50,
        name: "IssuedBy",
        nullable: true
        })
    public IssuedBy: string | null;

    @Column("varchar", {
        length: 50,
        name: "Reference",
        nullable: true
        })
    public Reference: string | null;

    @Column("int", {
        name: "Quantity",
        nullable: true
        })
    public Quantity: number | null;

    @Column("float", {
        name: "Weight",
        nullable: true
        })
    public Weight: number | null;

    @Column("float", {
        name: "Volume",
        nullable: true
        })
    public Volume: number | null;

    @Column("float", {
        name: "Value",
        nullable: true
        })
    public Value: number | null;

    @Column("varchar", {
        name: "Comment",
        nullable: true
        })
    public Comment: string | null;

    @Column("bit", {
        default: () => "'b'0''",
        name: "deleted",
        nullable: false,
        transformer: new BoolBitTransformer()
        })
    public deleted: boolean;

    @Column("timestamp", {
        default: () => "CURRENT_TIMESTAMP",
        name: "createdAt",
        nullable: false
        })
    public createdAt: Date;

    @Column("datetime", {
        name: "updatedAt",
        nullable: false
        })
    public updatedAt: Date | null;

    @OneToMany((type) => DatabaseHandedOvers, (handedOvers) => handedOvers.idDelivery
        , { onDelete: "RESTRICT" , onUpdate: "RESTRICT" })
    public handedOvers: Array<DatabaseHandedOvers>;

    @OneToMany((type) => DatabaseJourneyDeliveries, (journeyDeliveries) => journeyDeliveries.idDelivery
        , { onDelete: "RESTRICT" , onUpdate: "RESTRICT" })
    public journeyDeliveries: Array<DatabaseJourneyDeliveries>;

    constructor(init?: Partial<DatabaseDeliveries>, model?: Deliveries) {
        super();
        if (init) {
            Object.assign(this, init);
        } else if (model) {
            this.Comment = model.comment;
            this.IssuedAt = new Date(model.issuedAt);
            this.IssuedBy = model.issuedBy;
            this.Quantity = model.quantity;
            this.Reference = model.reference;
            this.Value = model.value;
            this.Volume = model.volume;
            this.Weight = model.weight;
            this.deleted = model.deleted;
            if (model.handedOvers) {
                this.handedOvers = [];
                for ( const handed of model.handedOvers) {
                    this.handedOvers.push(new DatabaseHandedOvers(null, handed));
                }
            }
            if (model.journeys) {
                this.journeyDeliveries = [];
                for (const journey of model.journeys) {
                    this.journeyDeliveries.push(new DatabaseJourneyDeliveries(null, journey));
                }
            }
            if (model.account) {
                this.idAccount = new DatabaseAccounts(null, model.account);
            } else {
                this.idAccount = new DatabaseAccounts({ id: model.idAccount });
            }
            if (model.location) {
                this.idLocation = new DatabaseLocations(null, model.location);
            } else {
                this.idLocation = new DatabaseLocations({ id: model.idLocation });
            }
        }
    }

}
