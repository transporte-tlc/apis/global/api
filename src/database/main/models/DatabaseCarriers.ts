import {BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import { BoolBitTransformer } from "../../../database/utils/BoolBitTransformer";
import { Carriers } from "../../../models";
import {DatabaseJourneys} from "./DatabaseJourneys";

@Entity("Carriers")
export class DatabaseCarriers extends BaseEntity {

    @PrimaryGeneratedColumn({
        name: "id",
        type: "int"
        })
    public id: number;

    @Column("varchar", {
        length: 50,
        name: "Name",
        nullable: false
        })
    public Name: string;

    @Column("bit", {
        default: () => "'b'0''",
        name: "deleted",
        nullable: false,
        transformer: new BoolBitTransformer()
        })
    public deleted: boolean;

    @Column("timestamp", {
        default: () => "CURRENT_TIMESTAMP",
        name: "createdAt",
        nullable: false
        })
    public createdAt: Date;

    @Column("datetime", {
        name: "updatedAt",
        nullable: true
        })
    public updatedAt: Date | null;

    @OneToMany((type) => DatabaseJourneys, (journeys) => journeys.idCarrier
        , { onDelete: "RESTRICT" , onUpdate: "RESTRICT" })
    public journeys: Array<DatabaseJourneys>;

    constructor(init?: Partial<DatabaseCarriers>, model?: Carriers) {
        super();
        if (init) {
            Object.assign(this, init);
        } else if (model) {
            this.Name = model.name;
            this.id = model.id;
            this.deleted = model.deleted;
        }
    }

}
