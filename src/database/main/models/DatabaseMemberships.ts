import {BaseEntity, Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import { BoolBitTransformer } from "../../../database/utils/BoolBitTransformer";
import { Memberships } from "../../../models";
import {DatabaseAccounts} from "./DatabaseAccounts";
import {DatabaseUsers} from "./DatabaseUsers";

@Entity("Memberships")
@Index("FK_Memberships_Account", ["idAccount"])
@Index("FK_Memberships_User", ["idUser"])
export class DatabaseMemberships extends BaseEntity {

    @PrimaryGeneratedColumn({
        name: "id",
        type: "int"
        })
    public id: number;

    @ManyToOne((type) => DatabaseUsers, (users) => users.memberships
        , {  nullable: false, onDelete: "RESTRICT", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "idUser"})
    public idUser: DatabaseUsers | null;

    @ManyToOne((type) => DatabaseAccounts, (accounts) => accounts.memberships
        , {  nullable: false, onDelete: "RESTRICT", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "idAccount"})
    public idAccount: DatabaseAccounts | null;

    @Column("varchar", {
        length: 50,
        name: "Role",
        nullable: false
        })
    public Role: string;

    @Column("bit", {
        default: () => "'b'0''",
        name: "ReceiveEmail",
        nullable: false,
        transformer: new BoolBitTransformer()
        })
    public ReceiveEmail: boolean;

    @Column("bit", {
        default: () => "'b'0''",
        name: "deleted",
        nullable: false,
        transformer: new BoolBitTransformer()
        })
    public deleted: boolean;

    @Column("timestamp", {
        default: () => "CURRENT_TIMESTAMP",
        name: "createdAt",
        nullable: false
        })
    public createdAt: Date;

    @Column("datetime", {
        name: "updatedAt",
        nullable: true
        })
    public updatedAt: Date | null;

    constructor(init?: Partial<DatabaseMemberships>, model?: Memberships) {
        super();
        if (init) {
            Object.assign(this, init);
        } else if (model) {
            this.ReceiveEmail = model.receiveEmail;
            this.Role = model.role.toString();
            this.id = model.id;
            if (model.account) {
                this.idAccount = new DatabaseAccounts(null, model.account);
            } else {
                this.idAccount = new DatabaseAccounts({ id: model.idAccount });
            }
            if (model.idUser) {
                this.idUser = new DatabaseUsers(null, model.user);
            } else {
                this.idUser = new DatabaseUsers({ id: model.idUser });
            }
            this.deleted = model.deleted;
        }
    }

}
