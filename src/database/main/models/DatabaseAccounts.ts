import {BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import { BoolBitTransformer } from "../../../database/utils/BoolBitTransformer";
import { Accounts } from "../../../models";
import {DatabaseAccountEmails} from "./DatabaseAccountEmails";
import {DatabaseDeliveries} from "./DatabaseDeliveries";
import {DatabaseMemberships} from "./DatabaseMemberships";

@Entity("Accounts")
export class DatabaseAccounts extends BaseEntity {

    @PrimaryGeneratedColumn({
        name: "id",
        type: "int"
        })
    public id: number;

    @Column("varchar", {
        length: 50,
        name: "Name",
        nullable: false
        })
    public Name: string;

    @Column("bit", {
        default: () => "'b'0''",
        name: "deleted",
        nullable: false,
        transformer: new BoolBitTransformer()
        })
    public deleted: boolean;

    @Column("timestamp", {
        default: () => "CURRENT_TIMESTAMP",
        name: "createdAt",
        nullable: false
        })
    public createdAt: Date;

    @Column("datetime", {
        name: "updatedAt",
        nullable: true
        })
    public updatedAt: Date | null;

    @OneToMany((type) => DatabaseAccountEmails, (accountEmails) => accountEmails.idAccount
        , { onDelete: "RESTRICT" , onUpdate: "RESTRICT" })
    public accountEmails: Array<DatabaseAccountEmails>;

    @OneToMany((type) => DatabaseDeliveries, (deliveries) => deliveries.idAccount
        , { onDelete: "RESTRICT" , onUpdate: "RESTRICT" })
    public deliveries: Array<DatabaseDeliveries>;

    @OneToMany((type) => DatabaseMemberships, (memberships) => memberships.idAccount
        , { onDelete: "RESTRICT" , onUpdate: "RESTRICT" })
    public memberships: Array<DatabaseMemberships>;

    constructor(init?: Partial<DatabaseAccounts>, model?: Accounts) {
        super();
        if (init) {
            Object.assign(this, init);
        } else if (model) {
            this.Name = model.name;
            this.id = model.id;
            this.deleted = model.deleted;
            if (model.memberships && model.memberships.length) {
                this.memberships = [];
                for (const mem of model.memberships) {
                    this.memberships.push(new DatabaseMemberships(null, mem));
                }
            }
            if (model.accountEmails && model.accountEmails.length) {
                this.accountEmails = [];
                for (const acc of model.accountEmails) {
                    this.accountEmails.push(new DatabaseAccountEmails(null, acc));
                }
            }
        }
    }

}
