import {BaseEntity, Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import { BoolBitTransformer } from "../../../database/utils/BoolBitTransformer";
import { JourneyDeliveries } from "../../../models";
import {DatabaseDeliveries} from "./DatabaseDeliveries";
import {DatabaseJourneys} from "./DatabaseJourneys";

@Entity("JourneyDeliveries")
@Index("FK_JourneyDeliveries_Journey", ["idJourney"])
@Index("FK_JourneyDeliveries_Delivery", ["idDelivery"])
export class DatabaseJourneyDeliveries extends BaseEntity {

    @PrimaryGeneratedColumn({
        name: "id",
        type: "int"
        })
    public id: number;

    @ManyToOne((type) => DatabaseJourneys, (journeys) => journeys.journeyDeliveries
        , {  nullable: false, onDelete: "RESTRICT", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "idJourney"})
    public idJourney: DatabaseJourneys | null;

    @ManyToOne((type) => DatabaseDeliveries, (deliveries) => deliveries.journeyDeliveries
        , {  nullable: false, onDelete: "RESTRICT", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "idDelivery"})
    public idDelivery: DatabaseDeliveries | null;

    @Column("int", {
        name: "Quantity",
        nullable: true
        })
    public Quantity: number | null;

    @Column("float", {
        name: "Weight",
        nullable: true
        })
    public Weight: number | null;

    @Column("float", {
        name: "Volume",
        nullable: true
        })
    public Volume: number | null;

    @Column("float", {
        name: "Value",
        nullable: true
        })
    public Value: number | null;

    @Column("bit", {
        default: () => "'b'0''",
        name: "deleted",
        nullable: false,
        transformer: new BoolBitTransformer()
        })
    public deleted: boolean;

    @Column("timestamp", {
        default: () => "CURRENT_TIMESTAMP",
        name: "createdAt",
        nullable: false
        })
    public createdAt: Date;

    @Column("datetime", {
        name: "updatedAt",
        nullable: true
        })
    public updatedAt: Date | null;

    constructor(init?: Partial<DatabaseJourneyDeliveries>, model?: JourneyDeliveries) {
        super();
        if (init) {
            Object.assign(this, init);
        } else if (model) {
            this.Quantity = model.quantity;
            this.Value = model.value;
            this.Volume = model.volume;
            this.Weight = model.weight;
            this.deleted = model.deleted;
            this.id = model.id;
            if (model.delivery) {
                this.idDelivery = new DatabaseDeliveries(null, model.delivery);
            } else if (model.idDelivery) {
                this.idDelivery = new DatabaseDeliveries({id: model.idDelivery});
            }
            if (model.journey) {
                this.idJourney = new DatabaseJourneys(null, model.journey);
            } else if (model.idJourney) {
                this.idJourney = new DatabaseJourneys({id: model.idJourney});
            }
        }
    }

}
