import {BaseEntity, Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import { BoolBitTransformer } from "../../../database/utils/BoolBitTransformer";
import { Journeys } from "../../../models";
import {DatabaseCarriers} from "./DatabaseCarriers";
import {DatabaseJourneyDeliveries} from "./DatabaseJourneyDeliveries";
import {DatabaseLocations} from "./DatabaseLocations";

@Entity("Journeys")
@Index("FK_Journeys_Carrier", ["idCarrier"])
@Index("FK_Journeys_Location_2", ["idLocationEnd"])
@Index("FK_Journeys_Location_1", ["idLocationStart"])
export class DatabaseJourneys extends BaseEntity {

    @PrimaryGeneratedColumn({
        name: "id",
        type: "int"
        })
    public id: number;

    @ManyToOne((type) => DatabaseCarriers, (carriers) => carriers.journeys
        , { onDelete: "RESTRICT", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "idCarrier"})
    public idCarrier: DatabaseCarriers | null;

    @ManyToOne((type) => DatabaseLocations, (locations) => locations.journeysStart
        , {  nullable: false, onDelete: "RESTRICT", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "idLocationStart"})
    public idLocationStart: DatabaseLocations | null;

    @ManyToOne((type) => DatabaseLocations, (locations) => locations.journeysEnd
        , {  nullable: false, onDelete: "RESTRICT", onUpdate: "RESTRICT" })
    @JoinColumn({ name: "idLocationEnd"})
    public idLocationEnd: DatabaseLocations | null;

    @Column("datetime", {
        name: "StartDate",
        nullable: false
        })
    public StartDate: Date;

    @Column("datetime", {
        name: "EndDate",
        nullable: true
        })
    public EndDate: Date | null;

    @Column("bit", {
        default: () => "'b'0''",
        name: "deleted",
        nullable: false,
        transformer: new BoolBitTransformer()
        })
    public deleted: boolean;

    @Column("timestamp", {
        default: () => "CURRENT_TIMESTAMP",
        name: "createdAt",
        nullable: false
        })
    public createdAt: Date;

    @Column("datetime", {
        name: "updatedAt",
        nullable: true
        })
    public updatedAt: Date | null;

    @OneToMany((type) => DatabaseJourneyDeliveries, (journeyDeliveries) => journeyDeliveries.idJourney
        , { onDelete: "RESTRICT" , onUpdate: "RESTRICT" })
    public journeyDeliveries: Array<DatabaseJourneyDeliveries>;

    constructor(init?: Partial<DatabaseJourneys>, model?: Journeys) {
        super();
        if (init) {
            Object.assign(this, init);
        } else if (model) {
            this.EndDate = new Date(model.endDate);
            this.StartDate = new Date(model.startDate);
            this.deleted = model.deleted;
            this.id = model.id;
            if (model.carrier) {
                this.idCarrier = new DatabaseCarriers(null, model.carrier);
            } else if (model.idCarrier) {
                this.idCarrier = new DatabaseCarriers({id: model.idCarrier });
            }
            if (model.locationEnd) {
                this.idLocationEnd = new DatabaseLocations(null, model.locationEnd);
            } else if (model.idLocationEnd) {
                this.idCarrier = new DatabaseCarriers({id: model.idLocationEnd });
            }
            if (model.locationStart) {
                this.idLocationStart = new DatabaseLocations(null, model.locationStart);
            } else if (model.idCarrier) {
                this.idLocationStart = new DatabaseLocations({id: model.idLocationStart });
            }
            if (model.deliveries) {
                this.journeyDeliveries = [];
                for (const journey of model.deliveries) {
                    this.journeyDeliveries.push(new DatabaseJourneyDeliveries(null, journey));
                }
            }
        }
    }

}
