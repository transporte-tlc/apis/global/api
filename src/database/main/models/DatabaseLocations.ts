import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { BoolBitTransformer } from "../../../database/utils/BoolBitTransformer";
import { Locations } from "../../../models";
import { DatabaseDeliveries } from "./DatabaseDeliveries";
import { DatabaseHandedOvers } from "./DatabaseHandedOvers";
import { DatabaseJourneys } from "./DatabaseJourneys";

@Entity("Locations")
export class DatabaseLocations extends BaseEntity {

    @PrimaryGeneratedColumn({
        name: "id",
        type: "int"
    })
    public id: number;

    @Column("varchar", {
        length: 50,
        name: "Name",
        nullable: false
    })
    public Name: string;

    @Column("bit", {
        default: () => "'b'0''",
        name: "deleted",
        nullable: false,
        transformer: new BoolBitTransformer()
    })
    public deleted: boolean;

    @Column("timestamp", {
        default: () => "CURRENT_TIMESTAMP",
        name: "createdAt",
        nullable: false
    })
    public createdAt: Date;

    @Column("datetime", {
        name: "updatedAt",
        nullable: true
    })
    public updatedAt: Date | null;

    @OneToMany((type) => DatabaseDeliveries, (deliveries) => deliveries.idLocation
        , { onDelete: "RESTRICT", onUpdate: "RESTRICT" })
    public deliveries: Array<DatabaseDeliveries>;

    @OneToMany((type) => DatabaseHandedOvers, (handedOvers) => handedOvers.idLocation
        , { onDelete: "RESTRICT", onUpdate: "RESTRICT" })
    public handedOvers: Array<DatabaseHandedOvers>;

    @OneToMany((type) => DatabaseJourneys, (journeys) => journeys.idLocationStart
        , { onDelete: "RESTRICT", onUpdate: "RESTRICT" })
    public journeysStart: Array<DatabaseJourneys>;

    @OneToMany((type) => DatabaseJourneys, (journeys) => journeys.idLocationEnd
        , { onDelete: "RESTRICT", onUpdate: "RESTRICT" })
    public journeysEnd: Array<DatabaseJourneys>;

    constructor(init?: Partial<DatabaseLocations>, model?: Locations) {
        super();
        if (init) {
            Object.assign(this, init);
        } else if (model) {
            this.Name = model.name;
            this.id = model.id;
            this.deleted = model.deleted;
        }
    }
}
