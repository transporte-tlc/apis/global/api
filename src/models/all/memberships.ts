import { DatabaseMemberships } from "../../database/main/models/DatabaseMemberships";
import { VALID_RESPONSES } from "../../utils/ValidResponses";
import { Accounts } from "./accounts";
import { Users } from "./users";

export class Memberships {

    public static discriminator: string | undefined = undefined;

    public static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            baseName: "id",
            name: "id",
            type: "number"
        },
        {
            baseName: "idUser",
            name: "idUser",
            type: "number"
        },
        {
            baseName: "user",
            name: "user",
            type: "Users"
        },
        {
            baseName: "idAccount",
            name: "idAccount",
            type: "number"
        },
        {
            baseName: "account",
            name: "account",
            type: "Accounts"
        },
        {
            baseName: "role",
            name: "role",
            type: "Memberships.RoleEnum"
        },
        {
            baseName: "receiveEmail",
            name: "receiveEmail",
            type: "boolean"
        }    ];

    public static getAttributeTypeMap() {
        return Memberships.attributeTypeMap;
    }

    public "id"?: number;
    public "idUser"?: number;
    public "user"?: Users;
    public "idAccount"?: number;
    public "account"?: Accounts;
    public "role"?: MembershipsRole;
    public "receiveEmail"?: boolean;
    public "deleted"?: boolean;

    constructor(init?: Partial<Memberships>, model?: DatabaseMemberships) {
        if (init) {
            Object.assign(this, init);
            if (init.user && init.user.id && !init.idUser) {
                this.idUser = this.user.id;
            }
            if (init.account && init.account.id && !init.idAccount) {
                this.idAccount = this.account.id;
            }
            this.user = new Users({ id: this.idUser });
            this.account = new Accounts({ id: this.idAccount });
        } else if (model) {
            this.account = new Accounts(null, model.idAccount);
            if (this.account) {
                this.idAccount = this.account.id;
            }
            this.id = model.id;
            this.user = new Users(null, model.idUser);
            if (this.user) {
                this.idUser = this.user.id;
            }
            this.receiveEmail = model.ReceiveEmail;
            this.role = MembershipsRole[model.Role];
            this.deleted = model.deleted;
        }
    }

    public isValid(): string {
        if (!this.isUserValid()) {
            return VALID_RESPONSES.ERROR.VALIDATION.USER.ID;
        }
        if (!this.isAccountValid()) {
            return VALID_RESPONSES.ERROR.VALIDATION.ACCOUNT.ID;
        }
        if (!this.role) {
            return VALID_RESPONSES.ERROR.VALIDATION.MEMBERSHIP.ROLE;
        }
        return null;
    }

    public isAccountValid(): boolean {
        return this.idAccount || (this.account && this.account.id) ? true : false;
    }

    public isUserValid(): boolean {
        return this.idUser || (this.user && this.user.id) ? true : false;
    }
}
export enum MembershipsRole {
    VIEWER = "VIEWER",
    EDITOR = "EDITOR",
    ADMIN = "ADMIN"
}
