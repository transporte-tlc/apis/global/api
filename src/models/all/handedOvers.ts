import { DatabaseHandedOvers } from "src/database";
import { Deliveries } from "./deliveries";
import { Locations } from "./locations";

export class HandedOvers {

    public static discriminator: string | undefined = undefined;

    public static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            baseName: "id",
            name: "id",
            type: "number"
        },
        {
            baseName: "idDelivery",
            name: "idDelivery",
            type: "number"
        },
        {
            baseName: "delivery",
            name: "delivery",
            type: "Deliveries"
        },
        {
            baseName: "idLocation",
            name: "idLocation",
            type: "number"
        },
        {
            baseName: "location",
            name: "location",
            type: "Locations"
        },
        {
            baseName: "issuedAt",
            name: "issuedAt",
            type: "Date"
        },
        {
            baseName: "issuedTo",
            name: "issuedTo",
            type: "string"
        },
        {
            baseName: "quantity",
            name: "quantity",
            type: "number"
        },
        {
            baseName: "weight",
            name: "weight",
            type: "number"
        },
        {
            baseName: "volume",
            name: "volume",
            type: "number"
        },
        {
            baseName: "value",
            name: "value",
            type: "number"
        },
        {
            baseName: "comment",
            name: "comment",
            type: "string"
        }    ];

        public static getAttributeTypeMap() {
        return HandedOvers.attributeTypeMap;
    }

    public "id"?: number;
    public "idDelivery"?: number;
    public "delivery"?: Deliveries;
    public "idLocation"?: number;
    public "location"?: Locations;
    public "issuedAt"?: Date;
    public "issuedTo"?: string;
    public "quantity"?: number;
    public "weight"?: number;
    public "volume"?: number;
    public "value"?: number;
    public "comment"?: string;
    public "deleted"?: boolean;

    constructor(init?: Partial<HandedOvers>, model?: DatabaseHandedOvers) {
        if (init) {
            Object.assign(this, init);
        } else if (model) {
            this.comment = model.Comment;
            this.deleted = model.deleted;
            if (model.idDelivery) {
                this.delivery = new Deliveries(null, model.idDelivery);
                this.idDelivery = model.idDelivery.id;
            }
            this.id = model.id;
            if (model.idLocation) {
                this.location = new Locations(null, model.idLocation);
                this.idLocation = model.idLocation.id;
            }
            this.issuedAt = model.IssuedAt;
            this.issuedTo = model.IssuedTo;
            this.quantity = model.Quantity;
            this.value = model.Value;
            this.volume = model.Volume;
            this.weight = model.Weight;
        }
    }
}
