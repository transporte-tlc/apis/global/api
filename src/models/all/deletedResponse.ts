export class DeletedResponse {

    public static discriminator: string | undefined = undefined;

    public static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            baseName: "id",
            name: "id",
            type: "number"
        }    ];

    public static getAttributeTypeMap() {
        return DeletedResponse.attributeTypeMap;
    }

    public "id"?: number;

}
