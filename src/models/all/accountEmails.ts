import { DatabaseAccountEmails } from "../../database/main/models/DatabaseAccountEmails";
import { Accounts } from "./accounts";

export class AccountEmails {

    public static discriminator: string | undefined = undefined;

    public static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            baseName: "id",
            name: "id",
            type: "number"
        },
        {
            baseName: "idAccount",
            name: "idAccount",
            type: "number"
        },
        {
            baseName: "account",
            name: "account",
            type: "Accounts"
        },
        {
            baseName: "name",
            name: "name",
            type: "string"
        },
        {
            baseName: "email",
            name: "email",
            type: "string"
        }    ];

    public static getAttributeTypeMap() {
        return AccountEmails.attributeTypeMap;
    }

    public "id"?: number;
    public "idAccount"?: number;
    public "account"?: Accounts;
    public "name"?: string;
    public "email"?: string;
    public "deleted"?: boolean;

    constructor(init?: Partial<AccountEmails>, model?: DatabaseAccountEmails) {
        if (init) {
            Object.assign(this, init);
        } else if (model) {
            this.account = new Accounts(null, model.idAccount);
            if (this.account) {
                this.idAccount = this.account.id;
            }
            this.email = model.Email;
            this.id = model.id;
            this.name = model.Name;
            this.deleted = model.deleted;
        }
    }
}
