import { DatabaseAccounts } from "../../database/main/models/DatabaseAccounts";
import { AccountEmails } from "./accountEmails";
import { Memberships } from "./memberships";

export class Accounts {

    public static discriminator: string | undefined = undefined;

    public static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            baseName: "id",
            name: "id",
            type: "number"
        },
        {
            baseName: "name",
            name: "name",
            type: "string"
        },
        {
            baseName: "memberships",
            name: "memberships",
            type: "Array<Memberships>"
        },
        {
            baseName: "accountEmails",
            name: "accountEmails",
            type: "Array<AccountEmails>"
        }    ];

    public static getAttributeTypeMap() {
        return Accounts.attributeTypeMap;
    }

    public "id"?: number;
    public "name"?: string;
    public "deleted"?: boolean;
    public "memberships"?: Array<Memberships>;
    public "accountEmails"?: Array<AccountEmails>;

    constructor(init?: Partial<Accounts>, model?: DatabaseAccounts) {
        if (init) {
            Object.assign(this, init);
        } else if (model) {
            this.id = model.id;
            this.name = model.Name;
            this.deleted = model.deleted;
            if (model.accountEmails && model.accountEmails.length) {
                this.accountEmails = [];
                for (const acc of model.accountEmails) {
                    this.accountEmails.push(new AccountEmails(null, acc));
                }
            }
            if (model.memberships && model.memberships.length) {
                this.memberships = [];
                for (const mem of model.memberships) {
                    this.memberships.push(new Memberships(null, mem));
                }
            }
        }
    }
}
