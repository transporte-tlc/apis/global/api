import { DatabaseLocations } from "../../database";

export class Locations {

    public static discriminator: string | undefined = undefined;

    public static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            baseName: "id",
            name: "id",
            type: "number"
        },
        {
            baseName: "name",
            name: "name",
            type: "string"
        }    ];

    public static getAttributeTypeMap() {
        return Locations.attributeTypeMap;
    }

    public "id"?: number;
    public "name"?: string;
    public "deleted"?: boolean;

    constructor(init?: Partial<Locations>, model?: DatabaseLocations) {
        if (init) {
            Object.assign(this, init);
        } else if (model) {
            this.id = model.id;
            this.name = model.Name;
            this.deleted = model.deleted;
        }
    }
}
