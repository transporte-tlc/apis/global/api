export class Data {

    public static discriminator: string | undefined = undefined;

    public static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            baseName: "email",
            name: "email",
            type: "string"
        },
        {
            baseName: "urlResponse",
            name: "urlResponse",
            type: "string"
        }    ];

    public static getAttributeTypeMap() {
        return Data.attributeTypeMap;
    }

    public "email": string;
    public "urlResponse": string;
}
