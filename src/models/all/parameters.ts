export enum DeletedParameters {
    ALL = "ALL",
    DELETED = "DELETED",
    "NOT-DELETED" = "NOT-DELETED"
}
