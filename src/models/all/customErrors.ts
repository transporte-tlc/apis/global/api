export class CustomError {

    public static discriminator: string | undefined = undefined;

    public static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            baseName: "code",
            name: "code",
            type: "string"
        },
        {
            baseName: "message",
            name: "message",
            type: "string"
        },
        {
            baseName: "data",
            name: "data",
            type: "string"
        }    ];

    public static getAttributeTypeMap() {
        return CustomError.attributeTypeMap;
    }

    public "code"?: string;
    public "message": string;
    public "data"?: string;

}
