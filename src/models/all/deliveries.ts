import { DatabaseDeliveries } from "../../database";
import { VALID_RESPONSES } from "../../utils/ValidResponses";
import { Accounts } from "./accounts";
import { HandedOvers } from "./handedOvers";
import { JourneyDeliveries } from "./journeyDeliveries";
import { Locations } from "./locations";

export class Deliveries {

    public static discriminator: string | undefined = undefined;

    public static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            baseName: "id",
            name: "id",
            type: "number"
        },
        {
            baseName: "idAccount",
            name: "idAccount",
            type: "number"
        },
        {
            baseName: "account",
            name: "account",
            type: "Accounts"
        },
        {
            baseName: "idLocation",
            name: "idLocation",
            type: "number"
        },
        {
            baseName: "location",
            name: "location",
            type: "Locations"
        },
        {
            baseName: "issuedAt",
            name: "issuedAt",
            type: "string"
        },
        {
            baseName: "issuedBy",
            name: "issuedBy",
            type: "string"
        },
        {
            baseName: "reference",
            name: "reference",
            type: "string"
        },
        {
            baseName: "quantity",
            name: "quantity",
            type: "number"
        },
        {
            baseName: "weight",
            name: "weight",
            type: "number"
        },
        {
            baseName: "volume",
            name: "volume",
            type: "number"
        },
        {
            baseName: "value",
            name: "value",
            type: "number"
        },
        {
            baseName: "comment",
            name: "comment",
            type: "string"
        },
        {
            baseName: "handedOvers",
            name: "handedOvers",
            type: "Array<HandedOvers>"
        },
        {
            baseName: "journeys",
            name: "journeys",
            type: "Array<JourneyDeliveries>"
        }    ];

        public static getAttributeTypeMap() {
        return Deliveries.attributeTypeMap;
    }

    public "id"?: number;
    public "idAccount"?: number;
    public "account"?: Accounts;
    public "idLocation"?: number;
    public "location"?: Locations;
    public "issuedAt"?: string;
    public "issuedBy"?: string;
    public "reference"?: string;
    public "quantity"?: number;
    public "weight"?: number;
    public "volume"?: number;
    public "value"?: number;
    public "comment"?: string;
    public "deleted"?: boolean;
    public "handedOvers"?: Array<HandedOvers>;
    public "journeys"?: Array<JourneyDeliveries>;

    constructor(init?: Partial<Deliveries>, model?: DatabaseDeliveries) {
        if (init) {
            Object.assign(this, init);
            if (init.account && init.account.id && !init.idAccount) {
                this.idAccount = this.account.id;
            }
            if (init.location && init.location.id && !init.idLocation) {
                this.idLocation = this.location.id;
            }
        } else if (model) {
            if (model.idAccount) {
                this.account = new Accounts(null, model.idAccount);
                this.idAccount = model.idAccount.id;
            }
            this.comment = model.Comment;
            this.deleted = model.deleted;
            this.id = model.id;
            if (model.idLocation) {
                this.location = new Locations(null, model.idLocation);
                this.idLocation = model.idLocation.id;
            }
            this.issuedAt = model.IssuedAt.toLocaleDateString();
            this.issuedBy = model.IssuedBy;
            if (model.journeyDeliveries) {
                this.journeys = [];
                for (const journey of model.journeyDeliveries) {
                    this.journeys.push(new JourneyDeliveries(null, journey));
                }
            }
            if (model.handedOvers) {
                this.handedOvers = [];
                for (const handed of model.handedOvers) {
                    this.handedOvers.push(new HandedOvers(null, handed));
                }
            }
            this.quantity = model.Quantity;
            this.reference =  model.Reference;
            this.value = model.Value;
            this.volume = model.Value;
            this.weight = model.Weight;
        }
    }

    public isValid(): string {
        if (!this.isLocationValid()) {
            return VALID_RESPONSES.ERROR.VALIDATION.LOCATION.ID;
        }
        if (!this.isAccountValid()) {
            return VALID_RESPONSES.ERROR.VALIDATION.ACCOUNT.ID;
        }
        if (!this.isAnyValueProvided()) {
            return VALID_RESPONSES.ERROR.VALIDATION.DELIVERY.VALUES;
        }
        if (!this.reference) {
            return VALID_RESPONSES.ERROR.VALIDATION.DELIVERY.REFERENCE;
        }
        return null;
    }

    public isAccountValid(): boolean {
        return this.idAccount || (this.account && this.account.id) ? true : false;
    }

    public isLocationValid(): boolean {
        return this.idLocation || (this.location && this.location.id) ? true : false;
    }

    public isAnyValueProvided(): boolean {
        return !this.quantity && !this.value && !this.volume && !this.weight ? false : true;
    }
}
