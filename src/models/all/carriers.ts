import { DatabaseCarriers } from "../../database";

export class Carriers {

    public static discriminator: string | undefined = undefined;

    public static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            baseName: "id",
            name: "id",
            type: "number"
        },
        {
            baseName: "name",
            name: "name",
            type: "string"
        }    ];

    public static getAttributeTypeMap() {
        return Carriers.attributeTypeMap;
    }

    public "id"?: number;
    public "name"?: string;
    public "deleted"?: boolean;

    constructor(init?: Partial<Carriers>, model?: DatabaseCarriers) {
        if (init) {
            Object.assign(this, init);
        } else if (model) {
            this.id = model.id;
            this.name = model.Name;
            this.deleted = model.deleted;
        }
    }
}
