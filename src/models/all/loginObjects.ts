import { Users } from "./users";

export class LoginObject {

    public static discriminator: string | undefined = undefined;

    public static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            baseName: "user",
            name: "user",
            type: "Users"
        },
        {
            baseName: "token",
            name: "token",
            type: "string"
        },
        {
            baseName: "expDate",
            name: "expDate",
            type: "Date"
        }    ];

    public static getAttributeTypeMap() {
        return LoginObject.attributeTypeMap;
    }

    public "user"?: Users;
    public "token"?: string;
    public "expDate"?: Date;
}
