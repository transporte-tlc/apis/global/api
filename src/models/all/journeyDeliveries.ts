import { DatabaseJourneyDeliveries } from "src/database";
import { Deliveries } from "./deliveries";
import { Journeys } from "./journeys";

export class JourneyDeliveries {

    public static discriminator: string | undefined = undefined;

    public static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            baseName: "id",
            name: "id",
            type: "number"
        },
        {
            baseName: "idJourney",
            name: "idJourney",
            type: "number"
        },
        {
            baseName: "journey",
            name: "journey",
            type: "Journeys"
        },
        {
            baseName: "idDelivery",
            name: "idDelivery",
            type: "number"
        },
        {
            baseName: "delivery",
            name: "delivery",
            type: "Deliveries"
        },
        {
            baseName: "quantity",
            name: "quantity",
            type: "number"
        },
        {
            baseName: "weight",
            name: "weight",
            type: "number"
        },
        {
            baseName: "volume",
            name: "volume",
            type: "number"
        },
        {
            baseName: "value",
            name: "value",
            type: "number"
        }    ];

    public static getAttributeTypeMap() {
        return JourneyDeliveries.attributeTypeMap;
    }

    public "id"?: number;
    public "idJourney"?: number;
    public "journey"?: Journeys;
    public "idDelivery"?: number;
    public "delivery"?: Deliveries;
    public "quantity"?: number;
    public "weight"?: number;
    public "volume"?: number;
    public "value"?: number;
    public "deleted"?: boolean;

    constructor(init?: Partial<JourneyDeliveries>, model?: DatabaseJourneyDeliveries) {
        if (init) {
            Object.assign(this, init);
        } else if (model) {
            this.deleted = model.deleted;
            if (model.idDelivery) {
                this.delivery = new Deliveries(null, model.idDelivery);
                this.idDelivery = model.idDelivery.id;
            }
            this.id = model.id;
            if (model.idJourney) {
                this.journey = new Journeys(null, model.idJourney);
                this.idJourney = model.idJourney.id;
            }
            this.quantity = model.Quantity;
            this.value = model.Value;
            this.volume = model.Volume;
            this.weight = model.Weight;
        }
    }

    public isValid(): string {
        // TODO:
        return null;
    }
}
