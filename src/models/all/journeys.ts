import { DatabaseJourneys } from "../../database";
import { VALID_RESPONSES } from "../../utils/ValidResponses";
import { Carriers } from "./carriers";
import { JourneyDeliveries } from "./journeyDeliveries";
import { Locations } from "./locations";

export class Journeys {

    public static discriminator: string | undefined = undefined;

    public static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            baseName: "id",
            name: "id",
            type: "number"
        },
        {
            baseName: "idCarrier",
            name: "idCarrier",
            type: "number"
        },
        {
            baseName: "carrier",
            name: "carrier",
            type: "Carriers"
        },
        {
            baseName: "idLocationStart",
            name: "idLocationStart",
            type: "number"
        },
        {
            baseName: "locationStart",
            name: "locationStart",
            type: "Locations"
        },
        {
            baseName: "idLocationEnd",
            name: "idLocationEnd",
            type: "number"
        },
        {
            baseName: "locationEnd",
            name: "locationEnd",
            type: "Locations"
        },
        {
            baseName: "startDate",
            name: "startDate",
            type: "string"
        },
        {
            baseName: "endDate",
            name: "endDate",
            type: "string"
        },
        {
            baseName: "deliveries",
            name: "deliveries",
            type: "Array<JourneyDeliveries>"
        }    ];

    public static getAttributeTypeMap() {
        return Journeys.attributeTypeMap;
    }

    public "id"?: number;
    public "idCarrier"?: number;
    public "carrier"?: Carriers;
    public "idLocationStart"?: number;
    public "locationStart"?: Locations;
    public "idLocationEnd"?: number;
    public "locationEnd"?: Locations;
    public "startDate"?: string;
    public "endDate"?: string;
    public "deleted"?: boolean;
    public "deliveries"?: Array<JourneyDeliveries>;

    constructor(init?: Partial<Journeys>, model?: DatabaseJourneys) {
        if (init) {
            Object.assign(this, init);
        } else if (model) {
            if (model.idCarrier) {
                this.carrier = new Carriers(null, model.idCarrier);
                this.idCarrier = model.idCarrier.id;
            }
            this.deleted = model.deleted;
            if (model.journeyDeliveries) {
                this.deliveries = [];
                for (const delivery of model.journeyDeliveries) {
                    this.deliveries.push(new JourneyDeliveries(null, delivery));
                }
            }
            this.endDate = model.EndDate.toLocaleDateString();
            this.id = model.id;
            if (model.idLocationEnd) {
                this.locationEnd = new Locations(null, model.idLocationEnd);
                this.idLocationEnd = model.idLocationEnd.id;
            }
            if (model.idLocationStart) {
                this.locationStart = new Locations(null, model.idLocationStart);
                this.idLocationStart = model.idLocationStart.id;
            }
            this.startDate = model.StartDate.toLocaleDateString();
        }
    }

    public isValid(): string {
        if (!this.isLocationEndValid()) {
            return VALID_RESPONSES.ERROR.VALIDATION.JOURNEY.LOCATION_END;
        }
        if (!this.isLocationStartValid()) {
            return VALID_RESPONSES.ERROR.VALIDATION.JOURNEY.LOCATION_START;
        }
        if (!this.isCarrierValid()) {
            return VALID_RESPONSES.ERROR.VALIDATION.CARRIER.ID;
        }
        const deliveries = this.areDeliveriesValid();
        if (deliveries) {
            return deliveries;
        }
        return null;
    }

    public isLocationEndValid(): boolean {
        return this.idLocationEnd || (this.locationEnd && this.locationEnd.id) ? true : false;
    }

    public isLocationStartValid(): boolean {
        return this.idLocationStart || (this.locationStart && this.locationStart.id) ? true : false;
    }

    public isCarrierValid(): boolean {
        return this.idCarrier || (this.carrier && this.carrier.id) ? true : false;
    }

    public areDeliveriesValid(): string {
        let response;
        if (this.deliveries) {
            for (const delivery of this.deliveries) {
                response = delivery.isValid();
                if (response) {
                    return response;
                }
            }
        }
        return null;
    }
}
