export class Metadata {

    public static discriminator: string | undefined = undefined;

    public static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            baseName: "first",
            name: "first",
            type: "number"
        },
        {
            baseName: "prev",
            name: "prev",
            type: "number"
        },
        {
            baseName: "self",
            name: "self",
            type: "number"
        },
        {
            baseName: "next",
            name: "next",
            type: "number"
        },
        {
            baseName: "last",
            name: "last",
            type: "number"
        }    ];

    public static getAttributeTypeMap() {
        return Metadata.attributeTypeMap;
    }

    public "first"?: number;
    public "prev"?: number;
    public "self"?: number;
    public "next"?: number;
    public "last"?: number;
}
