import { DatabaseUsers } from "../../database/main/models/DatabaseUsers";
import { VALID_RESPONSES } from "../../utils/ValidResponses";
import { Memberships } from "./memberships";

export class Users {

    public static discriminator: string | undefined = undefined;

    public static attributeTypeMap: Array<{name: string, baseName: string, type: string}> = [
        {
            baseName: "id",
            name: "id",
            type: "number"
        },
        {
            baseName: "name",
            name: "name",
            type: "string"
        },
        {
            baseName: "identification",
            name: "identification",
            type: "string"
        },
        {
            baseName: "email",
            name: "email",
            type: "string"
        },
        {
            baseName: "emailVerified",
            name: "emailVerified",
            type: "boolean"
        },
        {
            baseName: "password",
            name: "password",
            type: "string"
        },
        {
            baseName: "passwordReset",
            name: "passwordReset",
            type: "boolean"
        },
        {
            baseName: "role",
            name: "role",
            type: "Users.RoleEnum"
        },
        {
            baseName: "memberships",
            name: "memberships",
            type: "Array<Memberships>"
        }    ];

    public static getAttributeTypeMap() {
        return Users.attributeTypeMap;
    }

    public static hasValidPermissions(userRole: UsersRole, newUserRole: UsersRole): boolean {
        switch (userRole) {
            case UsersRole.EDITOR:
                return newUserRole !== UsersRole.ADMIN;
            case UsersRole.ADMIN:
                return true;
            case UsersRole.VIEWER:
            case UsersRole.COMMON:
            default:
                return newUserRole === UsersRole.COMMON;
        }
    }

    public "id"?: number;
    public "name"?: string;
    public "identification"?: string;
    public "email"?: string;
    public "emailVerified"?: boolean;
    public "password"?: string;
    public "passwordReset"?: boolean;
    public "role"?: UsersRole;
    public "deleted"?: boolean;
    public "memberships"?: Array<Memberships>;

    constructor(init?: Partial<Users>, model?: DatabaseUsers) {
        if (init) {
            Object.assign(this, init);
        } else if (model) {
            this.email = model.Email;
            this.identification = model.Identification;
            this.name = model.Name;
            this.role = UsersRole[model.Type];
            this.id = model.id;
            this.emailVerified = model.verifiedEmail;
            this.passwordReset = model.resetPassword;
            this.deleted = model.deleted;
            if (model.memberships) {
                this.memberships = [];
                for (const membership of model.memberships) {
                    this.memberships.push(new Memberships(null, membership));
                }
            }
        }
    }

    public isPasswordValid(): boolean {
        // Minimum eight characters, at least one letter and one number:
        // const regex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
        // Minimum eight characters, at least one letter, one number and one special character:
        const regex = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
        // Minimum eight characters, at least one uppercase letter, one lowercase letter and one number:
        // const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
        // Minimum eight, at least one uppercase letter, one lowercase letter, one number and one special character:
        // const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
        // Minimum eight and maximum 10, one uppercase, one lowercase, one number and one special character:
        // const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,10}$/;
        return this.password && regex.test(this.password);
    }

    public isEmailValid(): boolean {
        // tslint:disable-next-line: max-line-length
        const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return this.email && regex.test(this.email);
    }

    public isValid(checkPassword = true): string {
        if (!this.isEmailValid()) {
            return VALID_RESPONSES.ERROR.VALIDATION.USER.EMAIL;
        }
        if (checkPassword && !this.isPasswordValid()) {
            return VALID_RESPONSES.ERROR.VALIDATION.USER.PASSWORD;
        }
        if (!this.name || !this.name.length) {
            return VALID_RESPONSES.ERROR.VALIDATION.NAME;
        }
        if (!this.role) {
            return VALID_RESPONSES.ERROR.VALIDATION.USER.ROLE;
        }
        return null;
    }
}

export enum UsersRole {
    COMMON = "COMMON",
    VIEWER = "VIEWER",
    EDITOR = "EDITOR",
    ADMIN = "ADMIN"
}
