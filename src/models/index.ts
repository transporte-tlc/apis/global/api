import { AccountEmails } from "./all/accountEmails";
import { Accounts } from "./all/accounts";
import { Carriers } from "./all/carriers";
import { CustomError } from "./all/customErrors";
import { Data } from "./all/data";
import { DeletedResponse } from "./all/deletedResponse";
import { Deliveries } from "./all/deliveries";
import { HandedOvers } from "./all/handedOvers";
import { JourneyDeliveries } from "./all/journeyDeliveries";
import { Journeys } from "./all/journeys";
import { Locations } from "./all/locations";
import { LoginObject } from "./all/loginObjects";
import { Memberships, MembershipsRole } from "./all/memberships";
import { Metadata } from "./all/metadata";
import { Users, UsersRole } from "./all/users";

export * from "./all/accountEmails";
export * from "./all/accounts";
export * from "./all/carriers";
export * from "./all/deletedResponse";
export * from "./all/deliveries";
export * from "./all/handedOvers";
export * from "./all/journeyDeliveries";
export * from "./all/journeys";
export * from "./all/locations";
export * from "./all/loginObjects";
export * from "./all/memberships";
export * from "./all/customErrors";
export * from "./all/users";

export let enumsMap: { [index: string]: any } = {
    "MembershipsRole": MembershipsRole,
    "Users.RoleEnum": UsersRole
};

export let typeMap: { [index: string]: any } = {
    AccountEmails,
    Accounts,
    Carriers,
    CustomError,
    Data,
    DeletedResponse,
    Deliveries,
    HandedOvers,
    JourneyDeliveries,
    Journeys,
    Locations,
    LoginObject,
    Memberships,
    Metadata,
    Users
};
