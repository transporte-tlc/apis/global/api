import { NextFunction, Response } from "express";
import { Swagger12Request, SwaggerRequestParameters } from "swagger-tools";
import { Users } from "../models/all/users";

export interface CustomNext extends NextFunction {
}

export interface CustomResponse extends Response {
}

export interface CustomRequest extends Swagger12Request {
    user: CustomUserInfo;
}

export interface CustomUserInfo {
    data: Users;
}
