import { SwaggerRequestParameters } from "swagger-tools";
import { BaseEntity, FindConditions, FindManyOptions, FindOneOptions } from "typeorm";
import { DatabaseAccountEmails, DatabaseAccounts, DatabaseCarriers, DatabaseDeliveries
    , DatabaseHandedOvers, DatabaseJourneyDeliveries, DatabaseJourneys, DatabaseLocations
    , DatabaseMemberships, DatabaseUsers
} from "../database";
import { Memberships, Users, UsersRole } from "../models";
import { Metadata } from "../models/all/metadata";
import { DeletedParameters } from "../models/all/parameters";
import { CustomRequest, CustomResponse } from "./customsHandlers";
import { JwtUtils } from "./jwt-util";
import { LoggerUtility } from "./LoggerUtility";
import { VALID_RESPONSES } from "./ValidResponses";
import { ResponsePayload } from "./writer";

export class Utilities {
    public static checkVariableNotNull(name: string, parameters: SwaggerRequestParameters, res?: CustomResponse): any {
        let error: boolean = true;
        let value = null;
        if (parameters[name]) {
            value = parameters[name].value;
            error = false;
            // LoggerUtility..info(name,"=",value);
        }
        if (res && error) {
            this.sendError(res, name);
        }
        return value;
    }

    public static checkVariablesNotNull(
            names: Array<string>, parameters: SwaggerRequestParameters, res?: CustomResponse): any {
        const responses = {};
        for (const name of names) {
            const parameter = this.checkVariableNotNull(name, parameters, res);
            if (!parameter) {
                return null;
            }
            responses[name] = parameter;
        }
        return responses;
    }
    public static checkId(parameters: SwaggerRequestParameters, res?: CustomResponse): number | null {
        return this.checkVariableNotNull("id", parameters, res);
    }

    public static checkDeleted(parameters: SwaggerRequestParameters, res?: CustomResponse): DeletedParameters | null {
        return this.checkVariableNotNull("deleted", parameters, res);
    }

    public static checkFilter(parameters: SwaggerRequestParameters, res?: CustomResponse): string | null {
        return this.checkVariableNotNull("filter", parameters, res);
    }

    public static checkIdAndDelete(
            parameters: SwaggerRequestParameters, res?: CustomResponse): ParametersIdDeleted | null {
        const response: ParametersIdDeleted = {
            deleted: this.checkDeleted(parameters),
            id: this.checkId(parameters),
            idUser: this.checkVariableNotNull("idUser", parameters)
        };
        if (!response.deleted || (response.id !== 0 && !response.id)) {
            return null;
        }
        return response;
    }

    public static checkAllParametersGet(
            parameters: SwaggerRequestParameters, res?: CustomResponse): ParametersComplete | null {
        const error = false;
        const response: ParametersComplete = {
            deleted: this.checkDeleted(parameters),
            filter: this.checkVariableNotNull("filter", parameters),
            id: this.checkId(parameters),
            idAccount: this.checkVariableNotNull("idAccount", parameters),
            idUser: this.checkVariableNotNull("idUser", parameters),
            limit: this.checkVariableNotNull("limit", parameters),
            metadata: this.checkVariableNotNull("metadata", parameters),
            orderBy: this.checkVariableNotNull("orderBy", parameters),
            skip: this.checkVariableNotNull("skip", parameters)
        };
        if (!response.skip || response.skip < 0) {
            response.skip = 0;
        }
        if (!response.limit || response.limit < 0) {
            response.limit = 10;
        }
        // TODO: check if any variable is null.
        if (res && error) {
            this.sendError(res);
        }
        return response;
    }

    public static hasPermission(req: CustomRequest, role: UsersRole, res?: CustomResponse): boolean {
        let response: boolean = false;
        const user = Utilities.getUser(req);
        if (user) {
            switch (user.role) {
                case UsersRole.ADMIN:
                    response = true;
                    break;
                case UsersRole.EDITOR:
                    response = (role === UsersRole.EDITOR || role === UsersRole.VIEWER || role === UsersRole.COMMON);
                    break;
                case UsersRole.VIEWER:
                    response = (role === UsersRole.VIEWER || role === UsersRole.COMMON);
                    break;
                case UsersRole.COMMON:
                    response = (role === UsersRole.COMMON);
                    break;
                default:
                    response = false;
                    break;
            }
        }
        if (!response) {
            let textResponse: string;
            if (user) {
                LoggerUtility.warn("User", user.id, "with role", user.role, "permission denied of", role);
                textResponse = VALID_RESPONSES.ERROR.AUTH.UNPRIVILEGED;
            } else {
                LoggerUtility.warn("Unregistered user permission denied of", role);
                textResponse = VALID_RESPONSES.ERROR.AUTH.TOKEN.USER;
            }
            if (res) {
                ResponsePayload.response(res, textResponse, 401);
            }
        }
        return response;
    }

    public static hasViewerPermission(req: CustomRequest, res?: CustomResponse): boolean {
        return this.hasPermission(req, UsersRole.VIEWER, res);
    }

    public static hasEditorPermission(req: CustomRequest, res?: CustomResponse): boolean {
        return this.hasPermission(req, UsersRole.EDITOR, res);
    }

    public static hasAdminPermission(req: CustomRequest, res?: CustomResponse): boolean {
        return this.hasPermission(req, UsersRole.ADMIN, res);
    }

    public static hasOwnPermissions(req: CustomRequest, data: Users | number, res?: CustomResponse): boolean {
        let result = false;
        let id: number;
        if (data instanceof Users) {
            if (data && data.id) {
                id = data.id;
            }
        } else if (typeof data === "number") {
            id = data;
        }
        if (id && this.getUserId(req)) {
            result = this.getUserId(req) === id;
        }
        if (!result && res) {
            ResponsePayload.response(res, VALID_RESPONSES.ERROR.AUTH.UNPRIVILEGED, 401);
        }
        return result;
    }

    public static getUser(req: CustomRequest): Users {
        if (req && req.user && req.user.data) {
            return req.user.data;
        } else if (req.headers.x_user_key) {
            return JwtUtils.getUser(req.headers.x_user_key.toString());
        }
        return null;
    }
    public static getUserRole(req: CustomRequest): UsersRole {
        const user = this.getUser(req);
        if (user) {
            return user.role;
        }
        return null;
    }

    public static getUserId(req: CustomRequest): number {
        const user = this.getUser(req);
        if (user) {
            return user.id;
        }
        return null;
    }

    public static async getUserAccounts(req: CustomRequest, permission: UsersRole, idUser?: number
                                    ,   idAccount?: number, res?: CustomResponse): Promise<Array<number>> {
        const actualIdUser = Utilities.getUserId(req);
        const whereClause: FindConditions<Memberships> = {};
        if (Utilities.hasViewerPermission(req)) {
            if (idUser) {
                whereClause.idUser = idUser;
            }
        } else {
            whereClause.idUser = actualIdUser;
        }
        if (idAccount) {
            whereClause.idAccount = idAccount;
        }
        return (await DatabaseMemberships.find({ where: whereClause, select: ["idAccount"], relations: ["idAccount"] }))
            .map((acc) => acc.idAccount.id);
    }

    public static addDeletedParam(deleted: DeletedParameters, params: any): object {
        if (!params) {
            params = {};
        }
        switch (deleted) {
            case DeletedParameters.ALL:
                break;
            case DeletedParameters.DELETED:
                params.deleted = true;
                break;
            case DeletedParameters["NOT-DELETED"]:
                params.deleted = false;
                break;
        }
        return params;
    }
    public static getMetadataFormat(
        items: Array<any>, skip: number,
        limit: number, total: number,
        containsMetadata: boolean) {
        let metadata: Metadata = null;
        if (containsMetadata) {
            metadata = {
                first: 0
                , last: total
                , next: skip + limit
                , prev: skip - limit
                , self: skip
            };
            if (metadata.prev < 0) {
                metadata.prev = 0;
            }
            if (metadata.next > metadata.last) {
                metadata.next = -1;
            }
        }
        return { metadata, items };
    }

    public static getFindObject(skip: number, limit: number
                            ,   orderBy: string, filter: string, deleted: DeletedParameters
                            ,   entity: BaseEntity, relations?: Array<string>, idUser?: number): FindManyOptions {
        let whereObject: { idUser?: number } = {};
        if (filter) {
            try {
                whereObject = JSON.parse(orderBy);
            } catch (e) {
                LoggerUtility.warn("orderBy parameter provided is not in JSON format.", orderBy);
            }
        }
        if (idUser) {
            whereObject.idUser = idUser;
        }
        const object: FindManyOptions<typeof entity> = {
            skip,
            take: limit,
            where: Utilities.addDeletedParam(deleted, whereObject)
        };
        if (orderBy) {
            let order: object;
            try {
                order = JSON.parse(orderBy);
            } catch (e) {
                order = { id: orderBy };
            }
            if (order) {
                object.order = order;
            } else {
                return null;
            }
        }
        object.relations = this.addRelations(entity, relations);
        return object;
    }

    public static getFindOneObject(id: number, deleted: DeletedParameters
                                ,  entity: BaseEntity, relations?: Array<string>, idUser?: number): FindOneOptions {
        const whereObject: { id: number, idUser?: number } = { id };
        if (idUser) {
            whereObject.idUser = idUser;
        }
        const object: FindOneOptions<typeof entity> = {
            where:
                Utilities.addDeletedParam(deleted, whereObject)
        };
        object.relations = Utilities.addRelations(entity, relations);
        return object;
    }

    public static addRelations(entity: BaseEntity, relations?: Array<string>): Array<string> {
        let finalRelations: Array<string>;
        if (relations) {
            finalRelations = relations;
        } else if (entity instanceof DatabaseAccounts) {
            finalRelations = ["memberships", "memberships.idUser", "accountEmails"];
        } else if (entity instanceof DatabaseAccountEmails) {
            // finalRelations = [];
        } else if (entity instanceof DatabaseCarriers) {
            // finalRelations = [];
        } else if (entity instanceof DatabaseDeliveries) {
            finalRelations = ["idAccount", "idLocation", "handedOvers", "journeyDeliveries"];
        } else if (entity instanceof DatabaseHandedOvers) {
            // finalRelations = [];
        } else if (entity instanceof DatabaseJourneyDeliveries) {
            // finalRelations = [];
        } else if (entity instanceof DatabaseJourneys) {
            finalRelations = [ "idCarrier", "idLocationStart", "idLocationEnd", "journeyDeliveries" ];
        } else if (entity instanceof DatabaseLocations) {
            // finalRelations = [];
        } else if (entity instanceof DatabaseMemberships) {
            finalRelations = ["idAccount", "idUser"];
        } else if (entity instanceof DatabaseUsers) {
            finalRelations = ["memberships", "memberships.idAccount"];
        }
        return finalRelations;
    }

    public static checkUrl(url: string): boolean {
        const pattern = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
        return pattern.test(url);
    }

    private static sendError(res: CustomResponse, text?: string) {
        ResponsePayload.response400(res, VALID_RESPONSES.ERROR.PARAMS.MISSING + " " + text);
    }
}
export interface ParametersIdDeleted {
    id: number | null;
    deleted: DeletedParameters | null;
    idUser?: number | null;
}

export interface ParametersComplete extends ParametersIdDeleted {
    skip: number | null;
    limit: number | null;
    orderBy: string | null;
    filter: string | null;
    metadata: boolean | null;
    idUser?: number | null;
    idAccount?: number | null;
}
