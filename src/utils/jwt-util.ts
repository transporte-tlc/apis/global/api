// TODO: convert this file to a class.
import * as dotenv from "dotenv";
dotenv.config();

import jwt from "express-jwt";
import * as fs from "fs";
import json from "jsonwebtoken";
import _ from "lodash";
import pathToRegexp from "path-to-regexp";
import { Users } from "../models";
import { CustomNext, CustomRequest, CustomResponse } from "./customsHandlers";
import { LoggerUtility } from "./LoggerUtility";
import { VALID_RESPONSES } from "./ValidResponses";
import { ResponsePayload } from "./writer";

let fileJwt = process.env.JWT_FILE;
const fileJwtDefault = "./keys/key.b64.pub";

if (!fileJwt) {
    LoggerUtility.warn("Variable JWT_FILE not set. Used default value: ", fileJwtDefault);
    fileJwt = fileJwtDefault;
}
const publicKey = fs.readFileSync(fileJwt).toString();
module.exports.publicKey = publicKey;

let fileAppTokens = process.env.APP_TOKENS_FILE;
if (!fileAppTokens) {
    fileAppTokens = "./app_tokens.json";
}
const APP_TOKENS = JSON.parse(fs.readFileSync(fileAppTokens, "utf8"));
module.exports.addJWT = (app: any, SWAGGER_BASE_PATH: string) => {
    const PUBLIC_URLS: Array<any> = [
        pathToRegexp(SWAGGER_BASE_PATH + "users/email/change/:token"),
        pathToRegexp(SWAGGER_BASE_PATH + "users/email/request"),
        pathToRegexp(SWAGGER_BASE_PATH + "users/email/request/:token"),
        pathToRegexp(SWAGGER_BASE_PATH + "users/password/change/:token"),
        pathToRegexp(SWAGGER_BASE_PATH + "users/password/forgot"),
        pathToRegexp(SWAGGER_BASE_PATH + "users/login"),
        pathToRegexp(SWAGGER_BASE_PATH + "users/login/:token"),
        pathToRegexp(SWAGGER_BASE_PATH + "users"),
        pathToRegexp(SWAGGER_BASE_PATH + "docs"),
        pathToRegexp(SWAGGER_BASE_PATH + "docs/:option"),
        pathToRegexp(SWAGGER_BASE_PATH + "api-docs")
    ];
    app.use(jwt({
        credentialsRequired: true
        , getToken: function fromHeaderOrQuerystring(req: any) {
            const token = req.headers.x_user_key;
            if (token) {
                if (token.split(" ")[0] === "Bearer") {
                    return token.split(" ")[1];
                }
                return token;
            }
            return null;
        }
        , secret: publicKey
    }).unless({ path: PUBLIC_URLS }));
    app.use(checkAppToken);
    app.use(checkUserToken);
};
const checkAppToken = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (JwtUtils.verifyAppToken(req)) {
        next();
    } else {
        ResponsePayload.response(res, VALID_RESPONSES.ERROR.AUTH.TOKEN.APP, 401);
    }
    // if (err) {
    //   LoggerUtility.warn("Error verifying token", app_token, req.url, err);
    //   ResponsePayload.response401(res);
    //   return;
    // }
    return;
};

const checkUserToken = (err, req, res, next) => {
    if (err.name === "UnauthorizedError") {
        ResponsePayload.response(res, VALID_RESPONSES.ERROR.AUTH.TOKEN.USER, 401);
        LoggerUtility.warn("Unregistered user for url", req.url);
        return;
    }
    next();
};

let GLOBAL_SWAGGER_BASE_PATH = process.env.SWAGGER_BASE_PATH;
if (!GLOBAL_SWAGGER_BASE_PATH) {
    GLOBAL_SWAGGER_BASE_PATH = "/";
}

LoggerUtility.info("Global base path", GLOBAL_SWAGGER_BASE_PATH);

const WHITE_LIST: Array<RegExp> = [
    pathToRegexp(GLOBAL_SWAGGER_BASE_PATH + "users/email/change/:token"),
    pathToRegexp(GLOBAL_SWAGGER_BASE_PATH + "users/email/request/:token"),
    pathToRegexp(GLOBAL_SWAGGER_BASE_PATH + "users/password/change/:token"),
    pathToRegexp(GLOBAL_SWAGGER_BASE_PATH + "users/login/:token"),
    pathToRegexp(GLOBAL_SWAGGER_BASE_PATH + "docs"),
    pathToRegexp(GLOBAL_SWAGGER_BASE_PATH + "docs/:option"),
    pathToRegexp(GLOBAL_SWAGGER_BASE_PATH + "favicon.ico"),
    pathToRegexp(GLOBAL_SWAGGER_BASE_PATH + "api-docs")
];

export class JwtUtils {
    public static getUser(token: string): Users {
        const data = this.getData(token);
        if (data && data.data) {
            return data.data;
        }
        return null;
    }

    public static getData(token: string, jwtOptions?: json.VerifyOptions): JWtResponse {
        let response = null;
        try {
            response = json.verify(token, publicKey, jwtOptions);
        } catch (error) {
            LoggerUtility.warn("Malformed token.", token);
        }
        return response;
    }

    public static signData(user: Users, date?: Date): { user: Users, token: string, expDate: Date } {
        if (!date) {
            date = new Date();
        }
        date.setDate(date.getDate() + 1);
        const newToken = json.sign({
            data: user
        }, publicKey, {
                algorithm: "HS256"
                , expiresIn: "24h"
            });
        return { user, token: newToken, expDate: date };
    }

    public static verifyAppToken(req: CustomRequest): boolean {
        const appToken = req.headers.x_app_id;
        if (!appToken) {
            LoggerUtility.debug(req.url);
            for (const path of WHITE_LIST) {
                if (path.test(req.url)) {
                    LoggerUtility.debug(req.url, "white list");
                    return true;
                }
            }
            LoggerUtility.warn("No token provided ", req.url);
            return false;
        }
        let finalToken = 0;
        _.forEach(APP_TOKENS || [], (token: any) => {
            finalToken++;
            if (token.token === appToken) {
                if (finalToken === APP_TOKENS.length) {
                    finalToken--;
                }
                return false;
            }
            return true;
        });
        if (finalToken === APP_TOKENS.length) {
            LoggerUtility.warn("Token provided not exists", appToken, req.url);
            return false;
        }
        return true;
    }
}

export interface JWtResponse {
    data: Users;
    exp: number;
    iat: number;
}
