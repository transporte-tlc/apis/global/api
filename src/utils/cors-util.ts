import { CorsOptions } from "cors";
import cors from "cors";
import _ from "lodash";
import pathToRegexp from "path-to-regexp";

// TODO: add a file to register whitelist.
// FIXIT: convert this file to a class.
const whitelist = [pathToRegexp("http://localhost:port*"),
pathToRegexp("https://localhost:port*"),
pathToRegexp("http://127.0.0.1:port*"),
pathToRegexp("https://127.0.0.1:port*"),
pathToRegexp("http://vamosalacarga.com.ar"),
pathToRegexp("https://vamosalacarga.com.ar"),
pathToRegexp("http://tlc.singleton.com.ar")];

const corsOptions: CorsOptions = {
  allowedHeaders: ["Access-Control-Allow-Headers"
    , "Origin,Accept"
    , "X-Requested-With"
    , "Content-Type"
    , "Access-Control-Request-Method"
    , "Access-Control-Request-Headers"
    , "x_app_id"
    , "x_user_key"]
  , credentials: true
  , methods: ["GET", "HEAD", "OPTIONS", "POST", "PUT", "DELETE"]
  , origin: whitelist
};

module.exports = (app: any) => {
  app.use(cors(corsOptions));
};
