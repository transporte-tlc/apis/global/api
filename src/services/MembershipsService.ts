"use strict";

import { FindManyOptions } from "typeorm";
import { DatabaseMemberships } from "../database";
import { Memberships } from "../models";
import { DeletedParameters } from "../models/all/parameters";
import { LoggerUtility } from "../utils/LoggerUtility";
import { Utilities } from "../utils/utilities";
import { VALID_RESPONSES } from "../utils/ValidResponses";
import { AccountsService } from "./AccountsService";
import { UsersService } from "./UsersService";

const SERVICE_NAME = "MembershipsService";

export class MembershipsService {

    public static async checkIfExists(id: number): Promise<string> {
        if (id) {
            const FUNCTION_NAME = "checkIfExists";
            try {
                await MembershipsService.getMembershipsById(id, DeletedParameters["NOT-DELETED"]);
                return null;
            } catch (reason) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, reason, id);
                return reason;
            }
        }
        return null;
    }

    public static async checkDependencies(membership: Memberships): Promise<string> {
        const FUNCTION_NAME = "checkDependencies";
        LoggerUtility.debug(SERVICE_NAME, FUNCTION_NAME, membership);
        const promises = [];
        promises.push(UsersService.getUserById(membership.idUser, DeletedParameters["NOT-DELETED"]));
        promises.push(AccountsService.getAccountById(membership.idAccount, DeletedParameters["NOT-DELETED"]));
        try {
            await Promise.all(promises);
        } catch (reason) {
            LoggerUtility.debug(SERVICE_NAME, FUNCTION_NAME, reason);
            return reason;
        }
        return null;
    }

    /**
     * Add one Membership.
     * Add one Membership.
     *
     * handedOvers Memberships
     * returns Memberships
     */
    public static addMembership(membership: Memberships) {
        const FUNCTION_NAME = "add";
        return new Promise(async (resolve, reject) => {
            delete membership.id;
            membership.deleted = false;
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME);
            LoggerUtility.debug(SERVICE_NAME, FUNCTION_NAME, membership);
            const validationError = membership.isValid();
            if (validationError) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "data not valid", validationError, membership);
                reject(validationError);
                return;
            }
            const result = await MembershipsService.checkDependencies(membership);
            if (result) {
                reject(result);
                return;
            }
            let newMembership: DatabaseMemberships = await DatabaseMemberships
                .findOne({ where: { idAccount: membership.idAccount, idUser: membership.idUser } });
            if (newMembership) {
                if (newMembership.deleted) {
                    LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "found deleted", newMembership.id);
                    membership.id = newMembership.id;
                } else {
                    LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME
                        , "already created", VALID_RESPONSES.ERROR.EXIST, newMembership);
                    reject(VALID_RESPONSES.ERROR.EXIST);
                    return;
                }
            }
            newMembership = await DatabaseMemberships.save(new DatabaseMemberships(null, membership));
            if (newMembership) {
                const dataMembership = await DatabaseMemberships.findOne(Utilities.
                    getFindOneObject(newMembership.id, DeletedParameters["NOT-DELETED"], new DatabaseMemberships()));
                LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "created with id ", newMembership.id);
                resolve(new Memberships(null, dataMembership));
                return;
            }
            LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "NOT created ", membership);
            reject(VALID_RESPONSES.ERROR.UNRECOGNIZED);
            return;
        });
    }

    /**
     * Add a deleted Membership.
     * Add a deleted Membership.
     *
     * id Long id to delete
     * returns Long
     */
    public static addDeletedMembership(id: number) {
        const FUNCTION_NAME = "addDeleted";
        return new Promise(async (resolve, reject) => {
            resolve();
        });
    }

    /**
     * Delete one Membership.
     * Delete one Membership.
     *
     * id Long id to delete
     * returns String
     */
    public static deleteMembership(id: number) {
        const FUNCTION_NAME = "delete";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, id);
            const membership = await DatabaseMemberships.findOne({ where: { id, deleted: false } });
            if (!membership) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, VALID_RESPONSES.ERROR.NOT_EXIST.MAIN, id);
                reject(VALID_RESPONSES.ERROR.NOT_EXIST.MAIN);
                return;
            }
            membership.deleted = true;
            await membership.save();
            resolve({ id: membership.id });
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "deleted", id);
            return;
        });
    }

    /**
     * Edit one Membership.
     * Edit one Membership.
     *
     * handedOvers Memberships
     * returns Memberships
     */
    public static editMembership(membership: Memberships) {
        const FUNCTION_NAME = "edit";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME);
            if (!membership.id) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME
                    , "id not valid", VALID_RESPONSES.ERROR.VALIDATION.ID, membership.id);
                reject(VALID_RESPONSES.ERROR.VALIDATION.ID);
                return;
            }
            LoggerUtility.debug(SERVICE_NAME, FUNCTION_NAME, membership);
            const prevMembership = await DatabaseMemberships.findOne({ where: { id: membership.id } });
            if (!prevMembership) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, VALID_RESPONSES.ERROR.NOT_EXIST.MAIN, membership.id);
                reject(VALID_RESPONSES.ERROR.NOT_EXIST.MAIN);
                return;
            }
            const validationError = membership.isValid();
            if (validationError) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "data not valid", validationError, membership);
                reject(validationError);
                return;
            }
            const result = await MembershipsService.checkDependencies(membership);
            if (result) {
                reject(result);
                return;
            }
            let newMembership: DatabaseMemberships = await DatabaseMemberships
                .findOne({ where: { idAccount: membership.idAccount, idUser: membership.idUser } });
            if (newMembership && newMembership.id !== membership.id) {
                // if(newMembership.deleted){
                //   LoggerUtility.info(SERVICE_NAME,FUNCTION_NAME, "found membership deleted", newMembership.id);
                //   membership.id = newMembership.id;
                // }else{
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME
                    , "already created", VALID_RESPONSES.ERROR.EXIST, newMembership);
                reject(VALID_RESPONSES.ERROR.EXIST);
                return;
                // }
            }
            newMembership = await DatabaseMemberships.save(new DatabaseMemberships(null, membership));
            if (newMembership) {
                const dataMembership = await DatabaseMemberships.findOne(Utilities.
                    getFindOneObject(newMembership.id, DeletedParameters["NOT-DELETED"], new DatabaseMemberships()));
                LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "edited with id ", newMembership.id);
                resolve(new Memberships(null, dataMembership));
                return;
            }
            LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "NOT edited ", membership);
            reject(VALID_RESPONSES.ERROR.UNRECOGNIZED);
            return;
        });
    }

    /**
     * Get all Memberships.
     * Get all Memberships.
     *
     * skip Integer number of item to skip
     * limit Integer max records to return
     * orderBy String order by property. (optional)
     * filter String filter data. (optional)
     * returns List
     */
    public static getMemberships(
        skip: number, limit: number, orderBy: string,
        filter: string, deleted: DeletedParameters,
        metadata: boolean, idUser: number) {
        const FUNCTION_NAME = "get";
        return new Promise(async (resolve, reject) => {
            const userExist = await UsersService.checkIfExists(idUser);
            if (userExist) {
                reject(userExist);
                return;
            }
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME);
            const object: FindManyOptions<DatabaseMemberships> =
                Utilities.getFindObject(skip, limit, orderBy, filter, deleted
                    , new DatabaseMemberships(), null, idUser);
            if (!object) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "order param malformed", orderBy);
                reject(VALID_RESPONSES.ERROR.PARAMS.MALFORMED.ORDERBY);
                return;
            }
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "with", object);
            const [memberships, total] = await DatabaseMemberships.findAndCount(object);
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "got", total);
            if (!memberships || !memberships.length) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "empty result with object", object);
                resolve();
                return;
            }
            const apiMemberships = [];
            for (const us of memberships) {
                apiMemberships.push(new Memberships(null, us));
            }
            resolve(Utilities.getMetadataFormat(apiMemberships, skip, limit, total, metadata));
            return;
        });
    }

    /**
     * Get one Membership.
     * Get one Membership.
     *
     * id Long id to delete
     * returns Memberships
     */
    public static getMembershipsById(id: number, deleted: DeletedParameters, idUser?: number) {
        const FUNCTION_NAME = "getById";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, id);
            const userExist = await UsersService.checkIfExists(idUser);
            if (userExist) {
                reject(userExist);
                return;
            }
            const prevMembership = await DatabaseMemberships.findOne(
                Utilities.getFindOneObject(id, deleted, new DatabaseMemberships(), null, idUser));
            if (!prevMembership) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME
                    , "not exists with id", id, "and deleted", deleted.toString());
                reject(VALID_RESPONSES.ERROR.NOT_EXIST.MEMBERSHIP);
                return;
            }
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "got", prevMembership.id);
            resolve(new Memberships(null, prevMembership));
            return;
        });
    }

}
