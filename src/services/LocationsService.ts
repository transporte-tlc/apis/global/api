"use strict";

import { DatabaseLocations } from "../database";
import { Locations } from "../models";
import { DeletedParameters } from "../models/all/parameters";
import { LoggerUtility } from "../utils/LoggerUtility";
import { Utilities } from "../utils/utilities";
import { VALID_RESPONSES } from "../utils/ValidResponses";

const SERVICE_NAME = "LocationsService";

export class LocationsService {

    public static async checkIfExists(id: number): Promise<string> {
        if (id) {
            const FUNCTION_NAME = "checkIfExists";
            try {
                await LocationsService.getLocationById(id, DeletedParameters["NOT-DELETED"]);
                return null;
            } catch (reason) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, reason, id);
                return reason;
            }
        }
        return null;
    }

    /**
     * Add one Location.
     * Add one Location.
     *
     * locations Locations
     * returns Locations
     */
    public static addLocation(location: Locations) {
        const FUNCTION_NAME = "add";
        return new Promise(async (resolve, reject) => {
            delete location.id;
            location.deleted = false;
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, location.name);
            const prevLocation = await DatabaseLocations.findOne({ where: { Name: location.name } });
            if (prevLocation) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "name already exists", location.name);
                reject(VALID_RESPONSES.ERROR.EXIST);
                return;
            }
            const newLocation = await DatabaseLocations.save(new DatabaseLocations(null, location));
            if (newLocation) {
                LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "created with id ", newLocation.id);
                resolve(new Locations(null, newLocation));
                return;
            }
            LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "NOT created ", location);
            reject(VALID_RESPONSES.ERROR.UNRECOGNIZED);
            return;
        });
    }

    /**
     * Add deleted Location.
     * Add deleted Location.
     *
     * id Long id to delete
     * returns Long
     */
    public static addDeletedLocation(id: number) {
        // TODO: Add deleted
        const FUNCTION_NAME = "addDeleted";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, id);
            const location = await DatabaseLocations.findOne({ where: { id, deleted: false } });
            if (!location) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "not exists with id", id);
                reject(VALID_RESPONSES.ERROR.NOT_EXIST);
                return;
            }
            location.deleted = true;
            await location.save();
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "deleted", id);
            resolve({ id: location.id });
            return;
        });
    }

    /**
     * Delete one Location.
     * Delete one Location.
     *
     * id Long id to delete
     * returns Long
     */
    public static deleteLocation(id: number) {
        const FUNCTION_NAME = "delete";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, id);
            const location = await DatabaseLocations.findOne({ where: { id, deleted: false } });
            if (!location) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, VALID_RESPONSES.ERROR.NOT_EXIST.MAIN, id);
                reject(VALID_RESPONSES.ERROR.NOT_EXIST.MAIN);
                return;
            }
            location.deleted = true;
            await location.save();
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "deleted", id);
            resolve({ id: location.id });
            return;
        });
    }

    /**
     * Edit one Location.
     * Edit one Location.
     *
     * locations Locations
     * returns Locations
     */
    public static editLocation(location: Locations) {
        const FUNCTION_NAME = "edit";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, location.id);
            const prevLocation = await DatabaseLocations.findOne({ where: { id: location.id } });
            if (!prevLocation) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, VALID_RESPONSES.ERROR.NOT_EXIST.MAIN, location.id);
                reject(VALID_RESPONSES.ERROR.NOT_EXIST.MAIN);
                return;
            }
            prevLocation.Name = location.name;
            prevLocation.deleted = location.deleted;
            await prevLocation.save();
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "edited", prevLocation.id);
            resolve(new Locations(null, prevLocation));
            return;
        });
    }

    /**
     * Get one Location.
     * Get one Location.
     *
     * id Long id to delete
     * returns Locations
     */
    public static getLocationById(id: number, deleted: DeletedParameters) {
        const FUNCTION_NAME = "getById";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, id, deleted.toString());
            const prevLocation = await DatabaseLocations.findOne(
                Utilities.getFindOneObject(id, deleted, new DatabaseLocations()));
            if (!prevLocation) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME
                    , "not exists with id", id, "and deleted", deleted.toString());
                reject(VALID_RESPONSES.ERROR.NOT_EXIST.LOCATION);
                return;
            }
            resolve(new Locations(null, prevLocation));
            return;
        });
    }

    /**
     * Get all Locations.
     * Get all Locations.
     *
     * skip Integer number of item to skip
     * limit Integer max records to return
     * orderBy String order by property. (optional)
     * filter String filter data. (optional)
     * returns List
     */
    public static getLocations(skip: number, limit: number, orderBy: string
                            ,  filter: string, deleted: DeletedParameters, metadata: boolean) {
        const FUNCTION_NAME = "get";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME);
            const object = Utilities.getFindObject(skip, limit, orderBy,
                filter, deleted, new DatabaseLocations());
            if (!object) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "order param malformed", orderBy);
                reject(VALID_RESPONSES.ERROR.PARAMS.MALFORMED.ORDERBY);
                return;
            }
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "with", object);
            const [locations, total] = await DatabaseLocations.findAndCount(object);
            if (!locations || !locations.length) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "empty result");
                resolve();
                return;
            }
            const apiLocations = [];
            for (const us of locations) {
                apiLocations.push(new Locations(null, us));
            }
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "got", apiLocations.length);
            resolve(Utilities.getMetadataFormat(apiLocations, skip, limit, total, metadata));
            return;
        });
    }
}
