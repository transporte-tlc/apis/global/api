"use strict";

import { DatabaseCarriers } from "../database";
import { Carriers } from "../models";
import { DeletedParameters } from "../models/all/parameters";
import { LoggerUtility } from "../utils/LoggerUtility";
import { Utilities } from "../utils/utilities";
import { VALID_RESPONSES } from "../utils/ValidResponses";

const SERVICE_NAME = "CarriersService";

export class CarriersService {

    public static async checkIfExists(id: number): Promise<string> {
        if (id) {
            const FUNCTION_NAME = "checkIfExists";
            try {
                await CarriersService.getCarrierById(id, DeletedParameters["NOT-DELETED"]);
                return null;
            } catch (reason) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, reason, id);
                return reason;
            }
        }
        return null;
    }

    /**
     * Add one carrier.
     * Add one carrier.
     *
     * carriers Carriers
     * returns List
     */
    public static addCarrier(carrier: Carriers) {
        const FUNCTION_NAME = "add";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, carrier.name);
            delete carrier.id;
            carrier.deleted = false;
            const prevCarrier = await DatabaseCarriers.findOne({ where: { Name: carrier.name } });
            if (prevCarrier) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "name already exists", carrier.name);
                reject(VALID_RESPONSES.ERROR.EXIST);
                return;
            }
            const newCarrier = await DatabaseCarriers.save(new DatabaseCarriers(null, carrier));
            if (newCarrier) {
                LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "created with id", newCarrier.id);
                resolve(new Carriers(null, newCarrier));
                return;
            }
            LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "NOT created", carrier);
            reject(VALID_RESPONSES.ERROR.UNRECOGNIZED);
            return;
        });
    }

    /**
     * Add deleted carrier.
     * Add deleted carrier.
     *
     * id Long id to delete
     * returns Long
     */
    public static addDeletedCarrier(id: number) {
        const FUNCTION_NAME = "addDeleted";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, id);
            const carrier = await DatabaseCarriers.findOne({ where: { id } });
            if (!carrier) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "not exists with id", id);
                reject(VALID_RESPONSES.ERROR.NOT_EXIST);
                return;
            }
            if (!carrier.deleted) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "not deleted with id", id);
                reject(VALID_RESPONSES.ERROR.NOT_DELETED);
                return;
            }
            carrier.deleted = false;
            await carrier.save();
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "deleted", id);
            resolve({ id: carrier.id });
            return;
        });
    }

    /**
     * Delete one carrier.
     * Delete one carrier.
     *
     * id Long id to delete
     * returns Long
     */
    public static deleteCarrier(id: number) {
        const FUNCTION_NAME = "delete";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, id);
            const carrier = await DatabaseCarriers.findOne({ where: { id, deleted: false } });
            if (!carrier) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, VALID_RESPONSES.ERROR.NOT_EXIST.MAIN, id);
                reject(VALID_RESPONSES.ERROR.NOT_EXIST.MAIN);
                return;
            }
            carrier.deleted = true;
            await carrier.save();
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "deleted", id);
            resolve({ id: carrier.id });
            return;
        });
    }

    /**
     * Edit one carrier.
     * Edit one carrier.
     *
     * carriers Carriers
     * returns Carriers
     */
    public static editCarrier(carrier: Carriers) {
        const FUNCTION_NAME = "edit";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, carrier.id);
            if (!carrier.id) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "id not valid",
                    VALID_RESPONSES.ERROR.VALIDATION.ID, carrier.id);
                reject(VALID_RESPONSES.ERROR.VALIDATION.ID);
                return;
            }
            let prevCarrier = await DatabaseCarriers.findOne({ where: { Name: carrier.name } });
            if (prevCarrier && prevCarrier.id !== carrier.id) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "name already exists", carrier.name);
                reject(VALID_RESPONSES.ERROR.VALIDATION.NAME);
                return;
            }
            prevCarrier = await DatabaseCarriers.findOne({ where: { id: carrier.id } });
            if (!prevCarrier) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, VALID_RESPONSES.ERROR.NOT_EXIST.MAIN, carrier.id);
                reject(VALID_RESPONSES.ERROR.NOT_EXIST.MAIN);
                return;
            }
            prevCarrier.Name = carrier.name;
            prevCarrier.deleted = carrier.deleted;
            await prevCarrier.save();
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "edited", prevCarrier.id);
            resolve(new Carriers(null, prevCarrier));
            return;
        });
    }

    /**
     * Get one carrier.
     * Get one carrier.
     *
     * id Long id to delete
     * returns List
     */
    public static getCarrierById(id: number, deleted: DeletedParameters) {
        const FUNCTION_NAME = "getById";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, id, deleted.toString());
            const prevCarrier = await DatabaseCarriers
                .findOne(Utilities.getFindOneObject(id, deleted, new DatabaseCarriers()));
            if (!prevCarrier) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME
                    , "not exists with id", id, "and deleted", deleted.toString());
                reject(VALID_RESPONSES.ERROR.NOT_EXIST.CARRIER);
                return;
            }
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "got", prevCarrier.id);
            resolve(new Carriers(null, prevCarrier));
            return;
        });
    }

    /**
     * Get all carriers.
     * Get all carriers.
     *
     * skip Integer number of item to skip
     * limit Integer max records to return
     * orderBy String order by property. (optional)
     * filter String filter data. (optional)
     * returns List
     */
    public static getCarriers(skip: number, limit: number
        ,                     orderBy: string, filter: string
        ,                     deleted: DeletedParameters, metadata: boolean) {
        const FUNCTION_NAME = "get";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME);
            const object = Utilities.getFindObject(skip, limit, orderBy,
                filter, deleted, new DatabaseCarriers());
            if (!object) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "order param malformed", orderBy);
                reject(VALID_RESPONSES.ERROR.PARAMS.MALFORMED.ORDERBY);
                return;
            }
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "with", object);
            const [carriers, total] = await DatabaseCarriers.findAndCount(object);
            if (!carriers || !carriers.length) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "empty result");
                resolve();
                return;
            }
            const apiCarriers = [];
            for (const us of carriers) {
                apiCarriers.push(new Carriers(null, us));
            }
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "got", apiCarriers.length);
            resolve(Utilities.getMetadataFormat(apiCarriers, skip, limit, total, metadata));
            return;
        });
    }

}
