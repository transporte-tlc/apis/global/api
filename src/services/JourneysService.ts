"use strict";

import { DatabaseJourneys } from "../database";
import { Journeys } from "../models";
import { DeletedParameters } from "../models/all/parameters";
import { LoggerUtility } from "../utils/LoggerUtility";
import { Utilities } from "../utils/utilities";
import { VALID_RESPONSES } from "../utils/ValidResponses";
import { CarriersService } from "./CarriersService";
import { JourneyDeliveriesService } from "./JourneyDeliveriesService";
import { LocationsService } from "./LocationsService";

const SERVICE_NAME = "JourneysService";

export class JourneysService {

    public static async checkIfExists(id: number): Promise<string> {
        const FUNCTION_NAME = "checkIfExists";
        if (id) {
            try {
                await JourneysService.getJourneyById(id, DeletedParameters["NOT-DELETED"]);
                return null;
            } catch (reason) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, reason, id);
                return reason;
            }
        }
        return null;
    }

    public static async checkDependencies(journey: Journeys): Promise<string> {
        const FUNCTION_NAME = "check";
        LoggerUtility.debug(SERVICE_NAME, FUNCTION_NAME, journey);
        const promises = [];
        promises.push(CarriersService.checkIfExists(journey.idCarrier));
        promises.push(LocationsService.checkIfExists(journey.idLocationEnd));
        promises.push(LocationsService.checkIfExists(journey.idLocationStart));
        if (journey.deliveries) {
            for (const delivery of journey.deliveries) {
                promises.push(JourneyDeliveriesService.checkIfExists(delivery.id));
            }
        }
        try {
            await Promise.all(promises);
        } catch (reason) {
            LoggerUtility.debug(SERVICE_NAME, FUNCTION_NAME, reason);
            return reason;
        }
        return null;
    }

    /**
     * Add one Journey.
     * Add one Journey.
     *
     * journeys Journeys
     * returns Journeys
     */
    public static addJourney(journey: Journeys) {
        const FUNCTION_NAME = "add";
        delete journey.id;
        return new Promise(async (resolve, reject) => {
            const validationError = journey.isValid();
            if (validationError) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "data not valid", validationError, journey);
                reject(validationError);
                return;
            }
            const result = await JourneysService.checkDependencies(journey);
            if (result) {
                reject(result);
                return;
            }
            const newJourney = await DatabaseJourneys.save(new DatabaseJourneys(null, journey));
            if (newJourney) {
                const dataMembership = await DatabaseJourneys.findOne(Utilities.
                    getFindOneObject(newJourney.id, DeletedParameters["NOT-DELETED"], new DatabaseJourneys()));
                LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "created with id ", newJourney.id);
                resolve(new Journeys(null, dataMembership));
                return;
            }
            LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "NOT created ", journey);
            reject(VALID_RESPONSES.ERROR.UNRECOGNIZED);
            return;
        });
    }

    public static checkJourney(journeys: Journeys) {
        const FUNCTION_NAME = "check";
        return new Promise(async (resolve, reject) => {
            // _.forEach(journeys.deliveries || [], (delivery: JourneyDeliveries) => {
            //   if (!delivery.idDelivery)
            //     return reject({ message: 'Invalid id of delivery.', data: JSON.stringify(delivery) });
            //   if (!delivery.quantity && !delivery.weight && !delivery.volume && !delivery.value) {
            //   }
            //   fetches.push(
            //     new schema_main.Delivery().where('id', delivery.idDelivery)
            //         result = result.toJSON();
            //         if (!result || result.id !== delivery.idDelivery)
            //           return { message: 'Delivery not found. idDelivery = ' + delivery.idDelivery };
            //         if (result.idLocation !== journeys.idLocationStart)
            //         if (result.Quantity)
            //           if (!delivery.quantity)
            //           else {
            //             delete delivery.weight;
            //             delete delivery.volume;
            //             delete delivery.value;
            //           }
            //         if (result.Weight)
            //           if (!delivery.weight)
            //           else {
            //             delete delivery.quantity;
            //             delete delivery.volume;
            //             delete delivery.value;
            //           }
            //         if (result.Volume)
            //           if (!delivery.volume)
            //           else {
            //             delete delivery.weight;
            //             delete delivery.quantity;
            //             delete delivery.value;
            //           }
            //         if (result.Value)
            //           if (!delivery.value)
            //           else {
            //             delete delivery.weight;
            //             delete delivery.volume;
            //             delete delivery.quantity;
            //           }
            //           return null;
            //       }).catch(function (error: any) {
            //         return error;
            //       }));
            // });
            // fetches.push(
            //   new schema_main.Carrier().where('id', journeys.idCarrier)
            //     .fetch({ columns: 'id' }).then(function (result: Carriers) {
            //       if (!result || result.id !== journeys.idCarrier)
            //         return { message: 'Carrier not found. ', data: journeys.idCarrier };
            //         return null;
            //     }).catch(function (error: any) {
            //       return error;
            //     }));
            // fetches.push(
            //   new schema_main.Location().where('id', journeys.idLocationStart)
            //     .fetch({ columns: 'id' }).then(function (result: Locations) {
            //       if (!result || result.id !== journeys.idLocationStart)
            //         return { message: 'LocationStart not found. ', data: journeys.idLocationStart };
            //         return null;
            //     }).catch(function (error: any) {
            //       return error;
            //     }));
            // fetches.push(
            //   new schema_main.Location().where('id', journeys.idLocationEnd)
            //     .fetch({ columns: 'id' }).then(function (result: Locations) {
            //       if (!result || result.id !== journeys.idLocationEnd)
            //         return { message: 'LocationEnd not found. ', data: journeys.idLocationEnd };
            //         return null;
            //     }).catch(function (error:any) {
            //       return error;
            //     }));
            // Promise.all(fetches).then((values) => {
            //   _.forEach(values || [], (result:any) => {
            //     if (result)
            //       return reject(result);
            //   });
            //   let newJourney:Journeys = {};
            //   newJourney.idCarrier = journeys.idCarrier;
            //   newJourney.idLocationStart = journeys.idLocationStart;
            //   newJourney.idLocationEnd = journeys.idLocationEnd;
            //   newJourney.startDate = journeys.startDate;
            //   newJourney.endDate = journeys.endDate;
            //   delete journeys.locationEnd;
            //   delete journeys.locationStart;
            //   delete journeys.carrier;
            //   return resolve(newJourney);
            // });
        });
    }

    /**
     * Add deleted Journey.
     * Add deleted Journey.
     *
     * id Long id to delete
     * returns Long
     */
    public static addDeletedJourney(id: number) {
        const FUNCTION_NAME = "addDeleted";
        return new Promise(async (resolve, reject) => {
            // schema_main.JourneyDelivery.where({'idJourney': id})
            // .save({deleted: 1 }, {method: 'update', patch: true, require: false}).then(function () {
            //   schema_main.Journey.create({ id: id, deleted: 1 }).then(function (result:any) {
            //     return resolve(result);
            //   }).catch(function (error:any) {
            //     return reject({ message: 'Journey with id ' + id + ' does not exists.'});
            //   });
            // }).catch(function (error:any) {
            //   return reject({ message: 'Journey with id ' + id + ' does not exists.'});
            // });
        });
    }

    /**
     * Delete one Journey.
     * Delete one Journey.
     *
     * id Long id to delete
     * returns Long
     */
    public static deleteJourney(id: number) {
        const FUNCTION_NAME = "delete";
        return new Promise(async (resolve, reject) => {
            // schema_main.JourneyDelivery.where({'idJourney': id})
            // .save({deleted: 1 }, {method: 'update', patch: true, require: false}).then(function () {
            //   schema_main.Journey.create({ id: id, deleted: 1 }).then(function (result:any) {
            //     return resolve(result);
            //   }).catch(function (error:any) {
            //     return reject({ message: 'Journey with id ' + id + ' does not exists.'});
            //   });
            // }).catch(function (error:any) {
            //   return reject({ message: 'Journey with id ' + id + ' does not exists.'});
            // });
        });
    }

    /**
     * Edit one Journey.
     * Edit one Journey.
     *
     * journeys Journeys
     * returns Journeys
     */
    public static editJourney(journey: Journeys) {
        const FUNCTION_NAME = "edit";
        return new Promise(async (resolve, reject) => {
            // checkJourney(journey).then(function (newJourney:Journeys) {
            //   schema_main.Journey.create(newJourney).then(function (result:Journeys) {
            //     // delete result.attributes.createdAt;
            //     // delete result.attributes.updatedAt;
            //     // delete result.attributes.deleted;
            //     let fetches: any = []
            //     _.forEach(journey.deliveries || [], (delivery: JourneyDeliveries) => {
            //       let newDelivery: JourneyDeliveries = {};
            //       newDelivery.idJourney = result.id;
            //       newDelivery.idDelivery = delivery.idDelivery;
            //       newDelivery.id = delivery.id;
            //       newDelivery.quantity = delivery.quantity;
            //       newDelivery.weight = delivery.weight;
            //       newDelivery.volume = delivery.volume;
            //       newDelivery.value = delivery.value;
            //       fetches.push(
            //         schema_main.JourneyDelivery.create(newDelivery).then(function (result: JourneyDeliveries) {
            //           newDelivery.id = result.id;
            //           return newDelivery;
            //         }).catch(function (error: any) {
            //           return reject(error);
            //         })
            //       );
            //     });
            //     Promise.all(fetches).then((values) => {
            //       newJourney.deliveries = values
            //       return resolve(newJourney);
            //     });
            //   }).catch(function (error: any) {
            //     LoggerUtility.log(error);
            //     return reject(error);
            //   });
            // }).catch(function (response) {
            //   return reject(response);
            // });
        });
    }

    /**
     * Get one Journey.
     * Get one Journey.
     *
     * id Long id to delete
     * returns Journeys
     */
    public static getJourneyById(id: number, deleted: string) {
        const FUNCTION_NAME = "getById";
        return new Promise(async (resolve, reject) => {
            // switch (deleted) {
            //   case "ALL":
            //     new schema_main.Journey().where('id', id)
            //       .fetch({
            //         withRelated: [
            //           { 'Carrier': (qb: QueryBuilder) => qb.columns('id', 'Name') }
            //           , { 'LocationStart': (qb: QueryBuilder) => qb.columns('id', 'Name') }
            //           , { 'LocationEnd': (qb: QueryBuilder) => qb.columns('id', 'Name') }
            //         ]
            //       }).then(function (carrier: any) {
            //         if (carrier) {
            //           carrier = carrier.toJSON();
            //         }
            //         return resolve(carrier);
            //       }).catch(function (error: any) {
            //         LoggerUtility.log(error);
            //         reject(error);
            //       });
            //     break;
            //   case "DELETED":
            //     new schema_main.JourneyD().where('id', id)
            //       .fetch({
            //         withRelated: [
            //           { 'Carrier': (qb: QueryBuilder) => qb.columns('id', 'Name') }
            //           , { 'LocationStart': (qb: QueryBuilder) => qb.columns('id', 'Name') }
            //           , { 'LocationEnd': (qb: QueryBuilder) => qb.columns('id', 'Name') }
            //         ]
            //       }).then(function (carrier:any) {
            //         if (carrier) {
            //           carrier = carrier.toJSON();
            //         }
            //         return resolve(carrier);
            //       }).catch(function (error:any) {
            //         LoggerUtility.log(error);
            //         reject(error);
            //       });
            //     break;
            //   case "NOT-DELETED":
            //   default:
            //     new schema_main.JourneyND().where('id', id)
            //       .fetch({
            //         withRelated: [
            //           { 'Carrier': (qb: QueryBuilder) => qb.columns('id', 'Name') }
            //           , { 'LocationStart': (qb: QueryBuilder) => qb.columns('id', 'Name') }
            //           , { 'LocationEnd': (qb: QueryBuilder) => qb.columns('id', 'Name') }
            //         ]
            //       }).then(function (carrier:any) {
            //         if (carrier) {
            //           carrier = carrier.toJSON();
            //         }
            //         return resolve(carrier);
            //       }).catch(function (error:any) {
            //         LoggerUtility.log(error);
            //         reject(error);
            //       });
            //     break;
            // }
        });
    }

    /**
     * Get all Journeys.
     * Get all Journeys.
     *
     * skip Integer number of item to skip
     * limit Integer max records to return
     * orderBy String order by property. (optional)
     * filter String filter data. (optional)
     * returns List
     */
    public static getJourneys(skip: number, limit: number, orderBy: string
                            , filter: string, deleted: DeletedParameters, metadata: boolean) {
        const FUNCTION_NAME = "get";
        return new Promise(async (resolve, reject) => {
            // switch (deleted) {
            //   case "ALL":
            //     new schema_main.Journey().orderBy(orderBy).fetchPage({
            //       limit: limit,
            //       offset: skip,
            //       withRelated: [
            //         { 'Carrier': (qb: QueryBuilder) => qb.columns('id', 'Name') }
            //         , { 'LocationStart': (qb: QueryBuilder) => qb.columns('id', 'Name') }
            //         , { 'LocationEnd': (qb: QueryBuilder) => qb.columns('id', 'Name') }
            //       ]
            //     }).then(function (carriers:any) {
            //       if (carriers)
            //         carriers = carriers.toJSON();
            //       return resolve(carriers);
            //     }).catch(function (error:any) {
            //       reject(error);
            //     });
            //     break;
            //   case "DELETED":
            //     new schema_main.JourneyD().orderBy(orderBy).fetchPage({
            //       limit: limit,
            //       offset: skip,
            //       withRelated: [
            //         { 'Carrier': (qb: QueryBuilder) => qb.columns('id', 'Name') }
            //         , { 'LocationStart': (qb: QueryBuilder) => qb.columns('id', 'Name') }
            //         , { 'LocationEnd': (qb: QueryBuilder) => qb.columns('id', 'Name') }
            //       ]
            //     }).then(function (carriers:any) {
            //       if (carriers)
            //         carriers = carriers.toJSON();
            //       return resolve(carriers);
            //     }).catch(function (error:any) {
            //       reject(error);
            //     });
            //     break;
            //   case "NOT-DELETED":
            //   default:
            //     new schema_main.JourneyND().orderBy(orderBy).fetchPage({
            //       limit: limit,
            //       offset: skip,
            //       withRelated: [
            //         { 'Carrier': (qb: QueryBuilder) => qb.columns('id', 'Name') }
            //         , { 'LocationStart': (qb: QueryBuilder) => qb.columns('id', 'Name') }
            //         , { 'LocationEnd': (qb: QueryBuilder) => qb.columns('id', 'Name') }
            //       ]
            //     }).then(function (carriers:any) {
            //       if (carriers)
            //         carriers = carriers.toJSON();
            //       return resolve(carriers);
            //     }).catch(function (error:any) {
            //       reject(error);
            //     });
            //     break;
            // }
        });
    }
}
