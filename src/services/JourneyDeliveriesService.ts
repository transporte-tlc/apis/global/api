"use strict";

import { JourneyDeliveries } from "../models";
import { DeletedParameters } from "../models/all/parameters";
import { LoggerUtility } from "../utils/LoggerUtility";

const SERVICE_NAME = "JourneyDeliveriesService";

export class JourneyDeliveriesService {

    public static async checkIfExists(id: number): Promise<string> {
        const FUNCTION_NAME = "checkIfExists";
        if (id) {
            try {
                await JourneyDeliveriesService.getJourneyDeliveryById(id, DeletedParameters["NOT-DELETED"]);
                return null;
            } catch (reason) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, reason, id);
                return reason;
            }
        }
        return null;
    }

    /**
     * Add one JourneyDelivery.
     * Add one JourneyDelivery.
     *
     * handedOvers JourneyDeliveries
     * returns JourneyDeliveries
     */
    public static addJourneyDelivery(journeyDelivery: JourneyDeliveries) {
        const FUNCTION_NAME = "add";
        delete journeyDelivery.id;
        return new Promise(async (resolve, reject) => {
            // schema_main.JourneyDeliveries.create(journeyDelivery).then(function (result: JourneyDeliveries) {
            //   // delete result.attributes.createdAt;
            //   // delete result.attributes.updatedAt;
            //   // delete result.attributes.deleted;
            //   return resolve(result);
            // }).catch(function (error: any) {
            //   LoggerUtility.log(error);
            //   reject(error);
            // });
        });
    }

    /**
     * Add deleted JourneyDelivery.
     * Add deleted JourneyDelivery.
     *
     * id Long id to delete
     * returns Long
     */
    public static addDeletedJourneyDelivery(id: number) {
        const FUNCTION_NAME = "add";
        return new Promise(async (resolve, reject) => {
            // schema_main.JourneyDeliveries.create({id: id, deleted: 1}).then(function (result: any) {
            //   delete result.attributes.createdAt;
            //   delete result.attributes.updatedAt;
            //   delete result.attributes.deleted;
            //   return resolve(result);
            // }).catch(function (error: any) {
            //   LoggerUtility.log(error);
            //   reject(error);
            // });
        });
    }

    /**
     * Delete one JourneyDelivery.
     * Delete one JourneyDelivery.
     *
     * id Long id to delete
     * returns Long
     */
    public static deleteJourneyDelivery(id: number) {
        const FUNCTION_NAME = "delete";
        return new Promise(async (resolve, reject) => {
            // schema_main.JourneyDeliveries.create({id: id, deleted: 1}).then(function (result: any) {
            //   delete result.attributes.createdAt;
            //   delete result.attributes.updatedAt;
            //   delete result.attributes.deleted;
            //   return resolve(result);
            // }).catch(function (error: any) {
            //   LoggerUtility.log(error);
            //   reject(error);
            // });
        });
    }

    /**
     * Edit one JourneyDelivery.
     * Edit one JourneyDelivery.
     *
     * handedOvers JourneyDeliveries
     * returns JourneyDeliveries
     */
    public static editJourneyDelivery(journeyDelivery: JourneyDeliveries) {
        const FUNCTION_NAME = "edit";
        return new Promise(async (resolve, reject) => {
            // schema_main.JourneyDeliveries.create(journeyDelivery).then(function (result: JourneyDeliveries) {
            //   // delete result.attributes.createdAt;
            //   // delete result.attributes.updatedAt;
            //   // delete result.attributes.deleted;
            //   return resolve(result);
            // }).catch(function (error: any) {
            //   LoggerUtility.log(error);
            //   reject(error);
            // });
        });
    }

    /**
     * Get all JourneyDeliveries.
     * Get all JourneyDeliveries.
     *
     * skip Integer number of item to skip
     * limit Integer max records to return
     * orderBy String order by property. (optional)
     * filter String filter data. (optional)
     * returns List
     */
    public static getJourneyDeliveries(skip: number, limit: number, orderBy: string
                                    ,  filter: string, deleted: DeletedParameters
                                    ,  metadata: boolean, accounts?: number) {
        const FUNCTION_NAME = "get";
        return new Promise(async (resolve, reject) => {
            // switch(deleted){
            //   case DeletedParameters.ALL:
            //     new schema_main.JourneyDelivery().orderBy(orderBy).fetchPage({
            //       limit: limit,
            //       offset: skip,
            //       columns:['id','Name']
            //     }).then(function (carriers: any) {
            //       if (carriers)
            //         carriers = carriers.toJSON();
            //       return resolve(carriers);
            //     }).catch(function (error: any) {
            //       reject(error);
            //     });
            //   break;
            //   case DeletedParameters.DELETED:
            //     new schema_main.JourneyDeliveryD().orderBy(orderBy).fetchPage({
            //       limit: limit,
            //       offset: skip,
            //       columns:['id','Name'],
            //       withRelated: ['deliveries']
            //     }).then(function (carriers: any) {
            //       if (carriers)
            //         carriers = carriers.toJSON();
            //       return resolve(carriers);
            //     }).catch(function (error: any) {
            //       reject(error);
            //     });
            //   break;
            //   case DeletedParameters["NOT-DELETED"]:
            //   default:
            //     new schema_main.JourneyDeliveryND().orderBy(orderBy).fetchPage({
            //       limit: limit,
            //       offset: skip,
            //       columns:['id','Name']
            //     }).then(function (carriers: any) {
            //       if (carriers)
            //         carriers = carriers.toJSON();
            //       return resolve(carriers);
            //     }).catch(function (error: any) {
            //       reject(error);
            //     });
            //   break;
            // }
        });
    }

    /**
     * Get one JourneyDelivery.
     * Get one JourneyDelivery.
     *
     * id Long id to delete
     * returns JourneyDeliveries
     */
    public static getJourneyDeliveryById(id: number, deleted: DeletedParameters, accounts?: number) {
        const FUNCTION_NAME = "getById";
        return new Promise(async (resolve, reject) => {
            // switch(deleted){
            //   case DeletedParameters.ALL:
            //     new schema_main.JourneyDelivery().where('id', id)
            //     .fetch({columns:['id','Name'],
            //       withRelated: ['deliveries']}).then(function (carrier: any) {
            //     if (carrier){
            //       carrier = carrier.toJSON();
            //     }
            //     return resolve(carrier);
            //   }).catch(function (error: any) {
            //     LoggerUtility.log(error);
            //     reject(error);
            //   });
            //   break;
            //   case DeletedParameters.DELETED:
            //   new schema_main.JourneyDeliveryD().where('id', id)
            //   .fetch({columns:['id','Name']}).then(function (carrier: any) {
            //   if (carrier){
            //     carrier = carrier.toJSON();
            //   }
            //   return resolve(carrier);
            // }).catch(function (error: any) {
            //   LoggerUtility.log(error);
            //   reject(error);
            // });
            //   break;
            //   case DeletedParameters["NOT-DELETED"]:
            //   default:
            //   new schema_main.JourneyDeliveryND().where('id', id)
            //   .fetch({columns:['id','Name']}).then(function (carrier: any) {
            //   if (carrier){
            //     carrier = carrier.toJSON();
            //   }
            //   return resolve(carrier);
            // }).catch(function (error: any) {
            //   LoggerUtility.log(error);
            //   reject(error);
            // });
            //   break;
            // }
        });
    }

}
