"use strict";

import { DatabaseUsers } from "../database";
import { Users } from "../models";
import { Data } from "../models/all/data";
import { JwtUtils } from "../utils/jwt-util";
import { LoggerUtility } from "../utils/LoggerUtility";
import { VALID_RESPONSES } from "../utils/ValidResponses";

const SERVICE_NAME = "EmailsService";

export class EmailsService {
    /**
     * change user's email.
     * Change user's email.
     *
     * newEmail String
     * userId String User to change password. Only for admin users. (optional)
     * no response value expected for this operation
     */
    public static changeEmail(newEmail: string, userId: number) {
        const FUNCTION_NAME = "change";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME);
            let user = new Users({ email: newEmail });
            if (!user.isEmailValid()) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "email is not valid", userId);
                reject(VALID_RESPONSES.ERROR.VALIDATION.USER.EMAIL);
                return;
            }
            const userDatabase = await DatabaseUsers.findOne({ where: { id: userId, deleted: false } });
            if (!userDatabase) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "user not registered", userId);
                reject(VALID_RESPONSES.ERROR.NOT_EXIST);
                return;
            }
            user = new Users(null, userDatabase);
            user.email = newEmail;
            user.emailVerified = false;
            const editedUser = await DatabaseUsers.save(new DatabaseUsers(null, user));
            if (editedUser) {
                LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "user's email changed", userId);
                // TODO: send verification email.
                resolve("OK");
                return;
            }
            LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "unrecognized error", userId);
            reject(VALID_RESPONSES.ERROR.UNRECOGNIZED);
            return;
        });
    }

    public static changeEmailToken(token: string) {
        const FUNCTION_NAME = "changeEmailToken";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, token);
            const data: any = JwtUtils.getData(token, { ignoreExpiration: true });
            if (!data || !data.data) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "verify error", token);
                reject(VALID_RESPONSES.ERROR.EMAIL.VERIFICATION.TOKEN.UNRECOGNIZED);
                return;
            }
            if (data.exp && data.exp < Date.now().valueOf() / 1000) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "token expired", token, data.exp);
                reject(VALID_RESPONSES.ERROR.EMAIL.VERIFICATION.TOKEN.EXPIRED);
                return;
            }
            const user = await DatabaseUsers.findOne({ where: { Email: data.data.email, deleted: false } });
            if (!user) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "email not existed", data.data.email);
                reject(VALID_RESPONSES.ERROR.EMAIL.NOT_REGISTERED);
                return;
            }
            user.verifiedEmail = true;
            // TODO: send email with welcome message.
            await user.save();
            resolve("OK");
            return;
        });
    }

    /**
     * send verification email to user
     * Send verification email to user
     *
     * data Data
     * no response value expected for this operation
     */
    public static sendEmail(data: Data) {
        const FUNCTION_NAME = "send";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, data.email);
            const user = new Users({ email: data.email });
            if (!user.isEmailValid()) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "invalid email", data.email);
                reject(VALID_RESPONSES.ERROR.VALIDATION.USER.EMAIL);
                return;
            }
            const databaseUser = await DatabaseUsers.findOne({ where: { Email: data.email, deleted: false } });
            if (!databaseUser) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "email not registered", data.email);
                reject(VALID_RESPONSES.ERROR.EMAIL.NOT_REGISTERED);
                return;
            }
            const signedData = JwtUtils.signData(new Users(null, databaseUser));
            if (!signedData) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "signData error", databaseUser.id);
                reject(VALID_RESPONSES.ERROR.UNRECOGNIZED);
                return;
            }
            // TODO: send email. Information inside signed data.
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "sent to", data.email);
            resolve("OK");
            return;
        });
    }

    /**
     * Verify user's email
     *
     * token String Access token
     * returns loginObject
     */
    public static verifyEmail(token: string) {
        const FUNCTION_NAME = "verify";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, token);
            const data: any = JwtUtils.getData(token, { ignoreExpiration: true });
            if (!data || !data.data) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "verify error", token);
                reject(VALID_RESPONSES.ERROR.EMAIL.VERIFICATION.TOKEN.UNRECOGNIZED);
                return;
            }
            if (data.exp && data.exp < Date.now().valueOf() / 1000) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "token expired", token, data.exp);
                reject(VALID_RESPONSES.ERROR.EMAIL.VERIFICATION.TOKEN.EXPIRED);
                return;
            }
            const user = await DatabaseUsers.findOne({ where: { Email: data.data.email, deleted: false } });
            if (!user) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "email not existed", data.data.email);
                reject(VALID_RESPONSES.ERROR.EMAIL.NOT_REGISTERED);
                return;
            }
            user.verifiedEmail = true;
            // TODO: send email with welcome message.
            await user.save();
            resolve("OK");
            return;
        });
    }
}
