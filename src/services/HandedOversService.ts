"use strict";

import _ from "lodash";
import { DeletedResponse, HandedOvers } from "../models";
import { DeletedParameters } from "../models/all/parameters";

const SERVICE_NAME = "HandedOversService";

export class HandedOversService {

    /**
     * Add one HandedOver.
     * Add one HandedOver.
     *
     * handedOvers HandedOvers
     * returns HandedOvers
     */
    public static addHandedOver(handedOver: HandedOvers) {
        const FUNCTION_NAME = "add";
        delete handedOver.id;
        return new Promise(async (resolve, reject) => {
            // checkHanded(handedOver).then(function (handed) {
            //   schema_main.HandedOver.create(handed).then(function (result:any) {
            //     delete result.attributes.createdAt;
            //     delete result.attributes.updatedAt;
            //     delete result.attributes.deleted;
            //     return resolve(result);
            //   }).catch(function (error: any) {
            //     LoggerUtility.log(error);
            //     return reject(error);
            //   });
            // }).catch(function (error) {
            //   LoggerUtility.log(error);
            //   return reject(error);
            // });
        });
    }

    public static checkHanded(handed: HandedOvers) {
        const FUNCTION_NAME = "check";
        return new Promise(async (resolve, reject) => {
            // if (!handed)
            //   return reject({ message: 'Invalid handover object.' });
            // if (!handed.idLocation) {
            //   if (!handed.location || !handed.location.id)
            //     return reject({ message: 'handed must have idLocation.' });
            //   handed.idLocation = handed.location.id;
            // }
            // if (!handed.quantity && !handed.weight && !handed.volume && !handed.value)
            //   return reject({ message: 'It is not provided one of: Weight, Volume, Value.' });
            // if (!handed.issuedAt && Object.prototype.toString.call(handed.issuedAt) !== '[object Date]')
            //   return reject({ message: 'Invalid IssuedAt date.' });
            // let fetches = [];
            // fetches.push(
            //   new schema_main.handed().where('id', handed.id)
            //     .fetch({ columns: 'id' }).then(function (result: any) {
            //       if (!result || result.id !== handed.id)
            //         return { message: 'handed not found. ' + handed.id };
            //         return null;
            //     }).catch(function (error:any) {
            //       return error;
            //     }));
            // fetches.push(
            //   new schema_main.Location().where('id', handed.idLocation)
            //     .fetch({ columns: 'id' }).then(function (result: any) {
            //       if (!result || result.id !== handed.idLocation)
            //         return { message: 'Location not found. ' + handed.idLocation };
            //         return null;
            //     }).catch(function (error:any) {
            //       return error;
            //     }));
            // //TODO: check the location of the handed.
            // //TODO: check the type of the handed.
            // //TODO: check handed location in journeys.
            // Promise.all(fetches).then((values) => {
            //   _.forEach(values || [], (result:any) => {
            //     if (result)
            //       return reject(result);
            //   });
            //   let newHanded:HandedOvers = {};
            // newHanded.idAccount = handed.idAccount;
            // newHanded.idLocation = handed.idLocation;
            // newHanded.IssuedAt = handed.IssuedAt;
            // newHanded.IssuedBy = handed.IssuedBy;
            // newHanded.Reference = handed.Reference;
            // newHanded.Quantity = handed.Quantity;
            // newHanded.Weight = handed.Weight;
            // newHanded.Volume = handed.Volume;
            // newHanded.Value = handed.Value;
            // newHanded.Comment = handed.Comment;
            //   return resolve(newHanded)
            // });
        });
    }

    /**
     * Add deleted HandedOver.
     * Add deleted HandedOver.
     *
     * id Long id to delete
     * returns Long
     */
    public static addDeletedHandedOver(id: number) {
        const FUNCTION_NAME = "addDeleted";
        return new Promise(async (resolve, reject) => {
            // schema_main.HandedOver.create({ id: id, deleted: 1 }).then(function (result: any) {
            //   delete result.attributes.createdAt;
            //   delete result.attributes.updatedAt;
            //   delete result.attributes.deleted;
            //   return resolve(result);
            // }).catch(function (error: any) {
            //   LoggerUtility.log(error);
            //   reject(error);
            // });
        });
    }

    /**
     * Delete one HandedOver.
     * Delete one HandedOver.
     *
     * id Long id to delete
     * returns Long
     */
    public static deleteHandedOver(id: number) {
        const FUNCTION_NAME = "delete";
        return new Promise(async (resolve, reject) => {
            // schema_main.HandedOver.create({ id: id, deleted: 1 }).then(function (result: any) {
            //   delete result.attributes.createdAt;
            //   delete result.attributes.updatedAt;
            //   delete result.attributes.deleted;
            //   return resolve(result);
            // }).catch(function (error: any) {
            //   LoggerUtility.log(error);
            //   reject(error);
            // });
        });
    }

    /**
     * Edit one HandedOver.
     * Edit one HandedOver.
     *
     * handedOvers HandedOvers
     * returns HandedOvers
     */
    public static editHandedOver(handedOver: HandedOvers) {
        const FUNCTION_NAME = "edit";
        return new Promise(async (resolve, reject) => {
            // checkHanded(handedOver).then(function (handed) {
            //   schema_main.HandedOver.create(handed).then(function (result: any) {
            //     delete result.attributes.createdAt;
            //     delete result.attributes.updatedAt;
            //     delete result.attributes.deleted;
            //     return resolve(result);
            //   }).catch(function (error: any) {
            //     LoggerUtility.log(error);
            //     return reject(error);
            //   });
            // }).catch(function (error) {
            //   LoggerUtility.log(error);
            //   return reject(error);
            // });
        });
    }

    /**
     * Get one HandedOver.
     * Get one HandedOver.
     *
     * id Long id to delete
     * returns HandedOvers
     */
    public static getHandedOverById(id: number, deleted: DeletedParameters, accounts: string) {
        const FUNCTION_NAME = "getById";
        // var fetchPage = {
        //   withRelated: [
        //     { 'Location': (qb: QueryBuilder) => qb.columns('id', 'Name') },
        //     , { 'Delivery': (qb: QueryBuilder) => qb.columns('id', 'Quantity', 'Weight',
        // 'Volume', 'Value', 'Reference') }
        //   ]
        // };
        // if (accounts) {
        return new Promise((resolve, reject) => {
            //     switch (deleted) {
            //       case "ALL":
            //         new schema_main.HandedOver()
            //           .query(function (qb: QueryBuilder) {
            //             qb.join('Deliveries', 'HandedOvers.idDelivery', '=', 'Deliveries.id');
            //             qb.where('HandedOvers.id', id).andWhere('Deliveries.idAccount', 'in', accounts);
            //           }).fetch(fetchPage).then(function (carriers) {
            //             if (carriers)
            //               carriers = carriers.toJSON();
            //             return resolve(carriers);
            //           }).catch(function (error) {
            //             reject(error);
            //           });
            //         break;
            //       case "DELETED":
            //         new schema_main.HandedOverD()
            //           .query(function (qb) {
            //             qb.join('Deliveries', 'V_D_HandedOvers.idDelivery', '=', 'Deliveries.id');
            //             qb.where('V_D_HandedOvers.id', id).andWhere('Deliveries.idAccount', 'in', accounts);
            //           }).fetch(fetchPage).then(function (carriers) {
            //             if (carriers)
            //               carriers = carriers.toJSON();
            //             return resolve(carriers);
            //           }).catch(function (error) {
            //             reject(error);
        });
        //         break;
        //       case "NOT-DELETED":
        //       default:
        //         new schema_main.HandedOverND()
        //           .query(function (qb) {
        //             qb.join('Deliveries', 'V_HandedOvers.idDelivery', '=', 'Deliveries.id');
        //             qb.where('V_HandedOvers.id', id).andWhere('Deliveries.idAccount', 'in', accounts);
        //           }).fetch(fetchPage)
        //           .then(function (carriers) {
        //             if (carriers)
        //               carriers = carriers.toJSON();
        //             return resolve(carriers);
        //           }).catch(function (error) {
        //             reject(error);
        //           });
        //         break;
        //     }
        //   });
        // } else {
        // return new Promise(function (resolve, reject) {
        //   switch (deleted) {
        //     case DeletedParameters.ALL:
        //       new schema_main.HandedOver().where('id', id)
        //         .fetch(fetchPage).then(function (carriers: any) {
        //           if (carriers)
        //             carriers = carriers.toJSON();
        //           return resolve(carriers);
        //         }).catch(function (error: any) {
        //           reject(error);
        //         });
        //       break;
        //     case DeletedParameters.DELETED:
        //       new schema_main.HandedOverD().where('id', id)
        //         .fetch(fetchPage).then(function (carriers: any) {
        //           if (carriers)
        //             carriers = carriers.toJSON();
        //           return resolve(carriers);
        //         }).catch(function (error: any) {
        //           reject(error);
        //         });
        //       break;
        //     case DeletedParameters["NOT-DELETED"]:
        //     default:
        //       new schema_main.HandedOverND().where('id', id)
        //         .fetch(fetchPage)
        //         .then(function (carriers: any) {
        //           if (carriers)
        //             carriers = carriers.toJSON();
        //           return resolve(carriers);
        //         }).catch(function (error: any) {
        //           reject(error);
        //         });
        //       break;
        //   }
        // });
        // }
    }

    /**
     * Get all HandedOvers.
     * Get all HandedOvers.
     *
     * skip Integer number of item to skip
     * limit Integer max records to return
     * orderBy String order by property. (optional)
     * filter String filter data. (optional)
     * returns List
     */
    public static getHandedOvers(skip: number, limit: number, orderBy: string
                            ,    filter: string, deleted: DeletedParameters
                            ,    metadata: boolean, accounts: string) {
        const FUNCTION_NAME = "get";
        if (limit === 0) {
            limit = 10;
        }
        // var fetchPage = {
        //   limit: limit,
        //   offset: skip,
        //   withRelated: [
        //     { 'Location': (qb: QueryBuilder) => qb.columns('id', 'Name') },
        //   ]
        // };
        // if (accounts) {
        //   return new Promise(function (resolve, reject) {
        //     switch (deleted) {
        //       case DeletedParameters.ALL:
        //         new schema_main.HandedOver().orderBy(orderBy)
        //           .query(function (qb: QueryBuilder) {
        //             qb.join('Deliveries', 'HandedOvers.idDelivery', '=', 'Deliveries.id');
        //             qb.where('Deliveries.idAccount', 'in', accounts);
        //           }).fetchPage(fetchPage).then(function (carriers: any) {
        //             if (carriers)
        //               carriers = carriers.toJSON();
        //             return resolve(carriers);
        //           }).catch(function (error: any) {
        //             reject(error);
        //           });
        //         break;
        //       case DeletedParameters.DELETED:
        //         new schema_main.HandedOverD().orderBy(orderBy)
        //           .query(function (qb: QueryBuilder) {
        //             qb.join('Deliveries', 'V_D_HandedOvers.idDelivery', '=', 'Deliveries.id');
        //             qb.where('Deliveries.idAccount', 'in', accounts);
        //           }).fetchPage(fetchPage).then(function (carriers: any) {
        //             if (carriers)
        //               carriers = carriers.toJSON();
        //             return resolve(carriers);
        //           }).catch(function (error: any) {
        //             reject(error);
        //           });
        //         break;
        //       case DeletedParameters["NOT-DELETED"]:
        //       default:
        //         new schema_main.HandedOverND().orderBy(orderBy)
        //           .query(function (qb: QueryBuilder) {
        //             qb.join('Deliveries', 'V_HandedOvers.idDelivery', '=', 'Deliveries.id');
        //             qb.where('Deliveries.idAccount', 'in', accounts);
        //           }).fetchPage(fetchPage)
        //           .then(function (carriers: any) {
        //             if (carriers)
        //               carriers = carriers.toJSON();
        //             return resolve(carriers);
        //           }).catch(function (error: any) {
        //             reject(error);
        //           });
        //         break;
        //     }
        //   });
        // } else {
        return new Promise((resolve, reject) => {
            //     switch (deleted) {
            //       case DeletedParameters.ALL:
            //         new schema_main.HandedOver().orderBy(orderBy)
            //           .fetchPage(fetchPage).then(function (carriers: any) {
            //             if (carriers)
            //               carriers = carriers.toJSON();
            //             return resolve(carriers);
            //           }).catch(function (error: any) {
            //             reject(error);
            //           });
            //         break;
            //       case DeletedParameters.DELETED:
            //         new schema_main.HandedOverD().orderBy(orderBy)
            //           .fetchPage(fetchPage).then(function (carriers: any) {
            //             if (carriers)
            //               carriers = carriers.toJSON();
            //             return resolve(carriers);
            //           }).catch(function (error:any) {
            //             reject(error);
            //           });
            //         break;
            //       case DeletedParameters["NOT-DELETED"]:
            //       default:
            //         new schema_main.HandedOverND().orderBy(orderBy)
            //           .fetchPage(fetchPage)
            //           .then(function (carriers: any) {
            //             if (carriers)
            //               carriers = carriers.toJSON();
            //             return resolve(carriers);
            //           }).catch(function (error: any) {
            //             reject(error);
            //           });
            //         break;
            //     }
        });
    }
}
