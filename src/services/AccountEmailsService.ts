"use strict";

import { AccountEmails } from "../models";
import { DeletedParameters } from "../models/all/parameters";

export class AccountEmailsService {

    /**
     * Add one AccountEmail.
     * Add one AccountEmail.
     *
     * handedOvers AccountEmails
     * returns AccountEmails
     */
    public static addAccountEmails(account: AccountEmails) {
        const FUNCTION_NAME = "add";
        delete account.id;
        return new Promise(async (resolve, reject) => {
            // schema_main.Account.create(account).then(function (result:any) {
            //   delete result.attributes.createdAt;
            //   delete result.attributes.updatedAt;
            //   delete result.attributes.deleted;
            //   return resolve(result);
            // }).catch(function (error:any) {
            //   LoggerUtility.log(error);
            //   reject(error);
            // });
        });
    }

    /**
     * Add a deleted  AccountEmail.
     * Add a deleted  AccountEmail.
     *
     * id Long id to delete
     * returns Long
     */
    public static addDeletedAccountEmail(id: number) {
        const FUNCTION_NAME = "addDeleted";
        return new Promise(async (resolve, reject) => {
            // schema_main.Account.create({id: id, deleted: 1}).then(function (result:any) {
            //   delete result.attributes.createdAt;
            //   delete result.attributes.updatedAt;
            //   delete result.attributes.deleted;
            //   return resolve(result);
            // }).catch(function (error:any) {
            //   LoggerUtility.log(error);
            //   reject(error);
            // });
        });
    }

    /**
     * Delete one AccountEmail.
     * Delete one AccountEmail.
     *
     * id Long id to delete
     * returns Long
     */
    public static deleteAccountEmail(id: number) {
        const FUNCTION_NAME = "delete";
        return new Promise(async (resolve, reject) => {
            // schema_main.Account.create({id: id, deleted: 1}).then(function (result:any) {
            //   delete result.attributes.createdAt;
            //   delete result.attributes.updatedAt;
            //   delete result.attributes.deleted;
            //   return resolve(result);
            // }).catch(function (error:any) {
            //   LoggerUtility.log(error);
            //   reject(error);
            // });
        });
    }

    /**
     * Edit one AccountEmail.
     * Edit one AccountEmail.
     *
     * handedOvers AccountEmails
     * returns AccountEmails
     */
    public static editAccountEmail(account: AccountEmails) {
        const FUNCTION_NAME = "edit";
        return new Promise((resolve, reject) => {
            // schema_main.Account.create(account).then(function (result:any) {
            //   delete result.attributes.createdAt;
            //   delete result.attributes.updatedAt;
            //   delete result.attributes.deleted;
            //   return resolve(result);
            // }).catch(function (error:any) {
            //   LoggerUtility.log(error);
            //   reject(error);
            // });
        });
    }

    /**
     * Get one AccountEmail.
     * Get one AccountEmail.
     *
     * id Long id to delete
     * returns AccountEmails
     */
    public static getAccountEmailById(id: number, deleted: DeletedParameters) {
        const FUNCTION_NAME = "get";
        return new Promise(async (resolve, reject) => {
            // switch(deleted){
            //   case DeletedParameters.ALL:
            //     new schema_main.Account().where('id', id)
            //     .fetch({columns:['id','Name']}).then(function (carrier:any) {
            //     if (carrier){
            //       carrier = carrier.toJSON();
            //     }
            //     return resolve(carrier);
            //   }).catch(function (error:any) {
            //     LoggerUtility.log(error);
            //     reject(error);
            //   });
            //   break;
            //   case DeletedParameters.DELETED:
            //   new schema_main.AccountD().where('id', id)
            //   .fetch({columns:['id','Name']}).then(function (carrier:any) {
            //   if (carrier){
            //     carrier = carrier.toJSON();
            //   }
            //   return resolve(carrier);
            // }).catch(function (error:any) {
            //   LoggerUtility.log(error);
            //   reject(error);
            // });
            //   break;
            //   case DeletedParameters["NOT-DELETED"]:
            //   default:
            //   new schema_main.AccountND().where('id', id)
            //   .fetch({columns:['id','Name']}).then(function (carrier:any) {
            //   if (carrier){
            //     carrier = carrier.toJSON();
            //   }
            //   return resolve(carrier);
            // }).catch(function (error:any) {
            //   LoggerUtility.log(error);
            //   reject(error);
            // });
            //   break;
            // }
        });
    }

    /**
     * Get all AccountEmails.
     * Get all AccountEmails.
     *
     * skip Integer number of item to skip
     * limit Integer max records to return
     * orderBy String order by property. (optional)
     * filter String filter data. (optional)
     * returns List
     */
    public static getAccountEmails(skip: number, limit: number
                                ,  orderBy: string, filter: string
                                ,  deleted: DeletedParameters, metadata: boolean) {
        const FUNCTION_NAME = "get";
        if (limit === 0) {
            limit = 10;
        }
        return new Promise(async (resolve, reject) => {
            // switch(deleted){
            //   case DeletedParameters.ALL:
            //     new schema_main.Account().orderBy(orderBy).fetchPage({
            //       limit: limit,
            //       offset: skip,
            //       columns:['id','Name']
            //     }).then(function (carriers:any) {
            //       if (carriers)
            //         carriers = carriers.toJSON();
            //       return resolve(carriers);
            //     }).catch(function (error:any) {
            //       reject(error);
            //     });
            //   break;
            //   case DeletedParameters.DELETED:
            //     new schema_main.AccountD().orderBy(orderBy).fetchPage({
            //       limit: limit,
            //       offset: skip,
            //       columns:['id','Name']
            //     }).then(function (carriers:any) {
            //       if (carriers)
            //         carriers = carriers.toJSON();
            //       return resolve(carriers);
            //     }).catch(function (error:any) {
            //       reject(error);
            //     });
            //   break;
            //   case DeletedParameters["NOT-DELETED"]:
            //   default:
            //     new schema_main.AccountND().orderBy(orderBy).fetchPage({
            //       limit: limit,
            //       offset: skip,
            //       columns:['id','Name']
            //     }).then(function (carriers:any) {
            //       if (carriers)
            //         carriers = carriers.toJSON();
            //       return resolve(carriers);
            //     }).catch(function (error:any) {
            //       reject(error);
            //     });
            //   break;
            // }
        });
    }
}
