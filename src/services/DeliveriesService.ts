"use strict";

import _ from "lodash";
import { FindManyOptions } from "typeorm";
import { DatabaseDeliveries } from "../database";
import { Deliveries } from "../models";
import { DeletedParameters } from "../models/all/parameters";
import { LoggerUtility } from "../utils/LoggerUtility";
import { Utilities } from "../utils/utilities";
import { VALID_RESPONSES } from "../utils/ValidResponses";
import { AccountsService } from "./AccountsService";
import { LocationsService } from "./LocationsService";

const SERVICE_NAME = "AccountsService";

export class DeliveriesService {

    public static async checkIfExists(id: number): Promise<string> {
        const FUNCTION_NAME = "checkIfExists";
        if (id) {
            try {
                await DeliveriesService.getDeliveryById(id, DeletedParameters["NOT-DELETED"]);
                return null;
            } catch (reason) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, reason, id);
                return reason;
            }
        }
        return null;
    }

    public static async checkDependencies(delivery: Deliveries): Promise<string> {
        const FUNCTION_NAME = "check";
        LoggerUtility.debug(SERVICE_NAME, FUNCTION_NAME, delivery);
        const promises = [];
        promises.push(AccountsService.checkIfExists(delivery.idAccount));
        promises.push(LocationsService.checkIfExists(delivery.idLocation));
        try {
            await Promise.all(promises);
        } catch (reason) {
            LoggerUtility.debug(SERVICE_NAME, FUNCTION_NAME, reason);
            return reason;
        }
        return null;
    }

    /**
     * Add one Delivery.
     * Add one Delivery.
     *
     * deliveries Deliveries
     * returns Deliveries
     */
    public static addDelivery(delivery: Deliveries) {
        const FUNCTION_NAME = "add";
        return new Promise(async (resolve, reject) => {
            delete delivery.id;
            const validationError = delivery.isValid();
            if (validationError) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "data not valid", validationError, delivery);
                reject(validationError);
                return;
            }
            const result = await DeliveriesService.checkDependencies(delivery);
            if (result) {
                reject(result);
                return;
            }
            let newDelivery: DatabaseDeliveries = await DatabaseDeliveries
                .findOne({ where: { idAccount: delivery.idAccount, idLocation: delivery.idLocation } });
            if (newDelivery) {
                if (newDelivery.deleted) {
                    LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "found deleted", newDelivery.id);
                    delivery.id = newDelivery.id;
                } else {
                    LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "already created"
                        , VALID_RESPONSES.ERROR.EXIST, newDelivery);
                    reject(VALID_RESPONSES.ERROR.EXIST);
                    return;
                }
            }
            newDelivery = await DatabaseDeliveries.save(new DatabaseDeliveries(null, delivery));
            if (newDelivery) {
                const dataMembership = await DatabaseDeliveries.findOne(Utilities.
                    getFindOneObject(newDelivery.id, DeletedParameters["NOT-DELETED"], new DatabaseDeliveries()));
                LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "created with id ", newDelivery.id);
                resolve(new Deliveries(null, dataMembership));
                return;
            }
            LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "NOT created ", delivery);
            reject(VALID_RESPONSES.ERROR.UNRECOGNIZED);
            return;
        });
    }

    /**
     * Add deleted Delivery.
     * Add deleted Delivery.
     *
     * id Long id to delete
     * returns Long
     */
    public static addDeletedDelivery(id: number) {
        const FUNCTION_NAME = "addDeleted";
        return new Promise(async (resolve, reject) => {
            // TODO: see if it is needed.
        });
    }

    /**
     * Delete one Delivery.
     * Delete one Delivery.
     *
     * id Long id to delete
     * returns Long
     */
    public static deleteDelivery(id: number) {
        const FUNCTION_NAME = "delete";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, id);
            const delivery = await DatabaseDeliveries.findOne({ where: { id, deleted: false } });
            if (!delivery) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, VALID_RESPONSES.ERROR.NOT_EXIST.DELIVERY, id);
                reject(VALID_RESPONSES.ERROR.NOT_EXIST.DELIVERY);
                return;
            }
            delivery.deleted = true;
            await delivery.save();
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "deleted", id);
            resolve({ id: delivery.id });
            return;
        });
    }

    /**
     * Edit one Delivery.
     * Edit one Delivery.
     *
     * deliveries Deliveries
     * returns Deliveries
     */
    public static editDelivery(delivery: Deliveries) {
        const FUNCTION_NAME = "edit";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME);
            if (!delivery.id) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME
                    , "id not valid", VALID_RESPONSES.ERROR.VALIDATION.ID, delivery.id);
                reject(VALID_RESPONSES.ERROR.VALIDATION.ID);
                return;
            }
            LoggerUtility.debug(SERVICE_NAME, FUNCTION_NAME, delivery);
            const prevMembership = await DatabaseDeliveries.findOne({ where: { id: delivery.id } });
            if (!prevMembership) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, VALID_RESPONSES.ERROR.NOT_EXIST.MAIN, delivery.id);
                reject(VALID_RESPONSES.ERROR.NOT_EXIST.MAIN);
                return;
            }
            const validationError = delivery.isValid();
            if (validationError) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "data not valid", validationError, delivery);
                reject(validationError);
                return;
            }
            const result = await DeliveriesService.checkDependencies(delivery);
            if (result) {
                reject(result);
                return;
            }
            let newDelivery = await DatabaseDeliveries
                .findOne({ where: { idAccount: delivery.idAccount, idLocation: delivery.idLocation } });
            if (newDelivery && newDelivery.id !== delivery.id) {
                // if(newDelivery.deleted){
                //   LoggerUtility.info(SERVICE_NAME,FUNCTION_NAME, "found membership deleted", newDelivery.id);
                //   membership.id = newDelivery.id;
                // }else{
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME
                    , "already created", VALID_RESPONSES.ERROR.EXIST, newDelivery);
                reject(VALID_RESPONSES.ERROR.EXIST);
                return;
                // }
            }
            newDelivery = await DatabaseDeliveries.save(new DatabaseDeliveries(null, delivery));
            if (newDelivery) {
                const dataMembership = await DatabaseDeliveries.findOne(Utilities.
                    getFindOneObject(newDelivery.id, DeletedParameters["NOT-DELETED"], new DatabaseDeliveries()));
                LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "edited with id ", newDelivery.id);
                resolve(new Deliveries(null, dataMembership));
                return;
            }
            LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "NOT edited ", delivery);
            reject(VALID_RESPONSES.ERROR.UNRECOGNIZED);
            return;
        });
    }

    /**
     * Get all Deliveries.
     * Get all Deliveries.
     *
     * skip Integer number of item to skip
     * limit Integer max records to return
     * orderBy String order by property. (optional)
     * filter String filter data. (optional)
     * returns List
     */
    public static getDeliveries(skip: number, limit: number, orderBy: string
                            ,   filter: string, deleted: DeletedParameters
                            ,   metadata: boolean, accounts?: Array<number>) {
        const FUNCTION_NAME = "get";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME);
            // TODO: find the way to put where idAccount in array provided.
            const object: FindManyOptions<DatabaseDeliveries> =
                Utilities.getFindObject(skip, limit, orderBy, filter, deleted, new DatabaseDeliveries(), null);
            if (!object) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "order param malformed", orderBy);
                reject(VALID_RESPONSES.ERROR.PARAMS.MALFORMED.ORDERBY);
                return;
            }
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "with", object);
            const [memberships, total] = await DatabaseDeliveries.findAndCount(object);
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "got", total);
            if (!memberships || !memberships.length) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "empty result with object", object);
                resolve();
                return;
            }
            const apiMemberships = [];
            for (const us of memberships) {
                apiMemberships.push(new Deliveries(null, us));
            }
            resolve(Utilities.getMetadataFormat(apiMemberships, skip, limit, total, metadata));
            return;
        });
    }

    /**
     * Get one Delivery.
     * Get one Delivery.
     *
     * id Long id to delete
     * returns Deliveries
     */
    public static getDeliveryById(id: number, deleted: DeletedParameters, accounts?: Array<number>) {
        const FUNCTION_NAME = "getById";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME);
            // TODO: find the way to put where idAccount in array provided.
            const prevDelivery = await DatabaseDeliveries.findOne(
                Utilities.getFindOneObject(id, deleted, new DatabaseDeliveries()));
            if (!prevDelivery) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME
                    , "not exists with id", id, "and deleted", deleted.toString());
                reject(VALID_RESPONSES.ERROR.NOT_EXIST.DELIVERY);
                return;
            }
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "got", prevDelivery.id);
            resolve(new Deliveries(null, prevDelivery));
            return;
        });
    }
}
