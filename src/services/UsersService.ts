"use strict";

import * as bcrypt from "bcrypt";
import _ from "lodash";
import { FindManyOptions } from "typeorm";
import { DatabaseMemberships, DatabaseUsers } from "../database";
import { Users } from "../models";
import { DeletedParameters } from "../models/all/parameters";
import { JwtUtils } from "../utils/jwt-util";
import { LoggerUtility } from "../utils/LoggerUtility";
import { Utilities } from "../utils/utilities";
import { VALID_RESPONSES } from "../utils/ValidResponses";

const SERVICE_NAME = "UsersService";
export class UsersService {

    public static async checkIfExists(id: number): Promise<string> {
        if (id) {
            const FUNCTION_NAME = "checkIfExists";
            try {
                await UsersService.getUserById(id, DeletedParameters["NOT-DELETED"]);
                return null;
            } catch (reason) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, reason, id);
                return reason;
            }
        }
        return null;
    }

    /**
     * Add one User.
     * Add one User.
     *
     * handedOvers Users
     * returns Users
     */
    public static addUser(user: Partial<Users>): Promise<Users> {
        const FUNCTION_NAME = "add";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, user.name);
            delete user.id;
            delete user.deleted;
            user.emailVerified = false;
            let validationError: string;
            if (!user.password) {
                user.passwordReset = true;
                validationError = user.isValid(false);
            } else {
                user.passwordReset = false;
                validationError = user.isValid();
            }
            if (validationError) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "User data not valid", validationError, user);
                reject(validationError);
                return;
            }
            let prevUser = await DatabaseUsers.findOne({ where: { Email: user.email } });
            if (prevUser) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "email already exists", user.email);
                reject(VALID_RESPONSES.ERROR.VALIDATION.USER.REGISTERED.EMAIL);
                return;
            }
            if (user.identification) {
                prevUser = await DatabaseUsers.findOne({ where: { Identification: user.identification } });
                if (prevUser) {
                    LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME
                        , "identification already exists ", user.identification);
                    reject(VALID_RESPONSES.ERROR.VALIDATION.USER.REGISTERED.IDENTIFICATION);
                    return;
                }
            }
            const newUser: DatabaseUsers = await DatabaseUsers.save(new DatabaseUsers(null, user));
            if (newUser) {
                LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "created with id ", newUser.id);
                resolve(new Users(null, newUser));
                return;
            }
            LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "NOT created ", user);
            reject(VALID_RESPONSES.ERROR.UNRECOGNIZED);
            return;
        });
    }

    /**
     * Add a deleted User.
     * Add a deleted User.
     *
     * id Long id to delete
     * returns Long
     */
    public static addDeletedUser(id: number): Promise<object> {
        const FUNCTION_NAME = "addDeleted";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, id);
            const user = await DatabaseUsers.findOne({ where: { id } });
            if (!user) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "not exists with id", id);
                reject(VALID_RESPONSES.ERROR.NOT_EXIST);
                return;
            }
            if (!user.deleted) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "not deleted with id", id);
                reject(VALID_RESPONSES.ERROR.NOT_DELETED);
                return;
            }
            user.deleted = false;
            await user.save();
            resolve({ id: user.id });
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "added", id);
            return;
        });
    }

    /**
     * Delete one Users.
     * Delete one User.
     *
     * id Long id to delete
     * returns Long
     */
    public static deleteUsers(id: number): Promise<object> {
        const FUNCTION_NAME = "delete";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, id);
            const user = await DatabaseUsers.findOne({ where: { id, deleted: false } });
            if (!user) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, VALID_RESPONSES.ERROR.NOT_EXIST.USER, id);
                reject(VALID_RESPONSES.ERROR.NOT_EXIST.USER);
                return;
            }
            user.deleted = true;
            await user.save();
            resolve({ id: user.id });
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "deleted", id);
            return;
        });
    }

    /**
     * Edit one User.
     * Edit one User.
     *
     * handedOvers Users
     * returns Users
     */
    public static editUser(user: Users): Promise<Users> {
        const FUNCTION_NAME = "edit";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, user.id);
            if (!user.role) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "missing role"
                , VALID_RESPONSES.ERROR.VALIDATION.USER.ROLE, user);
                reject(VALID_RESPONSES.ERROR.VALIDATION.USER.ROLE);
                return;
            }
            if (!user.id) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME
                    , "User id not valid", VALID_RESPONSES.ERROR.VALIDATION.ID, user.id);
                reject(VALID_RESPONSES.ERROR.VALIDATION.ID);
                return;
            }
            const prevUser = await DatabaseUsers.findOne({ where: { id: user.id } });
            if (!prevUser) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, VALID_RESPONSES.ERROR.NOT_EXIST.MAIN, user.id);
                reject(VALID_RESPONSES.ERROR.NOT_EXIST.USER);
                return;
            }
            if (!user.isEmailValid()) {
                // LoggerUtility.warn(SERVICE_NAME,FUNCTION_NAME, "User data not valid",
                // VALID_RESPONSES.ERROR.VALIDATION.USER.EMAIL, user.email);
                // reject(VALID_RESPONSES.ERROR.VALIDATION.USER.EMAIL);
                // return;
            } else {
                const prevUserEmail = await DatabaseUsers.findOne({ where: { Email: user.email } });
                if (prevUserEmail && prevUserEmail.id !== user.id) {
                    LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME
                        , "new email already exists with id", prevUserEmail.id, prevUserEmail.Email);
                    reject(VALID_RESPONSES.ERROR.VALIDATION.USER.REGISTERED.EMAIL);
                    return;
                }
                prevUser.Email = user.email;
                prevUser.verifiedEmail = false;
            }
            if (user.identification) {
                const prevUserIdent = await DatabaseUsers.findOne({ where: { Identification: user.identification } });
                if (prevUserIdent && prevUserIdent.id !== user.id) {
                    LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME
                        , "new identification already exists with id", prevUserIdent.id, prevUserIdent.Identification);
                    reject(VALID_RESPONSES.ERROR.VALIDATION.USER.REGISTERED.IDENTIFICATION);
                    return;
                }
            }
            if (user.identification || user.identification === "") {
                prevUser.Identification = user.identification;
            }
            prevUser.Type = user.role.toString();
            if (user.name || user.name === "") {
                prevUser.Name = user.name;
            }
            prevUser.deleted = user.deleted;
            if (user.emailVerified || user.emailVerified === false) {
                prevUser.verifiedEmail = user.emailVerified;
            }
            await prevUser.save();
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "edited", user.id);
            resolve(new Users(null, prevUser));
            return;
        });
    }

    /**
     * Get one User.
     * Get one User.
     *
     * id Long id to delete
     * returns Users
     */
    public static getUserById(id: number, deleted: DeletedParameters): Promise<Users> {
        const FUNCTION_NAME = "getById";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, id);
            const prevUser = await DatabaseUsers.findOne(Utilities.getFindOneObject(id, deleted, new DatabaseUsers()));
            if (!prevUser) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME
                    , "not exists with id", id, "and deleted", deleted.toString());
                reject(VALID_RESPONSES.ERROR.NOT_EXIST.USER);
                return;
            }
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "got", prevUser.id);
            resolve(new Users(null, prevUser));
            return;
        });
    }

    /**
     * Get all Users.
     * Get all Users.
     *
     * skip Integer number of item to skip
     * limit Integer max records to return
     * orderBy String order by property. (optional)
     * filter String filter data. (optional)
     * returns List<Users>
     */
    public static getUsers(
        skip: number, limit: number,
        orderBy: string, filter: string,
        deleted: DeletedParameters, metadata: boolean): Promise<Array<Users>> {
        const FUNCTION_NAME = "get";
        return new Promise(async (resolve: any, reject: any) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME);
            const object: FindManyOptions<DatabaseUsers> = Utilities.
                getFindObject(skip, limit, orderBy, filter, deleted, new DatabaseUsers());
            if (!object) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "order param malformed", orderBy);
                reject(VALID_RESPONSES.ERROR.PARAMS.MALFORMED.ORDERBY);
                return;
            }
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "with", object);
            const [users, total] = await DatabaseUsers.findAndCount(object);
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "got", total);
            if (!users || !users.length) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "empty result with object", object);
                resolve();
                return;
            }
            const apiUsers = [];
            for (const us of users) {
                apiUsers.push(new Users(null, us));
            }
            resolve(Utilities.getMetadataFormat(apiUsers, skip, limit, total, metadata));
            return;
        });
    }

    /**
     * Logs user into the system
     *
     * email String The user email for login
     * password String The password for login in clear text
     * returns LoginObject
     */
    public static login(email: string, password: string) {
        const FUNCTION_NAME = "login";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, email);
            const user = await DatabaseUsers.findOne({ where: { Email: email } });
            if (!user) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "Email does not exists", email);
                reject(VALID_RESPONSES.ERROR.EMAIL.NOT_REGISTERED);
                return;
            }
            if (!user.verifiedEmail) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "Email not verified", user.id);
                reject(VALID_RESPONSES.ERROR.EMAIL.NOT_VERIFIED);
                return;
            }
            if (user.resetPassword) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "Password reset", user.id);
                reject(VALID_RESPONSES.ERROR.PASSWORD.RESET);
                return;
            }
            const validPassword = bcrypt.compareSync(password, user.PasswordHash);
            if (!validPassword) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "Password not matched", user.id);
                reject(VALID_RESPONSES.ERROR.PASSWORD.NOT_MATCHED);
                return;
            }
            _.forEach(user.memberships || [], (membership: DatabaseMemberships) => {
                delete membership.id;
                delete membership.idUser;
            });
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "user logged", user.id);
            resolve(JwtUtils.signData(new Users(null, user)));
            return;
        });
    }

    /**
     * Logs user into the system by providing token
     *
     * token String The access token
     * returns LoginObject
     */
    public static loginToken(token: string) {
        const FUNCTION_NAME = "loginToken";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME);
            const data = JwtUtils.getData(token);
            if (!data) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "invalid token", token);
                reject(VALID_RESPONSES.ERROR.EMAIL.NOT_REGISTERED);
                return;
            }
            const user = await DatabaseUsers.findOne({ where: { id: data.data.id } });
            if (!user) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "user does not exists", data.data.id);
                reject(VALID_RESPONSES.ERROR.EMAIL.NOT_REGISTERED);
                return;
            }
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "logged", user.id);
            resolve(JwtUtils.signData(new Users(null, user)));
            return;
        });
    }

    /**
     * Get user info by token
     *
     * token String The access token
     * returns LoginObject
     */
    public static verifyToken(token: string | Array<string>) {
        const FUNCTION_NAME = "verifyToken";
        return new Promise((resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME);
            // LoggerUtility.info(SERVICE_NAME,FUNCTION_NAME,"token", token);
            let tokenGot;
            if (token instanceof Array) {
                tokenGot = token[0];
            } else {
                tokenGot = token;
            }
            // LoggerUtility.info(SERVICE_NAME,FUNCTION_NAME,"tokenGot", tokenGot);
            const verifiedToken: any = JwtUtils.getData(tokenGot);
            if (verifiedToken) {
                LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "verified!", verifiedToken.data.id);
                resolve(JwtUtils.signData(verifiedToken.data));
                return;
            } else {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "not verified");
                return reject(VALID_RESPONSES.ERROR.AUTH);
            }
        });
    }

    /**
     * Logs out current logged in user session
     *
     * returns String
     */
    public static logout() {
        return new Promise((resolve, reject) => {
            resolve();
            return;
        });
    }

}
