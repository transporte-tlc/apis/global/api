"use strict";

import { DatabaseUsers } from "../database";
import { Users } from "../models";
import { JwtUtils } from "../utils/jwt-util";
import { LoggerUtility } from "../utils/LoggerUtility";
import { Utilities } from "../utils/utilities";
import { VALID_RESPONSES } from "../utils/ValidResponses";

const SERVICE_NAME = "PasswordService";

export class PasswordService {
    /**
     * Change user password by providing old one.
     *
     * oldPassword String Old Password
     * newPassword String New Password
     * userId String User to change password. Only for admin users. (optional)
     * no response value expected for this operation
     */
    public static changePassword(oldPassword: string, newPassword: string, userId: number) {
        const FUNCTION_NAME = "change";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, userId);
            if (oldPassword === newPassword) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "passwords are equals", userId);
                reject(VALID_RESPONSES.ERROR.PASSWORD.MATCHED);
                return;
            }
            let user = new Users({ password: newPassword });
            if (!user.isPasswordValid()) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "password insecure", userId);
                reject(VALID_RESPONSES.ERROR.PASSWORD.INSECURE);
                return;
            }
            const userDatabase = await DatabaseUsers.findOne({ where: { id: userId, deleted: false } });
            if (!userDatabase) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "user not registered", userId);
                reject(VALID_RESPONSES.ERROR.NOT_EXIST.USER);
                return;
            }
            if (!userDatabase.isSamePassword(oldPassword)) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "old password not match", userId);
                reject(VALID_RESPONSES.ERROR.PASSWORD.NOT_MATCHED);
                return;
            }
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, userId, "saving new password");
            user = new Users(null, userDatabase);
            user.password = newPassword;
            const editedUser = await DatabaseUsers.save(new DatabaseUsers(null, user));
            if (editedUser) {
                LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "user password changed", userId);
                resolve("OK");
                return;
            }
            LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "unrecognized error", userId);
            reject(VALID_RESPONSES.ERROR.UNRECOGNIZED);
            return;
        });
    }

    /**
     * Change user password with a token.
     *
     * token String The access token
     * password String The access token
     * no response value expected for this operation
     */
    public static changePasswordWithToken(token: string, password: string) {
        const FUNCTION_NAME = "changeWithToken";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME);
            let user = new Users({ password });
            if (!user.isPasswordValid()) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "password insecure");
                reject(VALID_RESPONSES.ERROR.PASSWORD.INSECURE);
                return;
            }
            const unsignedData: any = JwtUtils.getData(token, { ignoreExpiration: true });
            if (!unsignedData || !unsignedData.data) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "token not recognized", token);
                reject(VALID_RESPONSES.ERROR.PASSWORD.VERIFICATION.TOKEN.UNRECOGNIZED);
                return;
            }
            if (unsignedData.exp && unsignedData.exp < Date.now().valueOf() / 1000) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "token expired", token, unsignedData.exp);
                reject(VALID_RESPONSES.ERROR.PASSWORD.VERIFICATION.TOKEN.EXPIRED);
                return;
            }
            const userDatabase = await DatabaseUsers.findOne({ where: { id: unsignedData.data.id, deleted: false } });
            if (!userDatabase) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "user not registered", unsignedData.data.id);
                reject(VALID_RESPONSES.ERROR.NOT_EXIST);
                return;
            }
            user = new Users(null, userDatabase);
            user.password = password;
            const editedUser = await DatabaseUsers.save(new DatabaseUsers(null, user));
            if (editedUser) {
                LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "user password changed", user.id);
                resolve("OK");
                return;
            }
            LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "unrecognized error", user.id);
            reject(VALID_RESPONSES.ERROR.UNRECOGNIZED);
            return;
        });
    }

    /**
     * Request a change of password by email.
     *
     * email String User's email.
     * no response value expected for this operation
     */
    public static requestPassword(email: string, urlResponse: string) {
        const FUNCTION_NAME = "request";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME);
            const user = new Users({ email });
            if (!user.isEmailValid()) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "invalid email", email);
                reject(VALID_RESPONSES.ERROR.VALIDATION.USER.EMAIL);
                return;
            }
            if (!Utilities.checkUrl(urlResponse)) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "malformed url", urlResponse);
                reject(VALID_RESPONSES.ERROR.VALIDATION.URL);
                return;
            }
            const userDatabase = await DatabaseUsers.findOne({ where: { Email: email, deleted: false } });
            if (!userDatabase) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "user not registered", email);
                reject(VALID_RESPONSES.ERROR.NOT_EXIST.USER);
                return;
            }
            // TODO: send email for request new password.
            resolve("OK");
        });
    }

}
