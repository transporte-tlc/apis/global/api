"use strict";

import { DatabaseAccounts } from "../database/main/models/DatabaseAccounts";
import { Accounts } from "../models";
import { DeletedParameters } from "../models/all/parameters";
import { LoggerUtility } from "../utils/LoggerUtility";
import { Utilities } from "../utils/utilities";
import { VALID_RESPONSES } from "../utils/ValidResponses";

const SERVICE_NAME = "AccountsService";

export class AccountsService {

    public static async checkIfExists(id: number): Promise<string> {
        if (id) {
            const FUNCTION_NAME = "checkIfExists";
            try {
                await AccountsService.getAccountById(id, DeletedParameters["NOT-DELETED"]);
                return null;
            } catch (reason) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, reason, id);
                return reason;
            }
        }
        return null;
    }

    /**
     * Add one Account.
     * Add one Account.
     *
     * accounts Accounts
     * returns Accounts
     */
    public static addAccount(account: Accounts): Promise<Accounts> {
        const FUNCTION_NAME = "add";
        return new Promise(async (resolve, reject) => {
            delete account.id;
            delete account.memberships;
            delete account.accountEmails;
            account.deleted = false;
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, account.name);
            const prevAccount = await DatabaseAccounts.findOne({ where: { Name: account.name } });
            if (prevAccount) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "name already exists", prevAccount.Name);
                reject(VALID_RESPONSES.ERROR.EXIST);
                return;
            }
            const newAccount = await DatabaseAccounts.save(new DatabaseAccounts(null, account));
            if (newAccount) {
                LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "created with id ", newAccount.id);
                resolve(new Accounts(null, newAccount));
                return;
            }
            LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "NOT created ", account);
            reject(VALID_RESPONSES.ERROR.UNRECOGNIZED);
            return;
        });
    }

    /**
     * Add a deleted Account.
     * Add a deleted Account.
     *
     * id Long id to delete
     * returns Long
     */
    public static addDeletedAccount(id: number) {
        // TODO: addDeletedAccount
        const FUNCTION_NAME = "addDeleted";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, id);
            const account = await DatabaseAccounts.findOne({ where: { id } });
            if (!account) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "not exists with id", id);
                reject(VALID_RESPONSES.ERROR.NOT_EXIST);
                return;
            }
            if (!account.deleted) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "not deleted with id", id);
                reject(VALID_RESPONSES.ERROR.NOT_DELETED);
                return;
            }
            account.deleted = false;
            await account.save();
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "added", id);
            resolve({ id: account.id });
            return;
        });
    }

    /**
     * Delete one Account.
     * Delete one Account.
     *
     * id Long id to delete
     * returns Long
     */
    public static deleteAccount(id: number) {
        const FUNCTION_NAME = "delete";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, id);
            const account = await DatabaseAccounts.findOne({ where: { id, deleted: false } });
            if (!account) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, VALID_RESPONSES.ERROR.NOT_EXIST.MAIN, id);
                reject(VALID_RESPONSES.ERROR.NOT_EXIST.MAIN);
                return;
            }
            account.deleted = true;
            await account.save();
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "deleted", id);
            resolve({ id: account.id });
            return;
        });
    }

    /**
     * Edit one Account.
     * Edit one Account.
     *
     * accounts Accounts
     * returns Accounts
     */
    public static editAccount(account: Accounts) {
        const FUNCTION_NAME = "edit";
        return new Promise(async (resolve, reject) => {
            delete account.memberships;
            delete account.accountEmails;
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, account.id);
            if (!account.id) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME
                    , "id not valid", VALID_RESPONSES.ERROR.VALIDATION.ID, account.id);
                reject(VALID_RESPONSES.ERROR.VALIDATION.ID);
                return;
            }
            let prevAccount = await DatabaseAccounts.findOne({ where: { Name: account.name } });
            if (prevAccount && prevAccount.id !== account.id) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "name already exists", account.name);
                reject(VALID_RESPONSES.ERROR.VALIDATION.NAME);
                return;
            }
            prevAccount = await DatabaseAccounts.findOne({ where: { id: account.id } });
            if (!prevAccount) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, VALID_RESPONSES.ERROR.NOT_EXIST.MAIN, account.id);
                reject(VALID_RESPONSES.ERROR.NOT_EXIST.MAIN);
                return;
            }
            prevAccount.Name = account.name;
            prevAccount.deleted = account.deleted;
            await prevAccount.save();
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "edited", account.id);
            resolve(new Accounts(null, prevAccount));
            return;
        });
    }

    /**
     * Get one Account.
     * Get one Account.
     *
     * id Long id to delete
     * returns Accounts
     */
    public static getAccountById(id: number, deleted: DeletedParameters) {
        const FUNCTION_NAME = "getById";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME);
            const prevAccount = await DatabaseAccounts
                .findOne(Utilities.getFindOneObject(id, deleted, new DatabaseAccounts()));
            if (!prevAccount) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME
                    , "not exists with id", id, "and deleted", deleted.toString());
                reject(VALID_RESPONSES.ERROR.NOT_EXIST.ACCOUNT);
                return;
            }
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "got", prevAccount.id);
            resolve(new Accounts(null, prevAccount));
            return;
        });
    }

    /**
     * Get all Accounts.
     * Get all Accounts.
     *
     * skip Integer number of item to skip
     * limit Integer max records to return
     * orderBy String order by property. (optional)
     * filter String filter data. (optional)
     * returns List
     */
    public static getAccounts(
        skip: number, limit: number,
        orderBy: string, filter: string,
        deleted: DeletedParameters, metadata: boolean) {
        const FUNCTION_NAME = "get";
        return new Promise(async (resolve, reject) => {
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME);
            const object = Utilities.getFindObject(skip, limit, orderBy,
                filter, deleted, new DatabaseAccounts());
            if (!object) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "order param malformed", orderBy);
                reject(VALID_RESPONSES.ERROR.PARAMS.MALFORMED.ORDERBY);
                return;
            }
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "with", object);
            const [accounts, total] = await DatabaseAccounts.findAndCount(object);
            if (!accounts || !accounts.length) {
                LoggerUtility.warn(SERVICE_NAME, FUNCTION_NAME, "empty result");
                resolve();
                return;
            }
            const apiAccounts = [];
            for (const us of accounts) {
                apiAccounts.push(new Accounts(null, us));
            }
            LoggerUtility.info(SERVICE_NAME, FUNCTION_NAME, "got ", apiAccounts.length);
            resolve(Utilities.getMetadataFormat(apiAccounts, skip, limit, total, metadata));
            return;
        });
    }
}
