"use strict";

import http from "http";
import app from "./app";
import { LoggerUtility } from "./utils/LoggerUtility";

const serverPort = process.env.PORT || 3000;
app.use("port", serverPort);
// Start the server
http.createServer(app).listen(serverPort, () => {
    LoggerUtility.debug("Your server is listening on port %d (http://localhost:%d)", serverPort, serverPort);
    LoggerUtility.debug("Swagger-ui is available on http://localhost:%d/docs", serverPort);
});
