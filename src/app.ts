"use strict";
import * as fs from "fs";
import jsyaml from "js-yaml";
import * as path from "path";
// From TypeORM
import "reflect-metadata";
import { initializeMiddleware, Middleware20, SwaggerRouter20Options } from "swagger-tools";
import { DatabaseInit } from "./database/DatabaseInit";

import * as dotenv from "dotenv";
dotenv.config();

// tslint:disable-next-line: no-var-requires
const app = require("connect")();

// swaggerRouter configuration
const options: SwaggerRouter20Options = {
    // swaggerUi: path.join(__dirname, '/swagger.json'),
    controllers: path.join(__dirname, "./controllers")
    // ,useStubs: false
};

// The Swagger document (require it, build it programmatically, fetch it from a URL, ...)
const spec = fs.readFileSync(path.join(__dirname, "../src/api/swagger.yaml"), "utf8");
const swaggerDoc = jsyaml.safeLoad(spec);

if (process.env.SWAGGER_HOST) {
    swaggerDoc.host = process.env.SWAGGER_HOST;
}
let SWAGGER_BASE_PATH = process.env.SWAGGER_BASE_PATH;
if (!SWAGGER_BASE_PATH) {
    SWAGGER_BASE_PATH = "/";
}
if (process.env.SWAGGER_BASE_PATH) {
    swaggerDoc.basePath = process.env.SWAGGER_BASE_PATH;
}

// Allow cross origin
require("./utils/cors-util")(app);

// Enable JWT tokens
require("./utils/jwt-util").addJWT(app, SWAGGER_BASE_PATH);

// if(process.env.DATABASE_LOCAL && process.env.DATABASE_LOCAL === "1"){
//   console.log("Running with local database.");
//   var shell = require('shelljs');
//   shell.exec('./scripts/run_mysql_local.sh');
// }
// TODO: wait until connection to database is on to initiate swagger.
DatabaseInit.getInstance();

// Initialize the Swagger middleware
initializeMiddleware(swaggerDoc, (middleware: Middleware20) => {
    // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
    app.use(middleware.swaggerMetadata());

    // Validate Swagger requests
    app.use(middleware.swaggerValidator({
        // SwaggerValidatorOptions: true
    }));

    // Route validated requests to appropriate controller
    app.use(middleware.swaggerRouter(options));

    // Serve the Swagger documents and Swagger UI
    app.use(middleware.swaggerUi({
        apiDocs: SWAGGER_BASE_PATH + "api-docs",
        swaggerUi: SWAGGER_BASE_PATH + "docs"
    }));
});

export default app;
// module.exports = app;
