"use strict";

import { JourneysService } from "../services/JourneysService";
import { CustomNext, CustomRequest, CustomResponse } from "../utils/customsHandlers";
import { Utilities } from "../utils/utilities";
import { ResponsePayload } from "../utils/writer";

module.exports.addJourney = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkVariableNotNull("journey", req.swagger.params);
    if (!params) {
        return;
    }
    JourneysService.addJourney(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.addDeletedJourney = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkId(req.swagger.params);
    if (params !== 0 && !params) {
        return;
    }
    JourneysService.addDeletedJourney(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.deleteJourney = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkId(req.swagger.params);
    if (params !== 0 && !params) {
        return;
    }
    JourneysService.deleteJourney(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.editJourney = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkVariableNotNull("journey", req.swagger.params);
    if (!params) {
        return;
    }
    JourneysService.editJourney(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.getJourneyById = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params = Utilities.checkIdAndDelete(req.swagger.params, res);
    if (!params) {
        return;
    }
    JourneysService.getJourneyById(params.id, params.deleted)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.getJourneys = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params = Utilities.checkAllParametersGet(req.swagger.params, res);
    if (!params) {
        return;
    }
    JourneysService.getJourneys(params.skip, params.limit, params.orderBy,
        params.filter, params.deleted, params.metadata)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};
