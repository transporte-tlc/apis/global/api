"use strict";

import { EmailsService } from "../services/EmailsService";
import { CustomNext, CustomRequest, CustomResponse } from "../utils/customsHandlers";
import { Utilities } from "../utils/utilities";
import { ResponsePayload } from "../utils/writer";

module.exports.changeEmail = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params = Utilities.checkVariablesNotNull(["newEmail", "idUser"], req.swagger.params, res);
    if (!params) {
        return;
    }
    if (!params.idUser) {
        params.idUser = Utilities.getUserId(req);
    }
    if (!Utilities.hasOwnPermissions(req, params.idUser) && !Utilities.hasEditorPermission(req, res)) {
        return;
    }
    EmailsService.changeEmail(params.newEmail, params.idUser)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.changeEmailToken = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params = Utilities.checkVariableNotNull("token", req.swagger.params);
    if (!params) {
        return;
    }
    EmailsService.changeEmailToken(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.sendEmail = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params = Utilities.checkVariableNotNull("data", req.swagger.params);
    if (!params) {
        return;
    }
    EmailsService.sendEmail(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.verifyEmail = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params = Utilities.checkVariableNotNull("token", req.swagger.params);
    if (!params) {
        return;
    }
    EmailsService.verifyEmail(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};
