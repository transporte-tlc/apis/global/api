"use strict";

import { Accounts } from "../models";
import { AccountsService } from "../services/AccountsService";
import { CustomNext, CustomRequest, CustomResponse } from "../utils/customsHandlers";
import { ParametersComplete, ParametersIdDeleted, Utilities } from "../utils/utilities";
import { ResponsePayload } from "../utils/writer";

module.exports.addAccount = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params: Accounts = Utilities.checkVariableNotNull("account", req.swagger.params, res);
    if (!params) {
        return;
    }
    AccountsService.addAccount(params)
        .then((response: Accounts) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.addDeletedAccount = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkId(req.swagger.params, res);
    if (params !== 0 && !params) {
        return;
    }
    AccountsService.addDeletedAccount(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.deleteAccount = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkId(req.swagger.params, res);
    if (params !== 0 && !params) {
        return;
    }
    AccountsService.deleteAccount(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.editAccount = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkVariableNotNull("account", req.swagger.params, res);
    if (!params) {
        return;
    }
    AccountsService.editAccount(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.getAccountById = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasViewerPermission(req, res)) {
        return;
    }
    const params: ParametersIdDeleted = Utilities.checkIdAndDelete(req.swagger.params, res);
    if (!params) {
        return;
    }
    AccountsService.getAccountById(params.id, params.deleted)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.getAccounts = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasViewerPermission(req, res)) {
        return;
    }
    const params: ParametersComplete = Utilities.checkAllParametersGet(req.swagger.params, res);
    if (!params) {
        return;
    }
    AccountsService.getAccounts(params.skip, params.limit, params.orderBy, params.filter
        , params.deleted, params.metadata)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};
