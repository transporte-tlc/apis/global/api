"use strict";

import { Memberships } from "../models";
import { MembershipsService } from "../services/MembershipsService";
import { CustomNext, CustomRequest, CustomResponse } from "../utils/customsHandlers";
import { ParametersComplete, ParametersIdDeleted, Utilities } from "../utils/utilities";
import { ResponsePayload } from "../utils/writer";

module.exports.addMembership = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkVariableNotNull("membership", req.swagger.params, res);
    if (!params) {
        return;
    }
    MembershipsService.addMembership(new Memberships(params))
        .then((response: Memberships) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.addDeletedMembership = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params = Utilities.checkId(req.swagger.params, res);
    if (params !== 0 && !params) {
        return;
    }
    MembershipsService.addDeletedMembership(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.deleteMembership = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkId(req.swagger.params, res);
    if (params !== 0 && !params) {
        return;
    }
    MembershipsService.deleteMembership(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.editMembership = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkVariableNotNull("membership", req.swagger.params, res);
    if (!params) {
        return;
    }
    MembershipsService.editMembership(new Memberships(params))
        .then((response: Memberships) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.getMemberships = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params: ParametersComplete = Utilities.checkAllParametersGet(req.swagger.params, res);
    if (!params) {
        return;
    }
    if (!Utilities.hasOwnPermissions(req, params.idUser)) {
        if (!Utilities.hasViewerPermission(req, res)) {
            return;
        }
    }
    MembershipsService.getMemberships(params.skip, params.limit, params.orderBy
        , params.filter, params.deleted, params.metadata, params.idUser)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.getMembershipsById = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params: ParametersIdDeleted = Utilities.checkIdAndDelete(req.swagger.params, res);
    if (!params) {
        return;
    }
    if (!Utilities.hasOwnPermissions(req, params.idUser)) {
        if (!Utilities.hasViewerPermission(req, res)) {
            return;
        }
    }
    MembershipsService.getMembershipsById(params.id, params.deleted, params.idUser)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};
