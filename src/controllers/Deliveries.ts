"use strict";

import { Deliveries, Users, UsersRole } from "../models";
import { DeliveriesService } from "../services/DeliveriesService";
import { CustomNext, CustomRequest, CustomResponse } from "../utils/customsHandlers";
import { Utilities } from "../utils/utilities";
import { ResponsePayload } from "../utils/writer";

module.exports.addDelivery = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkVariableNotNull("delivery", req.swagger.params);
    if (!params) {
        return;
    }
    DeliveriesService.addDelivery(new Deliveries(params))
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.addDeletedDelivery = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkId(req.swagger.params, res);
    if (params !== 0 && !params) {
        return;
    }
    DeliveriesService.addDeletedDelivery(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.deleteDelivery = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkId(req.swagger.params, res);
    if (params !== 0 && !params) {
        return;
    }
    DeliveriesService.deleteDelivery(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.editDelivery = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkVariableNotNull("delivery", req.swagger.params);
    if (!params) {
        return;
    }
    DeliveriesService.editDelivery(new Deliveries(params))
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.getDeliveries = async (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params = Utilities.checkAllParametersGet(req.swagger.params, res);
    if (!params) {
        return;
    }
    if (!Utilities.hasOwnPermissions(req, params.idUser)) {
        if (!Utilities.hasViewerPermission(req, res)) {
            return;
        }
    }
    const accounts = await Utilities.getUserAccounts(req, UsersRole.VIEWER, params.idUser, params.idAccount, res);
    if (!accounts) {
        return;
    }
    DeliveriesService.getDeliveries(params.skip, params.limit, params.orderBy, params.filter
        , params.deleted, params.metadata, accounts)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.getDeliveryById = async (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params = Utilities.checkIdAndDelete(req.swagger.params, res);
    if (!params) {
        return;
    }
    if (!Utilities.hasOwnPermissions(req, params.idUser)) {
        if (!Utilities.hasViewerPermission(req, res)) {
            return;
        }
    }
    const accounts = await Utilities.getUserAccounts(req, UsersRole.VIEWER, params.idUser, null, res);
    if (!accounts) {
        return;
    }
    DeliveriesService.getDeliveryById(params.id, params.deleted, accounts)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};
