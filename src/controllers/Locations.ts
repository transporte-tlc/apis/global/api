"use strict";

import { LocationsService } from "../services/LocationsService";
import { CustomNext, CustomRequest, CustomResponse } from "../utils/customsHandlers";
import { Utilities } from "../utils/utilities";
import { ResponsePayload } from "../utils/writer";

module.exports.addLocation = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkVariableNotNull("location", req.swagger.params, res);
    if (!params) {
        return;
    }
    LocationsService.addLocation(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.addDeletedLocation = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkId(req.swagger.params, res);
    if (params !== 0 && !params) {
        return;
    }
    LocationsService.addDeletedLocation(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.deleteLocation = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkId(req.swagger.params, res);
    if (params !== 0 && !params) {
        return;
    }
    LocationsService.deleteLocation(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.editLocation = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkVariableNotNull("location", req.swagger.params, res);
    if (!params) {
        return;
    }
    LocationsService.editLocation(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.getLocationById = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasViewerPermission(req, res)) {
        return;
    }
    const params = Utilities.checkIdAndDelete(req.swagger.params, res);
    if (!params) {
        return;
    }
    LocationsService.getLocationById(params.id, params.deleted)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.getLocations = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasViewerPermission(req, res)) {
        return;
    }
    const params = Utilities.checkAllParametersGet(req.swagger.params, res);
    if (!params) {
        return;
    }
    LocationsService.getLocations(params.skip, params.limit, params.orderBy
        , params.filter, params.deleted, params.metadata)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};
