"use strict";

import { Users, UsersRole } from "../models";
import { UsersService } from "../services/UsersService";
import { CustomNext, CustomRequest, CustomResponse } from "../utils/customsHandlers";
import { Utilities } from "../utils/utilities";
import { VALID_RESPONSES } from "../utils/ValidResponses";
import { ResponsePayload } from "../utils/writer";

module.exports.addUser = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    // Uncomment here to allow only registered editor create new users.
    // if(Utilities.validatePermissions(req,res,UsersRole.EDITOR)){
    //   return;
    // }
    const params: Users = Utilities.checkVariableNotNull("user", req.swagger.params, res);
    if (!params) {
        return;
    }
    if (params.role !== UsersRole.COMMON) {
        if (params.role === UsersRole.ADMIN) {
            if (!Utilities.hasAdminPermission(req, res)) {
                return;
            }
        } else if (params.role === UsersRole.EDITOR) {
            if (!Utilities.hasEditorPermission(req, res)) {
                return;
            }
        } else if (params.role === UsersRole.VIEWER) {
            if (!Utilities.hasViewerPermission(req, res)) {
                return;
            }
        }
    }
    UsersService.addUser(new Users(params))
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.addDeletedUser = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkId(req.swagger.params, res);
    if (params !== 0 && !params) {
        return;
    }
    UsersService.addDeletedUser(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.deleteUsers = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkId(req.swagger.params, res);
    if (params !== 0 && !params) {
        return;
    }
    UsersService.deleteUsers(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.editUser = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params: Users = Utilities.checkVariableNotNull("user", req.swagger.params);
    if (!params) {
        return;
    }
    if (!Utilities.getUser(req)) {
        ResponsePayload.response(res, VALID_RESPONSES.ERROR.AUTH.TOKEN.USER, 401);
        return;
    }
    let canVerifyEmail = false;
    if (!Utilities.hasOwnPermissions(req, params)) {
        if (params.role === UsersRole.ADMIN) {
            if (!Utilities.hasAdminPermission(req, res)) {
                return;
            }
        } else if (!Utilities.hasEditorPermission(req, res)) {
                return;
            }
        canVerifyEmail = true;
    } else {
        // If the user is editing its self parameters, force user tu stay active.
        params.deleted = false;
    }
    if (!canVerifyEmail) {
        delete params.emailVerified;
    }
    UsersService.editUser(new Users(params))
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.getUserById = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.getUser(req)) {
        ResponsePayload.response(res, VALID_RESPONSES.ERROR.AUTH.TOKEN.USER, 401);
        return;
    }
    const params = Utilities.checkIdAndDelete(req.swagger.params, res);
    if (!params) {
        return;
    }
    if (!Utilities.hasOwnPermissions(req, params.id) && !Utilities.hasViewerPermission(req, res)) {
        return;
    }
    UsersService.getUserById(params.id, params.deleted)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.getUsers = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasViewerPermission(req, res)) {
        return;
    }
    const params = Utilities.checkAllParametersGet(req.swagger.params, res);
    if (!params) {
        return;
    }
    UsersService.getUsers(params.skip, params.limit, params.orderBy, params.filter
        , params.deleted, params.metadata)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.login = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params = Utilities.checkVariablesNotNull(["email", "password"], req.swagger.params, res);
    if (!params) {
        return;
    }
    UsersService.login(params.email, params.password)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.loginToken = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params = Utilities.checkVariableNotNull("token", req.swagger.params, res);
    if (!params) {
        return;
    }
    UsersService.loginToken(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};
module.exports.verifyToken = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    UsersService.verifyToken(req.headers.x_user_key).then((response: any) => {
        ResponsePayload.response(res, response);
    })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.logout = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    UsersService.logout()
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};
