"use strict";

import { Carriers } from "../models";
import { CarriersService } from "../services/CarriersService";
import { CustomNext, CustomRequest, CustomResponse } from "../utils/customsHandlers";
import { Utilities } from "../utils/utilities";
import { ResponsePayload } from "../utils/writer";

module.exports.addCarrier = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params: Carriers = Utilities.checkVariableNotNull("carrier", req.swagger.params, res);
    if (!params) {
        return;
    }
    CarriersService.addCarrier(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        }).catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.addDeletedCarrier = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkId(req.swagger.params, res);
    if (params !== 0 && !params) {
        return;
    }
    CarriersService.addDeletedCarrier(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        }).catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.deleteCarrier = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkId(req.swagger.params, res);
    if (params !== 0 && !params) {
        return;
    }
    CarriersService.deleteCarrier(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        }).catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.editCarrier = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkVariableNotNull("carrier", req.swagger.params, res);
    if (!params) {
        return;
    }
    CarriersService.editCarrier(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        }).catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.getCarrierById = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasViewerPermission(req, res)) {
        return;
    }
    const params = Utilities.checkIdAndDelete(req.swagger.params, res);
    if (!params) {
        return;
    }
    CarriersService.getCarrierById(params.id, params.deleted)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        }).catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.getCarriers = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasViewerPermission(req, res)) {
        return;
    }
    const params = Utilities.checkAllParametersGet(req.swagger.params, res);
    if (!params) {
        return;
    }
    CarriersService.getCarriers(params.skip, params.limit, params.orderBy, params.filter
        , params.deleted, params.metadata)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        }).catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};
