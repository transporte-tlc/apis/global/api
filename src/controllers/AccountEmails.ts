"use strict";

import { AccountEmailsService } from "../services/AccountEmailsService";
import { CustomNext, CustomRequest, CustomResponse } from "../utils/customsHandlers";
import { ParametersComplete, ParametersIdDeleted, Utilities } from "../utils/utilities";
import { ResponsePayload } from "../utils/writer";

module.exports.addAccountEmails = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params = Utilities.checkVariableNotNull("accountEmail", req.swagger.params, res);
    if (!params) {
        return;
    }
    AccountEmailsService.addAccountEmails(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.addDeletedAccountEmail = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params = Utilities.checkId(req.swagger.params, res);
    if (params !== 0 && !params) {
        return;
    }
    AccountEmailsService.addDeletedAccountEmail(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.deleteAccountEmail = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params = Utilities.checkId(req.swagger.params, res);
    if (params !== 0 && !params) {
        return;
    }
    AccountEmailsService.deleteAccountEmail(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.editAccountEmail = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params = Utilities.checkVariableNotNull("accountEmail", req.swagger.params, res);
    if (!params) {
        return;
    }
    AccountEmailsService.editAccountEmail(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.getAccountEmailById = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params: ParametersIdDeleted | null = Utilities.checkIdAndDelete(req.swagger.params, res);
    if (!params) {
        return;
    }
    AccountEmailsService.getAccountEmailById(params.id, params.deleted)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.getAccountEmails = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params: ParametersComplete = Utilities.checkAllParametersGet(req.swagger.params, res);
    if (!params) {
        return;
    }
    AccountEmailsService.getAccountEmails(params.skip, params.limit, params.orderBy
        , params.filter, params.deleted, params.metadata)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};
