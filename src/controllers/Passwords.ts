"use strict";

import { PasswordService } from "../services/PasswordsService";
import { CustomNext, CustomRequest, CustomResponse } from "../utils/customsHandlers";
import { Utilities } from "../utils/utilities";
import { ResponsePayload } from "../utils/writer";

module.exports.changePassword = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params = Utilities.checkVariablesNotNull(["oldPassword", "newPassword", "idUser"], req.swagger.params, res);
    if (!params) {
        return;
    }
    if (!Utilities.hasOwnPermissions(req, params.idUser) && !Utilities.hasEditorPermission(req, res)) {
        return;
    }
    PasswordService.changePassword(params.oldPassword, params.newPassword, params.idUser)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.changePasswordWithToken = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params = Utilities.checkVariablesNotNull(["token", "password"], req.swagger.params);
    if (!params) {
        return;
    }
    PasswordService.changePasswordWithToken(params.token, params.password)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.requestPassword = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params = Utilities.checkVariableNotNull("data", req.swagger.params);
    if (!params) {
        return;
    }
    PasswordService.requestPassword(params.email, params.urlResponse)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};
