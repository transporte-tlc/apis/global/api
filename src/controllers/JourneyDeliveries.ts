"use strict";

import { JourneyDeliveriesService } from "../services/JourneyDeliveriesService";
import { CustomNext, CustomRequest, CustomResponse } from "../utils/customsHandlers";
import { Utilities } from "../utils/utilities";
import { ResponsePayload } from "../utils/writer";

module.exports.addJourneyDelivery = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkVariableNotNull("journeyDelivery", req.swagger.params);
    if (!params) {
        return;
    }
    JourneyDeliveriesService.addJourneyDelivery(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.addDeletedJourneyDelivery = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkId(req.swagger.params);
    if (params !== 0 && !params) {
        return;
    }
    JourneyDeliveriesService.addDeletedJourneyDelivery(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.deleteJourneyDelivery = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkId(req.swagger.params);
    if (params !== 0 && !params) {
        return;
    }
    JourneyDeliveriesService.deleteJourneyDelivery(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.editJourneyDelivery = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkVariableNotNull("journeyDelivery", req.swagger.params);
    if (!params) {
        return;
    }
    JourneyDeliveriesService.editJourneyDelivery(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.getJourneyDeliveries = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params = Utilities.checkAllParametersGet(req.swagger.params, res);
    const accounts = 2;
    // if(!req.user || !req.user.data || (req.user.data.Type !== 'ADMIN' && req.user.data.Type !== 'EDITOR')){
    //   if(!req.user.data.Memberships){
    //     utils.writeJson(res, new utils.respondWithCode(401,'Not having the privilege.'));
    //     return;
    //   }
    //   accounts = req.user.data.Memberships.map( member => member.idAccount);
    // }
    if (!params) {
        return;
    }
    JourneyDeliveriesService.getJourneyDeliveries(params.skip, params.limit, params.orderBy,
        params.filter, params.deleted, params.metadata, accounts)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.getJourneyDeliveryById = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const accounts = 2;
    // if(!req.user || !req.user.data || (req.user.data.Type !== 'ADMIN' && req.user.data.Type !== 'EDITOR')){
    //   if(!req.user.data.Memberships){
    //     utils.writeJson(res, new utils.respondWithCode(401,'Not having the privilege.'));
    //     return;
    //   }
    //   accounts = req.user.data.Memberships.map( member => member.idAccount);
    // }
    const params = Utilities.checkIdAndDelete(req.swagger.params, res);
    if (!params) {
        return;
    }
    JourneyDeliveriesService.getJourneyDeliveryById(params.id, params.deleted, accounts)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};
