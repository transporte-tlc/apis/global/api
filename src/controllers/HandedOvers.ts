"use strict";

import { HandedOvers, Users } from "../models";
import { HandedOversService } from "../services/HandedOversService";
import { CustomNext, CustomRequest, CustomResponse } from "../utils/customsHandlers";
import { Utilities } from "../utils/utilities";
import { ResponsePayload } from "../utils/writer";

module.exports.addHandedOver = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkVariableNotNull("handedOver", req.swagger.params);
    if (!params) {
        return;
    }
    HandedOversService.addHandedOver(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.addDeletedHandedOver = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkId(req.swagger.params, res);
    if (params !== 0 && !params) {
        return;
    }
    HandedOversService.addDeletedHandedOver(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.deleteHandedOver = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkId(req.swagger.params, res);
    if (params !== 0 && !params) {
        return;
    }
    HandedOversService.deleteHandedOver(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.editHandedOver = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    if (!Utilities.hasEditorPermission(req, res)) {
        return;
    }
    const params = Utilities.checkVariableNotNull("handedOver", req.swagger.params);
    if (!params) {
        return;
    }
    HandedOversService.editHandedOver(params)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.getHandedOverById = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params = Utilities.checkIdAndDelete(req.swagger.params);
    const accounts = "";
    // if(!req.user || !req.user.data || (req.user.data.Type !== 'ADMIN' && req.user.data.Type !== 'EDITOR')){
    //   if(!req.user.data.Memberships){
    //     utils.writeJson(res, new utils.respondWithCode(401,'Not having the privilege.'));
    //     return;
    //   }
    //   accounts = req.user.data.Memberships.map( member => member.idAccount);
    // }
    if (!params) {
        return;
    }
    HandedOversService.getHandedOverById(params.id, params.deleted, accounts)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};

module.exports.getHandedOvers = (req: CustomRequest, res: CustomResponse, next: CustomNext) => {
    const params = Utilities.checkAllParametersGet(req.swagger.params, res);
    const accounts = "";
    // if(!req.user || !req.user.data || (req.user.data.Type !== 'ADMIN' && req.user.data.Type !== 'EDITOR')){
    //   if(!req.user.data.Memberships){
    //     utils.writeJson(res, new utils.respondWithCode(401,'Not having the privilege.'));
    //     return;
    //   }
    //   accounts = req.user.data.Memberships.map( member => member.idAccount);
    // }
    if (!params) {
        return;
    }
    HandedOversService.getHandedOvers(params.skip, params.limit, params.orderBy
        , params.filter, params.deleted, params.metadata, accounts)
        .then((response: any) => {
            ResponsePayload.response(res, response);
        })
        .catch((response: any) => {
            ResponsePayload.response400(res, response);
        });
};
