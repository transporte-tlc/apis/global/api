#Verify that builder and keymetrics use the same alpine image version
FROM node:8-alpine as builder

ARG NODE_ENV=development
ENV NODE_ENV=${NODE_ENV}

RUN apk add --no-cache \
    python make gcc g++

WORKDIR /usr/app

COPY package.json .

RUN npm install

FROM keymetrics/pm2:8-alpine

LABEL mainteiner="Patricio Perpetua <patricio.perpetua.arg@gmail.com>" \
    name="transporte-tlc/api" \
    architecture="x86_64" \
    vendor="SINGLETON" \
    vcs-type="git" \
    vcs-url="https://gitlab.com/transporte-tlc/apis/global/api.git" \
    distribution-scope="private" \
    Summary="Image to run tlc api."

WORKDIR /usr/app

#Binaries used by scripts to create options files.
RUN apk add --no-cache \
    bash openssh-keygen openssl outils-sha256

COPY --from=builder /usr/app/node_modules node_modules
COPY src src/
COPY package.json .
COPY tsconfig.json .

RUN npm run tsc

ENV NPM_CONFIG_LOGLEVEL warn

COPY scripts/init_service.sh scripts/
COPY scripts/keys_generator.sh scripts/
COPY environment/.docker.env ./.env
COPY database.json .
COPY ecosystem.config.docker.js ./ecosystem.config.js

# Generate keys and api_tokens file.
RUN ./scripts/init_service.sh p

VOLUME /usr/app/assets

# Show current folder structure in logs
RUN ls -al -R src && ls -al .

#TODO: add script to verify if it is necesary to delete bash bash openssh-keygen openssl outils-sha256.
#maybe 2 types of images, one with binaries and other without
RUN rm -rf /var/cache/apk/*

EXPOSE 3000

CMD [ "pm2-runtime", "start", "build/server.js" ]