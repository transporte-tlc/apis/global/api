import * as dotenv from "dotenv";
import fs from "fs";

dotenv.config();

export class AppTokenHelper {

    public static getInstance() {
        if (!this.instance) {
            this.instance = new AppTokenHelper();
            let fileAppTokens = process.env.APP_TOKENS_FILE;
            if (!fileAppTokens) {
                fileAppTokens = "./data/node/app_tokens.json";
                if (!fs.existsSync(fileAppTokens)) {
                    fileAppTokens = "./app_tokens.json";
                }
            }
            this.tokens = JSON.parse(fs.readFileSync(fileAppTokens, "utf8"));
        }
    }

    public static getToken(): string {
        this.getInstance();
        return this.tokens[0].token;
    }

    private static instance: AppTokenHelper;
    private static tokens: object;

    private constructor() {
    }
}
