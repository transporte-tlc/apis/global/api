import http from "http";
import { LoginObject, Users, UsersRole } from "../../src/models";
import { LoggerUtility } from "../../src/utils/LoggerUtility";
import { AppTokenHelper } from "./appToken";
import { environments } from "./environment";

export class UserTokenHelper {

    public static async getInstance() {
        return new Promise((resolve, reject) => {
            if (!this.instance) {
                this.instance = new UserTokenHelper();
                let promises = [];
                promises.push(
                    new Promise((resolve2) => {
                        http.get({
                        hostname: environments.URL, path: "/users/login?email="
                            + this.emailAdmin + "&password=" + this.passwordAdmin
                        , port: environments.PORT
                        , headers: {
                            "Content-Type": "application/json"
                            , "Accept": "application/json"
                            , x_app_id: this.apiToken
                        }
                        }, (res) => {
                            let data = "";
                            res.on("data", (chunk) => {
                                data += chunk;
                            });
                            // The whole response has been received. Print out the result.
                            res.on("end", () => {
                                try {
                                    this.loginAdmin = JSON.parse(data);
                                } catch (error) {
                                    LoggerUtility.error("Error admin token", data);
                                }
                                resolve2();
                            });
                        });
                }));
                promises.push(
                    new Promise((resolve2) => {
                        http.get({
                        hostname: environments.URL, path: "/users/login?email="
                        + this.emailEditor + "&password=" + this.passwordEditor
                        , port: environments.PORT
                        , headers: {
                            "Content-Type": "application/json"
                            , "Accept": "application/json"
                            , x_app_id: this.apiToken
                        }
                        }, (res) => {
                            let data = "";
                            res.on("data", (chunk) => {
                                data += chunk;
                            });
                            // The whole response has been received. Print out the result.
                            res.on("end", () => {
                                try {
                                    this.loginEditor = JSON.parse(data);
                                } catch (error) {
                                    LoggerUtility.error("Error editor token", data);
                                }
                                resolve2();
                            });
                        });
                }));
                promises.push(new Promise((resolve2) => {
                    http.get({
                    hostname: environments.URL, path: "/users/login?email="
                    + this.emailViewer + "&password=" + this.passwordViewer
                    , port: environments.PORT
                    , headers: {
                        "Content-Type": "application/json"
                        , "Accept": "application/json"
                        , x_app_id: this.apiToken
                    }
                    }, (res) => {
                        let data = "";
                        res.on("data", (chunk) => {
                            data += chunk;
                        });
                        // The whole response has been received. Print out the result.
                        res.on("end", () => {
                            try {
                                this.loginViewer = JSON.parse(data);
                            } catch (error) {
                                LoggerUtility.error("Error viewer token", data);
                            }
                            resolve2();
                        });
                    });
                }));
                promises.push(new Promise((resolve2) => {
                    http.get({
                    hostname: environments.URL, path: "/users/login?email="
                    + this.emailCommon + "&password=" + this.passwordCommon
                    , port: environments.PORT
                    , headers: {
                        "Content-Type": "application/json"
                        , "Accept": "application/json"
                        , x_app_id: this.apiToken
                    }
                    }, (res) => {
                        let data = "";
                        res.on("data", (chunk) => {
                            data += chunk;
                        });
                        // The whole response has been received. Print out the result.
                        res.on("end", () => {
                            try {
                                this.loginCommon = JSON.parse(data);
                            } catch (error) {
                                LoggerUtility.error("Error common token", data);
                            }
                            resolve2();
                        });
                    });
                }));
                Promise.all(promises).then(() => {
                    resolve(this.instance);
                    return;
                });
            } else {
                resolve(this.instance);
                return;
            }
        });
    }

    public static async getToken(userType: UsersRole): Promise<string> {
        await this.getInstance();
        switch (userType) {
            case UsersRole.ADMIN:
                return this.loginAdmin.token;
            case UsersRole.EDITOR:
                return this.loginEditor.token;
            case UsersRole.VIEWER:
                return this.loginViewer.token;
            case UsersRole.COMMON:
                return this.loginCommon.token;
        }
    }

    public static async getAdminToken(): Promise<string> {
        return this.getToken(UsersRole.ADMIN);
    }

    public static async getEditorToken(): Promise<string> {
        return this.getToken(UsersRole.EDITOR);
    }

    public static async getViewerToken(): Promise<string> {
        return this.getToken(UsersRole.VIEWER);
    }

    public static async getCommonToken(): Promise<string> {
        return this.getToken(UsersRole.COMMON);
    }

    public static async getUser(userType: UsersRole): Promise<Users> {
        this.getInstance();
        switch (userType) {
            case UsersRole.ADMIN:
                return this.loginAdmin.user;
            case UsersRole.EDITOR:
                return this.loginEditor.user;
            case UsersRole.VIEWER:
                return this.loginViewer.user;
            case UsersRole.COMMON:
                return this.loginCommon.user;
        }
    }

    public static async getAdminUser(): Promise<Users> {
        return this.getUser(UsersRole.ADMIN);
    }

    public static async getEditorUser(): Promise<Users> {
        return this.getUser(UsersRole.EDITOR);
    }

    public static async getViewerUser(): Promise<Users> {
        return this.getUser(UsersRole.VIEWER);
    }

    public static async getCommonUser(): Promise<Users> {
        return this.getUser(UsersRole.COMMON);
    }

    private static instance: UserTokenHelper;
    private static loginAdmin: LoginObject;
    private static loginEditor: LoginObject;
    private static loginViewer: LoginObject;
    private static loginCommon: LoginObject;

    private static readonly emailAdmin = "patricio.perpetua.arg@gmail.com";
    private static readonly passwordAdmin = "TlC2018!@";

    private static readonly emailEditor = "veronica@tlc.com.ar";
    private static readonly passwordEditor = "Editora!!2019";

    private static readonly emailViewer = "marcos@tlc.com.ar";
    private static readonly passwordViewer = "Editora!!2019";

    private static readonly emailCommon = "pato2011usa@gmail.com";
    private static readonly passwordCommon = "SuperPassword2018!!";

    private static readonly apiToken = AppTokenHelper.getToken();

    private constructor() {
    }
}