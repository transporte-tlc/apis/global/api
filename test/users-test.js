'use strict';
import * as chakram from "chakram";
import mocha from "mocha";
import { AppTokenHelper, environments, UserTokenHelper } from "./helpers";

const apiUrl = environments.FULL_URL;
const apiToken = AppTokenHelper.getToken();
let adminToken, editorToken, viewerToken, commonToken;

before(function (done) {
    let tokens = [];
    UserTokenHelper.getAdminToken().then((tokenAdmin) => {
        adminToken = tokenAdmin;
        tokens.push(UserTokenHelper.getEditorToken().then((token) => {
            editorToken = token;
        }));
        tokens.push(UserTokenHelper.getViewerToken().then((token) => {
            viewerToken = token;
        }));
        tokens.push(UserTokenHelper.getCommonToken().then((token) => {
            commonToken = token;
        }));
        return Promise.all(tokens).then(() => {
            done();
        });
    });
});

describe('tests for /users', function () {
    describe('tests for get', function () {
        it('should respond 200 for "admin"', function () {
            const response = chakram.request('get', apiUrl + '/users', {
                'qs': { "skip": 0, "limit": 5, "orderBy": null, "filter": null, "deleted": "ALL", "metadata": true },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true
            });

            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.contain.json({ metadata: {}, items: [] });
            return chakram.wait();
        });

        it('should respond 200 for "editor"', function () {
            const response = chakram.request('get', apiUrl + '/users', {
                'qs': { "skip": 0, "limit": 5, "orderBy": null, "filter": null, "deleted": "ALL", "metadata": true },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": editorToken },
                'time': true
            });

            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.contain.json({ metadata: {}, items: [] });
            return chakram.wait();
        });

        it('should respond 200 for "viewer"', function () {
            const response = chakram.request('get', apiUrl + '/users', {
                'qs': { "skip": 0, "limit": 5, "orderBy": null, "filter": null, "deleted": "ALL", "metadata": true },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": viewerToken },
                'time': true
            });

            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.contain.json({ metadata: {}, items: [] });
            return chakram.wait();
        });

        it('should respond 200 for "admin no metadata"', function () {
            const response = chakram.request('get', apiUrl + '/users', {
                'qs': { "skip": 0, "limit": 5, "orderBy": null, "filter": null, "deleted": "ALL", "metadata": false },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true
            });

            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.contain.json({ metadata: null, items: [] });
            return chakram.wait();
        });

        it('should respond 204 for "Response is empty."', function () {
            const response = chakram.request('get', apiUrl + '/users', {
                'qs': { "skip": 0, "limit": 10, "orderBy": null, "filter": null, "deleted": "DELETED", "metadata": false },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true
            });

            chakram.expect(response).to.have.status(204);
            return chakram.wait();
        });

        it('should respond 400 for "Some parameters are missing or badly entered."', function () {
            this.skip();
            const response = chakram.request('get', apiUrl + '/users', {
                'qs': { "skip": 0, "limit": 10, "orderBy": null, "filter": null, "deleted": "ALL", "metadata": false },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" } } });
            return chakram.wait();
        });

        it('should respond 401 for "user token"', function () {
            const response = chakram.request('get', apiUrl + '/users', {
                'qs': { "skip": 0, "limit": 10, "orderBy": null, "filter": null, "deleted": "ALL", "metadata": false },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
                'time': true
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.token.user" });
            return chakram.wait();
        });

        it('should respond 401 for "api token"', function () {
            const response = chakram.request('get', apiUrl + '/users', {
                'qs': { "skip": 0, "limit": 10, "orderBy": null, "filter": null, "deleted": "ALL", "metadata": false },
                'headers': { "Content-Type": "application/json", "Accept": "application/json" },
                'time': true
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.token.app" });
            return chakram.wait();
        });

        it('should respond 401 for "unprivileged"', function () {
            const response = chakram.request('get', apiUrl + '/users', {
                'qs': { "skip": 0, "limit": 10, "orderBy": null, "filter": null, "deleted": "ALL", "metadata": false },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": commonToken },
                'time': true
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.unprivileged" });
            return chakram.wait();
        });

        // it('should respond 404 for "Entity not found."', function () {
        //     const response = chakram.request('get', apiUrl + '/users', {
        //         'qs': { "skip": -37436050, "limit": -71924362, "orderBy": "ex fugiat", "filter": "nulla", "deleted": "NOT-DELETED", "metadata": true },
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(404);
        //     chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function () {
        //     const response = chakram.request('get', apiUrl + '/users', {
        //         'qs': { "skip": 49671945, "limit": 33828329, "orderBy": "reprehenderit dolor est", "filter": "ut culpa dolore cupidatat v", "deleted": "DELETED", "metadata": true },
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(405);
        //     chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
        //     return chakram.wait();
        // });

    });

    describe('tests for post', function () {
        it('should respond 200 for "admin common"', function () {
            const user = {
                "name": "10",
                "identification": "10",
                "email": "pato10@gmail.com",
                "emailVerified": false,
                "password": "string1212@",
                "passwordReset": false,
                "role": "COMMON",
                "deleted": true
            };
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true,
                'body': user
            });
            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "identification": { "type": "string" }, "email": { "type": "string" }, "emailVerified": { "type": "boolean" }, "password": { "type": "string", "format": "password" }, "passwordReset": { "type": "boolean" }, "role": { "type": "string", "enum": ["COMMON", "VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" }, "memberships": { "type": "array", "items": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "idUser": { "type": "integer", "format": "int64" }, "idAccount": { "type": "integer", "format": "int64" }, "account": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } }, "receiveEmail": { "type": "boolean" }, "role": { "type": "string", "enum": ["VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" } } } } } });
            chakram.expect(response).to.contain.json(user);
            return chakram.wait();
        });

        it('should respond 200 for "admin viewer"', function () {
            const user = {
                "name": "11",
                "identification": "11",
                "email": "pato11@gmail.com",
                "emailVerified": false,
                "password": "string1212@",
                "passwordReset": false,
                "role": "VIEWER",
                "deleted": true
            };
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true,
                'body': user
            });
            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "identification": { "type": "string" }, "email": { "type": "string" }, "emailVerified": { "type": "boolean" }, "password": { "type": "string", "format": "password" }, "passwordReset": { "type": "boolean" }, "role": { "type": "string", "enum": ["COMMON", "VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" }, "memberships": { "type": "array", "items": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "idUser": { "type": "integer", "format": "int64" }, "idAccount": { "type": "integer", "format": "int64" }, "account": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } }, "receiveEmail": { "type": "boolean" }, "role": { "type": "string", "enum": ["VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" } } } } } });
            chakram.expect(response).to.contain.json(user);
            return chakram.wait();
        });

        it('should respond 200 for "admin editor"', function () {
            const user = {
                "name": "12",
                "identification": "12",
                "email": "pato12@gmail.com",
                "emailVerified": false,
                "password": "string1212@",
                "passwordReset": false,
                "role": "EDITOR",
                "deleted": true
            };
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true,
                'body': user
            });
            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "identification": { "type": "string" }, "email": { "type": "string" }, "emailVerified": { "type": "boolean" }, "password": { "type": "string", "format": "password" }, "passwordReset": { "type": "boolean" }, "role": { "type": "string", "enum": ["COMMON", "VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" }, "memberships": { "type": "array", "items": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "idUser": { "type": "integer", "format": "int64" }, "idAccount": { "type": "integer", "format": "int64" }, "account": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } }, "receiveEmail": { "type": "boolean" }, "role": { "type": "string", "enum": ["VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" } } } } } });
            chakram.expect(response).to.contain.json(user);
            return chakram.wait();
        });

        it('should respond 200 for "admin admin"', function () {
            const user = {
                "name": "13",
                "identification": "13",
                "email": "pato13@gmail.com",
                "emailVerified": false,
                "password": "string1212@",
                "passwordReset": false,
                "role": "ADMIN",
                "deleted": true
            };
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true,
                'body': user
            });
            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "identification": { "type": "string" }, "email": { "type": "string" }, "emailVerified": { "type": "boolean" }, "password": { "type": "string", "format": "password" }, "passwordReset": { "type": "boolean" }, "role": { "type": "string", "enum": ["COMMON", "VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" }, "memberships": { "type": "array", "items": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "idUser": { "type": "integer", "format": "int64" }, "idAccount": { "type": "integer", "format": "int64" }, "account": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } }, "receiveEmail": { "type": "boolean" }, "role": { "type": "string", "enum": ["VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" } } } } } });
            chakram.expect(response).to.contain.json(user);
            return chakram.wait();
        });

        it('should respond 200 for "editor common"', function () {
            const user = {
                "name": "20",
                "identification": "20",
                "email": "pato20@gmail.com",
                "emailVerified": false,
                "password": "string1212@",
                "passwordReset": false,
                "role": "COMMON",
                "deleted": true
            };
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": editorToken },
                'time': true,
                'body': user
            });
            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "identification": { "type": "string" }, "email": { "type": "string" }, "emailVerified": { "type": "boolean" }, "password": { "type": "string", "format": "password" }, "passwordReset": { "type": "boolean" }, "role": { "type": "string", "enum": ["COMMON", "VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" }, "memberships": { "type": "array", "items": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "idUser": { "type": "integer", "format": "int64" }, "idAccount": { "type": "integer", "format": "int64" }, "account": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } }, "receiveEmail": { "type": "boolean" }, "role": { "type": "string", "enum": ["VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" } } } } } });
            chakram.expect(response).to.contain.json(user);
            return chakram.wait();
        });

        it('should respond 200 for "editor viewer"', function () {
            const user = {
                "name": "21",
                "identification": "21",
                "email": "pato21@gmail.com",
                "emailVerified": false,
                "password": "string1212@",
                "passwordReset": false,
                "role": "VIEWER",
                "deleted": true
            };
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": editorToken },
                'time': true,
                'body': user
            });
            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "identification": { "type": "string" }, "email": { "type": "string" }, "emailVerified": { "type": "boolean" }, "password": { "type": "string", "format": "password" }, "passwordReset": { "type": "boolean" }, "role": { "type": "string", "enum": ["COMMON", "VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" }, "memberships": { "type": "array", "items": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "idUser": { "type": "integer", "format": "int64" }, "idAccount": { "type": "integer", "format": "int64" }, "account": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } }, "receiveEmail": { "type": "boolean" }, "role": { "type": "string", "enum": ["VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" } } } } } });
            chakram.expect(response).to.contain.json(user);
            return chakram.wait();
        });

        it('should respond 200 for "editor editor"', function () {
            const user = {
                "name": "22",
                "identification": "22",
                "email": "pato22@gmail.com",
                "emailVerified": false,
                "password": "string1212@",
                "passwordReset": false,
                "role": "EDITOR",
                "deleted": true
            };
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": editorToken },
                'time': true,
                'body': user
            });
            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "identification": { "type": "string" }, "email": { "type": "string" }, "emailVerified": { "type": "boolean" }, "password": { "type": "string", "format": "password" }, "passwordReset": { "type": "boolean" }, "role": { "type": "string", "enum": ["COMMON", "VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" }, "memberships": { "type": "array", "items": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "idUser": { "type": "integer", "format": "int64" }, "idAccount": { "type": "integer", "format": "int64" }, "account": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } }, "receiveEmail": { "type": "boolean" }, "role": { "type": "string", "enum": ["VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" } } } } } });
            chakram.expect(response).to.contain.json(user);
            return chakram.wait();
        });

        it('should respond 200 for "viewer common"', function () {
            const user = {
                "name": "31",
                "identification": "31",
                "email": "pato31@gmail.com",
                "emailVerified": false,
                "password": "string1212@",
                "passwordReset": false,
                "role": "COMMON",
                "deleted": true
            };
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": viewerToken },
                'time': true,
                'body': user
            });
            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "identification": { "type": "string" }, "email": { "type": "string" }, "emailVerified": { "type": "boolean" }, "password": { "type": "string", "format": "password" }, "passwordReset": { "type": "boolean" }, "role": { "type": "string", "enum": ["COMMON", "VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" }, "memberships": { "type": "array", "items": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "idUser": { "type": "integer", "format": "int64" }, "idAccount": { "type": "integer", "format": "int64" }, "account": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } }, "receiveEmail": { "type": "boolean" }, "role": { "type": "string", "enum": ["VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" } } } } } });
            chakram.expect(response).to.contain.json(user);
            return chakram.wait();
        });

        it('should respond 200 for "viewer viewer"', function () {
            const user = {
                "name": "32",
                "identification": "32",
                "email": "pato32@gmail.com",
                "emailVerified": false,
                "password": "string1212@",
                "passwordReset": false,
                "role": "VIEWER",
                "deleted": true
            };
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": viewerToken },
                'time': true,
                'body': user
            });
            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "identification": { "type": "string" }, "email": { "type": "string" }, "emailVerified": { "type": "boolean" }, "password": { "type": "string", "format": "password" }, "passwordReset": { "type": "boolean" }, "role": { "type": "string", "enum": ["COMMON", "VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" }, "memberships": { "type": "array", "items": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "idUser": { "type": "integer", "format": "int64" }, "idAccount": { "type": "integer", "format": "int64" }, "account": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } }, "receiveEmail": { "type": "boolean" }, "role": { "type": "string", "enum": ["VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" } } } } } });
            chakram.expect(response).to.contain.json(user);
            return chakram.wait();
        });

        it('should respond 200 for "unregistered common"', function () {
            const user = {
                "name": "40",
                "identification": "40",
                "email": "pato40@gmail.com",
                "emailVerified": false,
                "password": "string1212@",
                "passwordReset": false,
                "role": "COMMON",
                "deleted": true
            };
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
                'time': true,
                'body': user
            });
            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "identification": { "type": "string" }, "email": { "type": "string" }, "emailVerified": { "type": "boolean" }, "password": { "type": "string", "format": "password" }, "passwordReset": { "type": "boolean" }, "role": { "type": "string", "enum": ["COMMON", "VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" }, "memberships": { "type": "array", "items": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "idUser": { "type": "integer", "format": "int64" }, "idAccount": { "type": "integer", "format": "int64" }, "account": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } }, "receiveEmail": { "type": "boolean" }, "role": { "type": "string", "enum": ["VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" } } } } } });
            chakram.expect(response).to.contain.json(user);
            return chakram.wait();
        });

        it('should respond 200 for "user without password"', function () {
            const user = {
                "name": "50",
                "identification": "50",
                "email": "pato50@gmail.com",
                "emailVerified": false,
                "passwordReset": true,
                "role": "COMMON",
                "deleted": true
            };
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true,
                'body': user
            });
            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "identification": { "type": "string" }, "email": { "type": "string" }, "emailVerified": { "type": "boolean" }, "password": { "type": "string", "format": "password" }, "passwordReset": { "type": "boolean" }, "role": { "type": "string", "enum": ["COMMON", "VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" }, "memberships": { "type": "array", "items": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "idUser": { "type": "integer", "format": "int64" }, "idAccount": { "type": "integer", "format": "int64" }, "account": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } }, "receiveEmail": { "type": "boolean" }, "role": { "type": "string", "enum": ["VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" } } } } } });
            chakram.expect(response).to.contain.json(user);
            return chakram.wait();
        });

        // it('should respond 204 for "Response is empty."', function () {
        //     const response = chakram.request('post', apiUrl + '/users', {
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(204);
        //     return chakram.wait();
        // });

        it('should respond 400 for "bad email"', function () {
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true,
                'body': {
                    "name": "Administrator",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "COMMON"
                }
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
            chakram.expect(response).to.have.json({ "message": "error.validation.user.email" });
            return chakram.wait();
        });

        it('should respond 400 for "bad password"', function () {
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true,
                'body': {
                    "name": "Administrator",
                    "identification": "string",
                    "email": "string@strg.com",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "COMMON"
                }
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
            chakram.expect(response).to.have.json({ "message": "error.validation.user.password" });
            return chakram.wait();
        });

        it('should respond 400 for "Bad name"', function () {
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true,
                'body': {
                    "name": "",
                    "identification": "378",
                    "email": "string@asda.com",
                    "emailVerified": true,
                    "password": "string12@@",
                    "passwordReset": true,
                    "role": "COMMON"
                }
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.json({ "message": "error.validation.name" });
            return chakram.wait();
        });

        it('should respond 400 for "Bad role"', function () {
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true,
                'body': {
                    "name": "222",
                    "identification": "378",
                    "email": "string@asda.com",
                    "emailVerified": true,
                    "password": "string12@@",
                    "passwordReset": true
                }
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.json({ "message": "error.validation.user.role" });
            return chakram.wait();
        });

        it('should respond 400 for "duplicated email"', function () {
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true,
                'body': {
                    "name": "ssswww",
                    "identification": "37854401",
                    "email": "pato2011usa@gmail.com",
                    "emailVerified": true,
                    "password": "string1212@",
                    "passwordReset": true,
                    "role": "COMMON"
                }
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
            chakram.expect(response).to.have.json({ "message": "error.validation.user.registered.email" });
            return chakram.wait();
        });

        it('should respond 400 for "duplicated identification"', function () {
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true,
                'body': {
                    "name": "ssswww",
                    "identification": "37854401",
                    "email": "pato2011usa222@gmail.com",
                    "emailVerified": true,
                    "password": "string1212@",
                    "passwordReset": true,
                    "role": "COMMON"
                }
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
            chakram.expect(response).to.have.json({ "message": "error.validation.user.registered.identification" });
            return chakram.wait();
        });

        it('should respond 401 for "user token admin"', function () {
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "ADMIN"
                }
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.token.user" });
            return chakram.wait();
        });

        it('should respond 401 for "user token editor"', function () {
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "EDITOR"
                }
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.token.user" });
            return chakram.wait();
        });

        it('should respond 401 for "user token viewer"', function () {
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "VIEWER"
                }
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.token.user" });
            return chakram.wait();
        });

        it('should respond 401 for "common user token admin"', function () {
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": commonToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "ADMIN"
                }
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.unprivileged" });
            return chakram.wait();
        });

        it('should respond 401 for "common user token editor"', function () {
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": commonToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "EDITOR"
                }
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.unprivileged" });
            return chakram.wait();
        });

        it('should respond 401 for "common user token viewer"', function () {
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": commonToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "VIEWER"
                }
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.unprivileged" });
            return chakram.wait();
        });

        it('should respond 401 for "viewer user token admin"', function () {
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": viewerToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "ADMIN"
                }
            });
            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.unprivileged" });
            return chakram.wait();
        });

        it('should respond 401 for "viewer user token editor"', function () {
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": viewerToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "EDITOR"
                }
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.unprivileged" });
            return chakram.wait();
        });

        it('should respond 401 for "editor user token admin"', function () {
            const response = chakram.request('post', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": editorToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "ADMIN"
                }
            });
            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.unprivileged" });
            return chakram.wait();
        });

        // it('should respond 404 for "Entity not found."', function () {
        //     const response = chakram.request('post', apiUrl + '/users', {
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(404);
        //     chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function () {
        //     const response = chakram.request('post', apiUrl + '/users', {
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(405);
        //     chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
        //     return chakram.wait();
        // });

    });

    describe('tests for put', function () {
        it('should respond 200 for "change everything but email"', function () {
            const user = {
                "id": 8,
                "name": "Carlos",
                "identification": "1001",
                "passwordReset": false,
                "role": "COMMON",
                "deleted": true
            };
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true,
                'body': user
            });

            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "identification": { "type": "string" }, "email": { "type": "string" }, "emailVerified": { "type": "boolean" }, "password": { "type": "string", "format": "password" }, "passwordReset": { "type": "boolean" }, "role": { "type": "string", "enum": ["COMMON", "VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" }, "memberships": { "type": "array", "items": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "idUser": { "type": "integer", "format": "int64" }, "idAccount": { "type": "integer", "format": "int64" }, "account": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } }, "receiveEmail": { "type": "boolean" }, "role": { "type": "string", "enum": ["VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" } } } } } });
            chakram.expect(response).to.contain.json(user);
            return chakram.wait();
        });

        it('should respond 200 for "change email"', function () {
            const user = {
                "id": 8,
                "name": "Carlos",
                "identification": "1001",
                "email": "dasdas@dasd.com",
                "emailVerified": false,
                "passwordReset": false,
                "role": "VIEWER",
                "deleted": false
            };
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true,
                'body': user
            });

            // chakram.expect(response).to.have.status(200);
            // chakram.expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "identification": { "type": "string" }, "email": { "type": "string" }, "emailVerified": { "type": "boolean" }, "password": { "type": "string", "format": "password" }, "passwordReset": { "type": "boolean" }, "role": { "type": "string", "enum": ["COMMON", "VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" }, "memberships": { "type": "array", "items": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "idUser": { "type": "integer", "format": "int64" }, "idAccount": { "type": "integer", "format": "int64" }, "account": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } }, "receiveEmail": { "type": "boolean" }, "role": { "type": "string", "enum": ["VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" } } } } } });
            chakram.expect(response).to.contain.json(user);
            return chakram.wait();
        });

        it('should respond 200 for "put everithing as before"', function () {
            const user = {
                "id": 8,
                "name": "Marcos",
                "identification": "450906",
                "email": "marcos@tlc.com.ar",
                "emailVerified": true,
                "passwordReset": false,
                "role": "VIEWER",
                "deleted": false
            };
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true,
                'body': user
            });

            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "identification": { "type": "string" }, "email": { "type": "string" }, "emailVerified": { "type": "boolean" }, "password": { "type": "string", "format": "password" }, "passwordReset": { "type": "boolean" }, "role": { "type": "string", "enum": ["COMMON", "VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" }, "memberships": { "type": "array", "items": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "idUser": { "type": "integer", "format": "int64" }, "idAccount": { "type": "integer", "format": "int64" }, "account": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } }, "receiveEmail": { "type": "boolean" }, "role": { "type": "string", "enum": ["VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" } } } } } });
            chakram.expect(response).to.contain.json(user);
            return chakram.wait();
        });

        // it('should respond 204 for "Response is empty."', function () {
        //     const response = chakram.request('put', apiUrl + '/users', {
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(204);
        //     return chakram.wait();
        // });

        it('should respond 400 for "missing id"', function () {
            const user = {
                "name": "31",
                "identification": "31",
                "email": "pato31@gmail.com",
                "emailVerified": false,
                "password": "string1212@",
                "passwordReset": false,
                "role": "COMMON",
                "deleted": true
            };
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true,
                'body': user
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
            chakram.expect(response).to.have.json({ "message": "error.validation.id" });
            return chakram.wait();
        });

        it('should respond 400 for "id not exits"', function () {
            const user = {
                "id": 20000,
                "name": "31",
                "identification": "31",
                "email": "pato31@gmail.com",
                "emailVerified": false,
                "password": "string1212@",
                "passwordReset": false,
                "role": "COMMON",
                "deleted": true
            };
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true,
                'body': user
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
            chakram.expect(response).to.have.json({ "message": "error.not_exist.user" });
            return chakram.wait();
        });

        it('should respond 400 for "email exits"', function () {
            const user = {
                "id": 5,
                "name": "31",
                "identification": "31",
                "email": "pato2011usa@gmail.com",
                "emailVerified": false,
                "password": "string1212@",
                "passwordReset": false,
                "role": "COMMON",
                "deleted": true
            };
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true,
                'body': user
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
            chakram.expect(response).to.have.json({ "message": "error.validation.user.registered.email" });
            return chakram.wait();
        });

        it('should respond 400 for "identification exits"', function () {
            const user = {
                "id": 5,
                "name": "31",
                "identification": "37854401",
                "email": "pato2011usa1@gmail.com",
                "emailVerified": false,
                "password": "string1212@",
                "passwordReset": false,
                "role": "COMMON",
                "deleted": true
            };
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true,
                'body': user
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
            chakram.expect(response).to.have.json({ "message": "error.validation.user.registered.identification" });
            return chakram.wait();
        });

        it('should respond 400 for "Bad role"', function () {
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true,
                'body': {
                    "id": 5,
                    "name": "222",
                    "identification": "378",
                    "email": "string@asda.com",
                    "emailVerified": true,
                    "password": "string12@@",
                    "passwordReset": true
                }
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.json({ "message": "error.validation.user.role" });
            return chakram.wait();
        });

        it('should respond 401 for "user token admin"', function () {
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "ADMIN"
                }
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.token.user" });
            return chakram.wait();
        });

        it('should respond 401 for "user token editor"', function () {
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "EDITOR"
                }
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.token.user" });
            return chakram.wait();
        });

        it('should respond 401 for "user token viewer"', function () {
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "VIEWER"
                }
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.token.user" });
            return chakram.wait();
        });

        it('should respond 401 for "user token common"', function () {
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "COMMON"
                }
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.token.user" });
            return chakram.wait();
        });

        it('should respond 401 for "common user token admin"', function () {
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": commonToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "ADMIN"
                }
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.unprivileged" });
            return chakram.wait();
        });

        it('should respond 401 for "common user token editor"', function () {
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": commonToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "EDITOR"
                }
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.unprivileged" });
            return chakram.wait();
        });

        it('should respond 401 for "common user token viewer"', function () {
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": commonToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "VIEWER"
                }
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.unprivileged" });
            return chakram.wait();
        });

        it('should respond 401 for "common user token common"', function () {
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": commonToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "COMMON"
                }
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.unprivileged" });
            return chakram.wait();
        });

        it('should respond 401 for "viewer user token admin"', function () {
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": viewerToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "ADMIN"
                }
            });
            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.unprivileged" });
            return chakram.wait();
        });

        it('should respond 401 for "viewer user token editor"', function () {
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": viewerToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "EDITOR"
                }
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.unprivileged" });
            return chakram.wait();
        });

        it('should respond 401 for "viewer user token viewer"', function () {
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": viewerToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "VIEWER"
                }
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.unprivileged" });
            return chakram.wait();
        });

        it('should respond 401 for "viewer user token common"', function () {
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": viewerToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "COMMON"
                }
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.unprivileged" });
            return chakram.wait();
        });

        it('should respond 401 for "editor user token admin"', function () {
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": editorToken },
                'time': true,
                'body': {
                    "name": "string",
                    "identification": "string",
                    "email": "string",
                    "emailVerified": true,
                    "password": "string",
                    "passwordReset": true,
                    "role": "ADMIN"
                }
            });
            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.unprivileged" });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function () {
        //     const response = chakram.request('put', apiUrl + '/users', {
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(404);
        //     chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function () {
        //     const response = chakram.request('put', apiUrl + '/users', {
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(405);
        //     chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
        //     return chakram.wait();
        // });

    });
});