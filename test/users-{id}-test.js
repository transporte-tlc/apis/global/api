'use strict';
import * as chakram from "chakram";
import mocha from "mocha";
import { Users } from "../src/models";
import { AppTokenHelper, environments, UserTokenHelper } from "./helpers";

const apiUrl = environments.FULL_URL;
const apiToken = AppTokenHelper.getToken();
let adminToken, editorToken, viewerToken, commonToken;

before(function (done) {
    let tokens = [];
    UserTokenHelper.getAdminToken().then((tokenAdmin) => {
        adminToken = tokenAdmin;
        tokens.push(UserTokenHelper.getEditorToken().then((token) => {
            editorToken = token;
        }));
        tokens.push(UserTokenHelper.getViewerToken().then((token) => {
            viewerToken = token;
        }));
        tokens.push(UserTokenHelper.getCommonToken().then((token) => {
            commonToken = token;
        }));
        return Promise.all(tokens).then(() => {
            done();
        });
    });
});

describe('tests for /users/{id}', function () {
    describe('tests for get', function () {
        it('should respond 200 for "admin"', function () {
            const user = {
                id: 2,
                name: "Salvador",
                email: "pato2011usa@gmail.com",
                identification: "11973446",
                emailVerified: true,
                passwordReset: false,
                role: "COMMON",
                deleted: false
            };
            const response = chakram.request('get', apiUrl + '/users/2', {
                'qs': { "deleted": "NOT-DELETED" },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true
            });

            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "identification": { "type": "string" }, "email": { "type": "string" }, "emailVerified": { "type": "boolean" }, "password": { "type": "string", "format": "password" }, "passwordReset": { "type": "boolean" }, "role": { "type": "string", "enum": ["COMMON", "VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" }, "memberships": { "type": "array", "items": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "idUser": { "type": "integer", "format": "int64" }, "idAccount": { "type": "integer", "format": "int64" }, "account": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } }, "receiveEmail": { "type": "boolean" }, "role": { "type": "string", "enum": ["VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" } } } } } });
            chakram.expect(response).to.contain.json(user);
            return chakram.wait();
        });

        it('should respond 200 for "editor"', function () {
            const user = {
                id: 2,
                name: "Salvador",
                email: "pato2011usa@gmail.com",
                identification: "11973446",
                emailVerified: true,
                passwordReset: false,
                role: "COMMON",
                deleted: false
            };
            const response = chakram.request('get', apiUrl + '/users/2', {
                'qs': { "deleted": "NOT-DELETED" },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": editorToken },
                'time': true
            });

            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "identification": { "type": "string" }, "email": { "type": "string" }, "emailVerified": { "type": "boolean" }, "password": { "type": "string", "format": "password" }, "passwordReset": { "type": "boolean" }, "role": { "type": "string", "enum": ["COMMON", "VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" }, "memberships": { "type": "array", "items": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "idUser": { "type": "integer", "format": "int64" }, "idAccount": { "type": "integer", "format": "int64" }, "account": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } }, "receiveEmail": { "type": "boolean" }, "role": { "type": "string", "enum": ["VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" } } } } } });
            chakram.expect(response).to.contain.json(user);
            return chakram.wait();
        });

        it('should respond 200 for "viewer"', function () {
            const user = {
                id: 2,
                name: "Salvador",
                email: "pato2011usa@gmail.com",
                identification: "11973446",
                emailVerified: true,
                passwordReset: false,
                role: "COMMON",
                deleted: false
            };
            const response = chakram.request('get', apiUrl + '/users/2', {
                'qs': { "deleted": "NOT-DELETED" },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": viewerToken },
                'time': true
            });

            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "identification": { "type": "string" }, "email": { "type": "string" }, "emailVerified": { "type": "boolean" }, "password": { "type": "string", "format": "password" }, "passwordReset": { "type": "boolean" }, "role": { "type": "string", "enum": ["COMMON", "VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" }, "memberships": { "type": "array", "items": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "idUser": { "type": "integer", "format": "int64" }, "idAccount": { "type": "integer", "format": "int64" }, "account": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } }, "receiveEmail": { "type": "boolean" }, "role": { "type": "string", "enum": ["VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" } } } } } });
            chakram.expect(response).to.contain.json(user);
            return chakram.wait();
        });

        // it('should respond 204 for "Response is empty."', function() {
        //     const response = chakram.request('get', apiUrl + '/users/-81371039', {
        //         'qs': {"deleted":"ALL"},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(204);
        //     chakram.expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

        it('should respond 400 for "unregistered user"', function () {
            const response = chakram.request('get', apiUrl + '/users/3000', {
                'qs': { "deleted": "NOT-DELETED" },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
            chakram.expect(response).to.have.json({ "message": "error.not_exist.user" });
            return chakram.wait();
        });

        it('should respond 400 for "registered but active"', function () {
            const response = chakram.request('get', apiUrl + '/users/3', {
                'qs': { "deleted": "DELETED" },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
            chakram.expect(response).to.have.json({ "message": "error.not_exist.user" });
            return chakram.wait();
        });

        it('should respond 401 for "without tokens"', function () {
            const response = chakram.request('get', apiUrl + '/users/3', {
                'qs': { "deleted": "NOT-DELETED" },
                'headers': { "Content-Type": "application/json", "Accept": "application/json" },
                'time': true
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.token.user" });
            return chakram.wait();
        });

        it('should respond 401 for "app token"', function () {
            const response = chakram.request('get', apiUrl + '/users/3', {
                'qs': { "deleted": "NOT-DELETED" },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_user_key": commonToken },
                'time': true
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.token.app" });
            return chakram.wait();
        });

        it('should respond 401 for "user token"', function () {
            const response = chakram.request('get', apiUrl + '/users/3', {
                'qs': { "deleted": "NOT-DELETED" },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
                'time': true
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.token.user" });
            return chakram.wait();
        });

        it('should respond 401 for "common user"', function () {
            const response = chakram.request('get', apiUrl + '/users/3', {
                'qs': { "deleted": "NOT-DELETED" },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": commonToken },
                'time': true
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.unprivileged" });
            return chakram.wait();
        });

        // it('should respond 404 for "Entity not found."', function() {
        //     const response = chakram.request('get', apiUrl + '/users/22349742', {
        //         'qs': {"deleted":"ALL"},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(404);
        //     chakram.expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function() {
        //     const response = chakram.request('get', apiUrl + '/users/105526', {
        //         'qs': {"deleted":"DELETED"},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(405);
        //     chakram.expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });

    describe('tests for delete', function () {
        it('should respond 200 for "admin"', function () {
            const response = chakram.request('delete', apiUrl + '/users/5', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true
            });

            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" } } });
            chakram.expect(response).to.contain.json({ id: 5 });
            return chakram.wait();
        });

        it('should respond 200 for "editor"', function () {
            const response = chakram.request('delete', apiUrl + '/users/4', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true
            });

            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" } } });
            chakram.expect(response).to.contain.json({ id: 4 });
            return chakram.wait();
        });

        it('should respond 200 for "set deleted user active 5"', function () {
            const user = {
                "id": 5,
                role: "COMMON",
                "deleted": false
            };
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true,
                'body': user
            });

            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "identification": { "type": "string" }, "email": { "type": "string" }, "emailVerified": { "type": "boolean" }, "password": { "type": "string", "format": "password" }, "passwordReset": { "type": "boolean" }, "role": { "type": "string", "enum": ["COMMON", "VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" }, "memberships": { "type": "array", "items": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "idUser": { "type": "integer", "format": "int64" }, "idAccount": { "type": "integer", "format": "int64" }, "account": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } }, "receiveEmail": { "type": "boolean" }, "role": { "type": "string", "enum": ["VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" } } } } } });
            chakram.expect(response).to.contain.json(user);
            return chakram.wait();
        });

        it('should respond 200 for "set deleted user active 4"', function () {
            const user = {
                "id": 4,
                role: "VIEWER",
                "deleted": false
            };
            const response = chakram.request('put', apiUrl + '/users', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true,
                'body': user
            });

            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "identification": { "type": "string" }, "email": { "type": "string" }, "emailVerified": { "type": "boolean" }, "password": { "type": "string", "format": "password" }, "passwordReset": { "type": "boolean" }, "role": { "type": "string", "enum": ["COMMON", "VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" }, "memberships": { "type": "array", "items": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "idUser": { "type": "integer", "format": "int64" }, "idAccount": { "type": "integer", "format": "int64" }, "account": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } }, "receiveEmail": { "type": "boolean" }, "role": { "type": "string", "enum": ["VIEWER", "EDITOR", "ADMIN"] }, "deleted": { "type": "boolean" } } } } } });
            chakram.expect(response).to.contain.json(user);
            return chakram.wait();
        });

        // it('should respond 204 for "Response is empty."', function() {
        //     const response = chakram.request('delete', apiUrl + '/users/-33650305', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(204);
        //     chakram.expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        it('should respond 400 for "unregistered user"', function () {
            const response = chakram.request('delete', apiUrl + '/users/3000', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
            chakram.expect(response).to.have.json({ "message": "error.not_exist.user" });
            return chakram.wait();
        });

        it('should respond 401 for "without tokens"', function () {
            const response = chakram.request('delete', apiUrl + '/users/3', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json" },
                'time': true
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.token.user" });
            return chakram.wait();
        });

        it('should respond 401 for "app token"', function () {
            const response = chakram.request('delete', apiUrl + '/users/3', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_user_key": commonToken },
                'time': true
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.token.app" });
            return chakram.wait();
        });

        it('should respond 401 for "user token"', function () {
            const response = chakram.request('delete', apiUrl + '/users/3', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
                'time': true
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.token.user" });
            return chakram.wait();
        });

        it('should respond 401 for "common user"', function () {
            const response = chakram.request('delete', apiUrl + '/users/3', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": commonToken },
                'time': true
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.unprivileged" });
            return chakram.wait();
        });

        // it('should respond 404 for "Entity not found."', function() {
        //     const response = chakram.request('delete', apiUrl + '/users/80279464', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(404);
        //     chakram.expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function() {
        //     const response = chakram.request('delete', apiUrl + '/users/-2376752', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(405);
        //     chakram.expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });
});