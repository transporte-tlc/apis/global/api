'use strict';
import * as chakram from "chakram";
import mocha from "mocha";
import { AppTokenHelper, environments, UserTokenHelper } from "./helpers";

const apiUrl = environments.FULL_URL;
const apiToken = AppTokenHelper.getToken();
let adminToken, editorToken, viewerToken, commonToken;

before((done) => {
    let tokens = [];
    UserTokenHelper.getAdminToken().then(async (tokenAdmin) => {
        adminToken = tokenAdmin;
        tokens.push(UserTokenHelper.getEditorToken().then((token) => {
            editorToken = token;
        }));
        tokens.push(UserTokenHelper.getViewerToken().then((token) => {
            viewerToken = token;
        }));
        tokens.push(UserTokenHelper.getCommonToken().then((token) => {
            commonToken = token;
        }));
        await Promise.all(tokens);
        done();
    });
});

describe('tests for /users/password/forgot', function () {
    describe('tests for post', function () {
        it('should respond 200 for "successful operation"', function () {
            const response = chakram.request('post', apiUrl + '/users/password/forgot', {
                'body': { "email": "pato2011usa@gmail.com", "urlResponse": "http://hola.com" },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
                'time': true
            });

            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "message": { "type": "string" } } });
            chakram.expect(response).to.have.json({ "message": "OK" });
            return chakram.wait();
        });

        it('should respond 400 for "invalid email"', function () {
            const response = chakram.request('post', apiUrl + '/users/password/forgot', {
                'body': { "email": "in consequat", "urlResponse": "in enim" },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
                'time': true
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
            chakram.expect(response).to.have.json({ "message": "error.validation.user.email" });
            return chakram.wait();
        });

        it('should respond 400 for "malformed url"', function () {
            const response = chakram.request('post', apiUrl + '/users/password/forgot', {
                'body': { "email": "pato2011usa@gmail.com", "urlResponse": "in enim" },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
                'time': true
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
            chakram.expect(response).to.have.json({ "message": "error.validation.url" });
            return chakram.wait();
        });

        it('should respond 400 for "unregistered email"', function () {
            const response = chakram.request('post', apiUrl + '/users/password/forgot', {
                'body': { "email": "pato20usa@gmail.com", "urlResponse": "http://hola.com" },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
                'time': true
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "message": { "type": "string" } } });
            chakram.expect(response).to.have.json({ "message": "error.not_exist.user" });
            return chakram.wait();
        });

        it('should respond 401 for "no app token"', function () {
            const response = chakram.request('post', apiUrl + '/users/password/forgot', {
                'body': { "email": "nisi elit dolore", "urlResponse": "ex veniam" },
                'headers': { "Content-Type": "application/json", "Accept": "application/json" },
                'time': true
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.token.app" });
            return chakram.wait();
        });

        // it('should respond 404 for "Entity not found."', function() {
        //     const response = chakram.request('post', apiUrl + '/users/password/forgot', {
        //         'body': {"email":"occaecat ut sint nisi consectetur","urlResponse":"culpa ad quis "},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(404);
        //     chakram.expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

        // it('should respond 405 for "Illegal input for operation."', function() {
        //     const response = chakram.request('post', apiUrl + '/users/password/forgot', {
        //         'body': {"email":"sunt velit eiusmod proident","urlResponse":"dolore ex magna dolor commodo"},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(405);
        //     chakram.expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });
});