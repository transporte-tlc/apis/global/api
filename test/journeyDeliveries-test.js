'use strict';
var mocha = require('mocha');
var chakram = require('chakram');
var request = chakram.request;
var expect = chakram.expect;

const apiUrl = 'http://localhost:3000';

describe('tests for /journeyDeliveries', function() {
    describe('tests for get', function() {
        it('should respond 200 for "Request OK."', function() {
            this.skip();
            var response = request('get', apiUrl + '/journeyDeliveries', {
                'qs': {"skip":-68342580,"limit":54355388,"orderBy":"ut ea","filter":"elit commodo","deleted":"ALL","metadata":true,"idUser":76573054,"idAccount":-68486938},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(200);
            expect(response).to.have.schema({"properties":{"metadata":{"type":"object","properties":{"first":{"type":"integer","format":"int64"},"prev":{"type":"integer","format":"int64"},"self":{"type":"integer","format":"int64"},"next":{"type":"integer","format":"int64"},"last":{"type":"integer","format":"int64"}}},"items":{"type":"array","items":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idJourney":{"type":"integer","format":"int64"},"journey":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idCarrier":{"type":"integer","format":"int64"},"carrier":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocationStart":{"type":"integer","format":"int64"},"locationStart":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocationEnd":{"type":"integer","format":"int64"},"locationEnd":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"startDate":{"type":"string","format":"date"},"endDate":{"type":"string","format":"date"},"deleted":{"type":"boolean"}}},"idDelivery":{"type":"integer","format":"int64"},"delivery":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idAccount":{"type":"integer","format":"int64"},"account":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocation":{"type":"integer","format":"int64"},"location":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"issuedAt":{"type":"string"},"issuedBy":{"type":"string"},"reference":{"type":"string"},"quantity":{"type":"integer","format":"int64"},"weight":{"type":"number","format":"float"},"volume":{"type":"number","format":"float"},"value":{"type":"number","format":"float"},"comment":{"type":"string"},"deleted":{"type":"boolean"}}},"quantity":{"type":"integer","format":"int64"},"weight":{"type":"number","format":"float"},"volume":{"type":"number","format":"float"},"value":{"type":"number","format":"float"},"deleted":{"type":"boolean"}}}}}});
            return chakram.wait();
        });


        it('should respond 204 for "Response is empty."', function() {
            this.skip();
            var response = request('get', apiUrl + '/journeyDeliveries', {
                'qs': {"skip":23575884,"limit":58957574,"orderBy":"nostrud reprehenderit nisi","filter":"tempor cupidatat aliquip velit non","deleted":"NOT-DELETED","metadata":true,"idUser":-61337056,"idAccount":1607487},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(204);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 400 for "Some parameters are missing or badly entered."', function() {
            this.skip();
            var response = request('get', apiUrl + '/journeyDeliveries', {
                'qs': {"skip":-94436463,"limit":-35358562,"orderBy":"irure voluptate","filter":"amet non sit commodo mollit","deleted":"ALL","metadata":false,"idUser":-4365220,"idAccount":83426508},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(400);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function() {
            this.skip();
            var response = request('get', apiUrl + '/journeyDeliveries', {
                'qs': {"skip":-54032866,"limit":1129392,"orderBy":"do labore","filter":"elit in","deleted":"DELETED","metadata":true,"idUser":-47112439,"idAccount":67031500},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function() {
        //     var response = request('get', apiUrl + '/journeyDeliveries', {
        //         'qs': {"skip":85257430,"limit":-97065093,"orderBy":"laboris incididunt in dolor","filter":"eu","deleted":"DELETED","metadata":true,"idUser":-49593675,"idAccount":-22773351},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(404);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function() {
        //     var response = request('get', apiUrl + '/journeyDeliveries', {
        //         'qs': {"skip":69134269,"limit":-38714171,"orderBy":"ullamco esse sit","filter":"Duis deserunt est consectetur laboris","deleted":"NOT-DELETED","metadata":true,"idUser":52272694,"idAccount":44605796},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(405);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });

    describe('tests for post', function() {
        it('should respond 200 for "Request OK."', function() {
            this.skip();
            var response = request('post', apiUrl + '/journeyDeliveries', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(200);
            expect(response).to.have.schema({"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idJourney":{"type":"integer","format":"int64"},"journey":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idCarrier":{"type":"integer","format":"int64"},"carrier":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocationStart":{"type":"integer","format":"int64"},"locationStart":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocationEnd":{"type":"integer","format":"int64"},"locationEnd":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"startDate":{"type":"string","format":"date"},"endDate":{"type":"string","format":"date"},"deleted":{"type":"boolean"}}},"idDelivery":{"type":"integer","format":"int64"},"delivery":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idAccount":{"type":"integer","format":"int64"},"account":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocation":{"type":"integer","format":"int64"},"location":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"issuedAt":{"type":"string"},"issuedBy":{"type":"string"},"reference":{"type":"string"},"quantity":{"type":"integer","format":"int64"},"weight":{"type":"number","format":"float"},"volume":{"type":"number","format":"float"},"value":{"type":"number","format":"float"},"comment":{"type":"string"},"deleted":{"type":"boolean"}}},"quantity":{"type":"integer","format":"int64"},"weight":{"type":"number","format":"float"},"volume":{"type":"number","format":"float"},"value":{"type":"number","format":"float"},"deleted":{"type":"boolean"}}});
            return chakram.wait();
        });


        // it('should respond 204 for "Response is empty."', function() {
        //     var response = request('post', apiUrl + '/journeyDeliveries', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(204);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        it('should respond 400 for "Some parameters are missing or badly entered."', function() {
            this.skip();
            var response = request('post', apiUrl + '/journeyDeliveries', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(400);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function() {
            this.skip();
            var response = request('post', apiUrl + '/journeyDeliveries', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function() {
        //     var response = request('post', apiUrl + '/journeyDeliveries', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(404);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function() {
        //     var response = request('post', apiUrl + '/journeyDeliveries', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(405);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });

    describe('tests for put', function() {
        it('should respond 200 for "Request OK."', function() {
            this.skip();
            var response = request('put', apiUrl + '/journeyDeliveries', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(200);
            expect(response).to.have.schema({"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idJourney":{"type":"integer","format":"int64"},"journey":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idCarrier":{"type":"integer","format":"int64"},"carrier":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocationStart":{"type":"integer","format":"int64"},"locationStart":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocationEnd":{"type":"integer","format":"int64"},"locationEnd":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"startDate":{"type":"string","format":"date"},"endDate":{"type":"string","format":"date"},"deleted":{"type":"boolean"}}},"idDelivery":{"type":"integer","format":"int64"},"delivery":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idAccount":{"type":"integer","format":"int64"},"account":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocation":{"type":"integer","format":"int64"},"location":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"issuedAt":{"type":"string"},"issuedBy":{"type":"string"},"reference":{"type":"string"},"quantity":{"type":"integer","format":"int64"},"weight":{"type":"number","format":"float"},"volume":{"type":"number","format":"float"},"value":{"type":"number","format":"float"},"comment":{"type":"string"},"deleted":{"type":"boolean"}}},"quantity":{"type":"integer","format":"int64"},"weight":{"type":"number","format":"float"},"volume":{"type":"number","format":"float"},"value":{"type":"number","format":"float"},"deleted":{"type":"boolean"}}});
            return chakram.wait();
        });


        // it('should respond 204 for "Response is empty."', function() {
        //     var response = request('put', apiUrl + '/journeyDeliveries', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(204);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        it('should respond 400 for "Some parameters are missing or badly entered."', function() {
            this.skip();
            var response = request('put', apiUrl + '/journeyDeliveries', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(400);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function() {
            this.skip();
            var response = request('put', apiUrl + '/journeyDeliveries', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function() {
        //     var response = request('put', apiUrl + '/journeyDeliveries', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(404);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function() {
        //     var response = request('put', apiUrl + '/journeyDeliveries', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(405);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });
});