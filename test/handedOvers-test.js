'use strict';
var mocha = require('mocha');
var chakram = require('chakram');
var request = chakram.request;
var expect = chakram.expect;

const apiUrl = 'http://localhost:3000';

describe('tests for /handedOvers', function() {
    describe('tests for get', function() {
        it('should respond 200 for "Request OK."', function() {
            this.skip();
            var response = request('get', apiUrl + '/handedOvers', {
                'qs': {"skip":-32042671,"limit":-39598286,"orderBy":"do aliquip","filter":"in consequat","deleted":"ALL","metadata":false,"idUser":-75423083,"idAccount":92833731},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(200);
            expect(response).to.have.schema({"properties":{"metadata":{"type":"object","properties":{"first":{"type":"integer","format":"int64"},"prev":{"type":"integer","format":"int64"},"self":{"type":"integer","format":"int64"},"next":{"type":"integer","format":"int64"},"last":{"type":"integer","format":"int64"}}},"items":{"type":"array","items":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idDelivery":{"type":"integer","format":"int64"},"delivery":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idAccount":{"type":"integer","format":"int64"},"account":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocation":{"type":"integer","format":"int64"},"location":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"issuedAt":{"type":"string"},"issuedBy":{"type":"string"},"reference":{"type":"string"},"quantity":{"type":"integer","format":"int64"},"weight":{"type":"number","format":"float"},"volume":{"type":"number","format":"float"},"value":{"type":"number","format":"float"},"comment":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocation":{"type":"integer","format":"int64"},"location":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"issuedAt":{"type":"string","format":"date-time"},"issuedTo":{"type":"string"},"quantity":{"type":"integer","format":"int64"},"weight":{"type":"number","format":"float"},"volume":{"type":"number","format":"float"},"value":{"type":"number","format":"float"},"comment":{"type":"string"},"deleted":{"type":"boolean"}}}}}});
            return chakram.wait();
        });


        it('should respond 204 for "Response is empty."', function() {
            this.skip();
            var response = request('get', apiUrl + '/handedOvers', {
                'qs': {"skip":32577317,"limit":-19031907,"orderBy":"pariatur minim exercitation nulla","filter":"fugiat occaecat non do proident","deleted":"ALL","metadata":false,"idUser":-58869424,"idAccount":-15458035},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(204);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 400 for "Some parameters are missing or badly entered."', function() {
            this.skip();
            var response = request('get', apiUrl + '/handedOvers', {
                'qs': {"skip":25484615,"limit":81234881,"orderBy":"consectetur","filter":"velit anim veniam","deleted":"NOT-DELETED","metadata":true,"idUser":67544262,"idAccount":89210674},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(400);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function() {
            this.skip();
            var response = request('get', apiUrl + '/handedOvers', {
                'qs': {"skip":57833002,"limit":-64989525,"orderBy":"incididunt qui do in","filter":"ipsum Excepteur cillum pariatur","deleted":"NOT-DELETED","metadata":false,"idUser":-17542986,"idAccount":6802668},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function() {
        //     var response = request('get', apiUrl + '/handedOvers', {
        //         'qs': {"skip":-85029501,"limit":-66387151,"orderBy":"sed","filter":"aute","deleted":"ALL","metadata":true,"idUser":-59943528,"idAccount":18613849},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(404);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function() {
        //     var response = request('get', apiUrl + '/handedOvers', {
        //         'qs': {"skip":58851470,"limit":-76155891,"orderBy":"ea laborum sit id","filter":"id laboris exercitation fugi","deleted":"ALL","metadata":true,"idUser":-11188328,"idAccount":86611923},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(405);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });

    describe('tests for post', function() {
        it('should respond 200 for "Request OK."', function() {
            this.skip();
            var response = request('post', apiUrl + '/handedOvers', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(200);
            expect(response).to.have.schema({"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idDelivery":{"type":"integer","format":"int64"},"delivery":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idAccount":{"type":"integer","format":"int64"},"account":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocation":{"type":"integer","format":"int64"},"location":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"issuedAt":{"type":"string"},"issuedBy":{"type":"string"},"reference":{"type":"string"},"quantity":{"type":"integer","format":"int64"},"weight":{"type":"number","format":"float"},"volume":{"type":"number","format":"float"},"value":{"type":"number","format":"float"},"comment":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocation":{"type":"integer","format":"int64"},"location":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"issuedAt":{"type":"string","format":"date-time"},"issuedTo":{"type":"string"},"quantity":{"type":"integer","format":"int64"},"weight":{"type":"number","format":"float"},"volume":{"type":"number","format":"float"},"value":{"type":"number","format":"float"},"comment":{"type":"string"},"deleted":{"type":"boolean"}}});
            return chakram.wait();
        });


        // it('should respond 204 for "Response is empty."', function() {
        //     var response = request('post', apiUrl + '/handedOvers', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(204);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        it('should respond 400 for "Some parameters are missing or badly entered."', function() {
            this.skip();
            var response = request('post', apiUrl + '/handedOvers', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(400);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function() {
            this.skip();
            var response = request('post', apiUrl + '/handedOvers', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        it('should respond 404 for "Entity not found."', function() {
            this.skip();
            var response = request('post', apiUrl + '/handedOvers', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(404);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 405 for "Illegal input for operation."', function() {
            this.skip();
            var response = request('post', apiUrl + '/handedOvers', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(405);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });

    });

    describe('tests for put', function() {
        it('should respond 200 for "Request OK."', function() {
            this.skip();
            var response = request('put', apiUrl + '/handedOvers', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(200);
            expect(response).to.have.schema({"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idDelivery":{"type":"integer","format":"int64"},"delivery":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idAccount":{"type":"integer","format":"int64"},"account":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocation":{"type":"integer","format":"int64"},"location":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"issuedAt":{"type":"string"},"issuedBy":{"type":"string"},"reference":{"type":"string"},"quantity":{"type":"integer","format":"int64"},"weight":{"type":"number","format":"float"},"volume":{"type":"number","format":"float"},"value":{"type":"number","format":"float"},"comment":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocation":{"type":"integer","format":"int64"},"location":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"issuedAt":{"type":"string","format":"date-time"},"issuedTo":{"type":"string"},"quantity":{"type":"integer","format":"int64"},"weight":{"type":"number","format":"float"},"volume":{"type":"number","format":"float"},"value":{"type":"number","format":"float"},"comment":{"type":"string"},"deleted":{"type":"boolean"}}});
            return chakram.wait();
        });


        // it('should respond 204 for "Response is empty."', function() {
        //     var response = request('put', apiUrl + '/handedOvers', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(204);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        it('should respond 400 for "Some parameters are missing or badly entered."', function() {
            this.skip();
            var response = request('put', apiUrl + '/handedOvers', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(400);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function() {
            this.skip();
            var response = request('put', apiUrl + '/handedOvers', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function() {
        //     var response = request('put', apiUrl + '/handedOvers', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(404);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function() {
        //     var response = request('put', apiUrl + '/handedOvers', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(405);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });
});