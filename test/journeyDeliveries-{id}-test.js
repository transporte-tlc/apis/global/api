'use strict';
var mocha = require('mocha');
var chakram = require('chakram');
var request = chakram.request;
var expect = chakram.expect;

const apiUrl = 'http://localhost:3000';

describe('tests for /journeyDeliveries/{id}', function() {
    describe('tests for get', function() {
        it('should respond 200 for "Request OK."', function() {
            this.skip();
            var response = request('get', apiUrl + '/journeyDeliveries/-23915173', {
                'qs': {"deleted":"DELETED","idUser":-67857342},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(200);
            expect(response).to.have.schema({"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idJourney":{"type":"integer","format":"int64"},"journey":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idCarrier":{"type":"integer","format":"int64"},"carrier":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocationStart":{"type":"integer","format":"int64"},"locationStart":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocationEnd":{"type":"integer","format":"int64"},"locationEnd":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"startDate":{"type":"string","format":"date"},"endDate":{"type":"string","format":"date"},"deleted":{"type":"boolean"}}},"idDelivery":{"type":"integer","format":"int64"},"delivery":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idAccount":{"type":"integer","format":"int64"},"account":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocation":{"type":"integer","format":"int64"},"location":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"issuedAt":{"type":"string"},"issuedBy":{"type":"string"},"reference":{"type":"string"},"quantity":{"type":"integer","format":"int64"},"weight":{"type":"number","format":"float"},"volume":{"type":"number","format":"float"},"value":{"type":"number","format":"float"},"comment":{"type":"string"},"deleted":{"type":"boolean"}}},"quantity":{"type":"integer","format":"int64"},"weight":{"type":"number","format":"float"},"volume":{"type":"number","format":"float"},"value":{"type":"number","format":"float"},"deleted":{"type":"boolean"}}});
            return chakram.wait();
        });


        // it('should respond 204 for "Response is empty."', function() {
        //     var response = request('get', apiUrl + '/journeyDeliveries/20146067', {
        //         'qs': {"deleted":"ALL","idUser":57111367},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(204);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        it('should respond 400 for "Some parameters are missing or badly entered."', function() {
            this.skip();
            var response = request('get', apiUrl + '/journeyDeliveries/-36786917', {
                'qs': {"deleted":"NOT-DELETED","idUser":61715362},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(400);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function() {
            this.skip();
            var response = request('get', apiUrl + '/journeyDeliveries/26570009', {
                'qs': {"deleted":"DELETED","idUser":-42729487},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function() {
        //     var response = request('get', apiUrl + '/journeyDeliveries/27996009', {
        //         'qs': {"deleted":"ALL","idUser":65389756},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(404);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function() {
        //     var response = request('get', apiUrl + '/journeyDeliveries/-63698338', {
        //         'qs': {"deleted":"DELETED","idUser":49173644},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(405);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });

    describe('tests for delete', function() {
        it('should respond 200 for "Request OK."', function() {
            this.skip();
            var response = request('delete', apiUrl + '/journeyDeliveries/-56811010', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(200);
            expect(response).to.have.schema({"type":"object","properties":{"id":{"type":"integer","format":"int64"}}});
            return chakram.wait();
        });


        // it('should respond 204 for "Response is empty."', function() {
        //     var response = request('delete', apiUrl + '/journeyDeliveries/-82123231', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(204);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        it('should respond 400 for "Some parameters are missing or badly entered."', function() {
            this.skip();
            var response = request('delete', apiUrl + '/journeyDeliveries/-75003892', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(400);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function() {
            this.skip();
            var response = request('delete', apiUrl + '/journeyDeliveries/22102974', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function() {
        //     var response = request('delete', apiUrl + '/journeyDeliveries/48567623', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(404);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function() {
        //     var response = request('delete', apiUrl + '/journeyDeliveries/-80374265', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(405);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });
});