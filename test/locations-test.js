'use strict';
var mocha = require('mocha');
var chakram = require('chakram');
var request = chakram.request;
var expect = chakram.expect;

const apiUrl = 'http://localhost:3000';

describe('tests for /locations', function() {
    describe('tests for get', function() {
        it('should respond 200 for "Request OK."', function() {
            this.skip();
            var response = request('get', apiUrl + '/locations', {
                'qs': {"skip":18528791,"limit":-42425731,"orderBy":"ut","filter":"anim ","deleted":"DELETED","metadata":true},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(200);
            expect(response).to.have.schema({"properties":{"metadata":{"type":"object","properties":{"first":{"type":"integer","format":"int64"},"prev":{"type":"integer","format":"int64"},"self":{"type":"integer","format":"int64"},"next":{"type":"integer","format":"int64"},"last":{"type":"integer","format":"int64"}}},"items":{"type":"array","items":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}}}}});
            return chakram.wait();
        });


        it('should respond 204 for "Response is empty."', function() {
            this.skip();
            var response = request('get', apiUrl + '/locations', {
                'qs': {"skip":-73269010,"limit":-90712936,"orderBy":"dolore","filter":"cillum","deleted":"ALL","metadata":true},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(204);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 400 for "Some parameters are missing or badly entered."', function() {
            this.skip();
            var response = request('get', apiUrl + '/locations', {
                'qs': {"skip":3369900,"limit":14732822,"orderBy":"dolore","filter":"minim dolor elit adipisicing","deleted":"DELETED","metadata":false},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(400);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function() {
            this.skip();
            var response = request('get', apiUrl + '/locations', {
                'qs': {"skip":-97190898,"limit":20060005,"orderBy":"irure dolor labo","filter":"Ut quis commodo adipisicing ut","deleted":"DELETED","metadata":false},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function() {
        //     var response = request('get', apiUrl + '/locations', {
        //         'qs': {"skip":95397434,"limit":-96728200,"orderBy":"dolore aliqua mollit minim incididunt","filter":"sit ullamco culpa","deleted":"NOT-DELETED","metadata":false},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(404);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function() {
        //     var response = request('get', apiUrl + '/locations', {
        //         'qs': {"skip":-87088819,"limit":-33040006,"orderBy":"Ut","filter":"consequat ipsum in labore dolore","deleted":"DELETED","metadata":true},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(405);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });

    describe('tests for post', function() {
        it('should respond 200 for "Request OK."', function() {
            this.skip();
            var response = request('post', apiUrl + '/locations', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(200);
            expect(response).to.have.schema({"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}});
            return chakram.wait();
        });


        // it('should respond 204 for "Response is empty."', function() {
        //     var response = request('post', apiUrl + '/locations', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(204);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        it('should respond 400 for "Some parameters are missing or badly entered."', function() {
            this.skip();
            var response = request('post', apiUrl + '/locations', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(400);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function() {
            this.skip();
            var response = request('post', apiUrl + '/locations', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function() {
        //     var response = request('post', apiUrl + '/locations', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(404);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function() {
        //     var response = request('post', apiUrl + '/locations', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(405);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });

    describe('tests for put', function() {
        it('should respond 200 for "Request OK."', function() {
            this.skip();
            var response = request('put', apiUrl + '/locations', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(200);
            expect(response).to.have.schema({"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}});
            return chakram.wait();
        });


        // it('should respond 204 for "Response is empty."', function() {
        //     var response = request('put', apiUrl + '/locations', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(204);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        it('should respond 400 for "Some parameters are missing or badly entered."', function() {
            this.skip();
            var response = request('put', apiUrl + '/locations', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(400);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function() {
            this.skip();
            var response = request('put', apiUrl + '/locations', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function() {
        //     var response = request('put', apiUrl + '/locations', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(404);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function() {
        //     var response = request('put', apiUrl + '/locations', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(405);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });
});