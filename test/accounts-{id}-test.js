'use strict';
var mocha = require('mocha');
var chakram = require('chakram');
var request = chakram.request;
var expect = chakram.expect;

const apiUrl = 'http://localhost:3000';

describe('tests for /accounts/{id}', function() {
    describe('tests for get', function() {
        it('should respond 200 for "Request OK."', function() {
            this.skip();
            var response = request('get', apiUrl + '/accounts/35701594', {
                'qs': {"deleted":"DELETED"},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(200);
            expect(response).to.have.schema({"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}});
            return chakram.wait();
        });


        // it('should respond 204 for "Response is empty."', function() {
        //     var response = request('get', apiUrl + '/accounts/29387013', {
        //         'qs': {"deleted":"NOT-DELETED"},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(204);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        it('should respond 400 for "Some parameters are missing or badly entered."', function() {
            this.skip();
            var response = request('get', apiUrl + '/accounts/83746909', {
                'qs': {"deleted":"NOT-DELETED"},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(400);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function() {
            this.skip();
            var response = request('get', apiUrl + '/accounts/-88206703', {
                'qs': {"deleted":"ALL"},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function() {
        //     var response = request('get', apiUrl + '/accounts/51094390', {
        //         'qs': {"deleted":"ALL"},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(404);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function() {
        //     var response = request('get', apiUrl + '/accounts/-80173830', {
        //         'qs': {"deleted":"ALL"},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(405);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });

    describe('tests for delete', function() {
        it('should respond 200 for "Request OK."', function() {
            this.skip();
            var response = request('delete', apiUrl + '/accounts/-20013961', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(200);
            expect(response).to.have.schema({"type":"object","properties":{"id":{"type":"integer","format":"int64"}}});
            return chakram.wait();
        });


        // it('should respond 204 for "Response is empty."', function() {
        //     var response = request('delete', apiUrl + '/accounts/17586918', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(204);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        it('should respond 400 for "Some parameters are missing or badly entered."', function() {
            this.skip();
            var response = request('delete', apiUrl + '/accounts/66656474', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(400);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function() {
            this.skip();
            var response = request('delete', apiUrl + '/accounts/88014895', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function() {
        //     var response = request('delete', apiUrl + '/accounts/-27089620', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(404);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function() {
        //     var response = request('delete', apiUrl + '/accounts/-65225505', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(405);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });
});