'use strict';
var mocha = require('mocha');
var chakram = require('chakram');
var request = chakram.request;
var expect = chakram.expect;

const apiUrl = 'http://localhost:3000';

describe('tests for /users/email/request', function() {
    describe('tests for post', function() {
        it('should respond 200 for "Request OK if email is registered."', function() {
            this.skip();
            var response = request('post', apiUrl + '/users/email/request', {
                'body': {"email":"ut","urlResponse":"eu vo"},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(200);
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function() {
            this.skip();
            var response = request('post', apiUrl + '/users/email/request', {
                'body': {"email":"cillum exercitation","urlResponse":"pariatur ad deseru"},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        // it('should respond 404 for "Email does not exists."', function() {
        //     var response = request('post', apiUrl + '/users/email/request', {
        //         'body': {"email":"sit eu","urlResponse":"ea aute in aliqua"},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(404);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function() {
        //     var response = request('post', apiUrl + '/users/email/request', {
        //         'body': {"email":"labore adipisicing veniam quis dolore","urlResponse":"pariatur"},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(405);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });
});