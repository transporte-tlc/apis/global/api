'use strict';
import * as chakram from "chakram";
import mocha from "mocha";
import { AppTokenHelper, environments, UserTokenHelper } from "./helpers";

const apiUrl = environments.FULL_URL;
const apiToken = AppTokenHelper.getToken();
let adminToken, editorToken, viewerToken, commonToken;

const passwordPrevious = "SuperPassword2018!!";
const passwordNew = "@!!1920tlc"
const passwordNew2 = "@!!tlc1920222"

before(function (done) {
    let tokens = [];
    UserTokenHelper.getAdminToken().then((tokenAdmin) => {
        adminToken = tokenAdmin;
        tokens.push(UserTokenHelper.getEditorToken().then((token) => {
            editorToken = token;
        }));
        tokens.push(UserTokenHelper.getViewerToken().then((token) => {
            viewerToken = token;
        }));
        tokens.push(UserTokenHelper.getCommonToken().then((token) => {
            commonToken = token;
        }));
        return Promise.all(tokens).then(() => {
            done();
        });
    });
});

describe('tests for /users/password/change', function () {
    describe('tests for get', function () {
        it('should respond 200 for "admin"', function () {
            const response = chakram.request('get', apiUrl + '/users/password/change', {
                'qs': { "oldPassword": passwordPrevious, "newPassword": passwordNew, "idUser": 2 },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true
            });

            // chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "message": { "type": "string" } } });
            chakram.expect(response).to.have.json({ "message": "OK" });
            return chakram.wait();
        });

        it('should respond 200 for "editor"', function () {
            const response = chakram.request('get', apiUrl + '/users/password/change', {
                'qs': { "oldPassword": passwordNew, "newPassword": passwordNew2, "idUser": 2 },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": editorToken },
                'time': true
            });

            // chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "message": { "type": "string" } } });
            chakram.expect(response).to.have.json({ "message": "OK" });
            return chakram.wait();
        });

        it('should respond 200 for "common"', function () {
            const response = chakram.request('get', apiUrl + '/users/password/change', {
                'qs': { "oldPassword": passwordNew2, "newPassword": passwordPrevious, "idUser": 2 },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": commonToken },
                'time': true
            });

            // chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "message": { "type": "string" } } });
            chakram.expect(response).to.have.json({ "message": "OK" });
            return chakram.wait();
        });

        // it('should respond 400 for "same password"', function () {
        //     const response = chakram.request('get', apiUrl + '/users/password/change', {
        //         'qs': { "oldPassword": "aliqua", "newPassword": "aliqua", "idUser": 98043441 },
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(400);
        //     chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
        //     chakram.expect(response).to.have.json({ "message": "error.password.matched" });
        //     return chakram.wait();
        // });

        // it('should respond 400 for "insecure password"', function () {
        //     const response = chakram.request('get', apiUrl + '/users/password/change', {
        //         'qs': { "oldPassword": "aliqua2", "newPassword": "aliqua", "idUser": 98043441 },
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(400);
        //     chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
        //     chakram.expect(response).to.have.json({ "message": "error.password.insecure" });
        //     return chakram.wait();
        // });

        // it('should respond 400 for "not registered user"', function () {
        //     const response = chakram.request('get', apiUrl + '/users/password/change', {
        //         'qs': { "oldPassword": "aliqua", "newPassword": "aliqua2@@@@", "idUser": 98043441 },
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(400);
        //     chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
        //     chakram.expect(response).to.have.json({ "message": "error.not_exist.user" });
        //     return chakram.wait();
        // });

        // it('should respond 400 for "old password does not match"', function () {
        //     const response = chakram.request('get', apiUrl + '/users/password/change', {
        //         'qs': { "oldPassword": "aliqua", "newPassword": "aliqua2@@@@", "idUser": 3 },
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(400);
        //     chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
        //     chakram.expect(response).to.have.json({ "message": "error.password.not_matched" });
        //     return chakram.wait();
        // });

        // it('should respond 401 for "user token"', function () {
        //     const response = chakram.request('get', apiUrl + '/users/password/change', {
        //         'qs': { "oldPassword": "nulla", "newPassword": "id sint dolor", "idUser": 73776440 },
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(401);
        //     chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
        //     chakram.expect(response).to.have.json({ "message": "error.auth.token.user" });
        //     return chakram.wait();
        // });

        // it('should respond 401 for "api token"', function () {
        //     const response = chakram.request('get', apiUrl + '/users/password/change', {
        //         'qs': { "oldPassword": "nulla", "newPassword": "id sint dolor", "idUser": 73776440 },
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_user_key": viewerToken },
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(401);
        //     chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
        //     chakram.expect(response).to.have.json({ "message": "error.auth.token.app" });
        //     return chakram.wait();
        // });

        // it('should respond 401 for "viewer unprivileged"', function () {
        //     const response = chakram.request('get', apiUrl + '/users/password/change', {
        //         'qs': { "oldPassword": "nulla", "newPassword": "id sint dolor", "idUser": 3 },
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": viewerToken },
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(401);
        //     chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
        //     chakram.expect(response).to.have.json({ "message": "error.auth.unprivileged" });
        //     return chakram.wait();
        // });

        // it('should respond 401 for "unprivileged"', function () {
        //     const response = chakram.request('get', apiUrl + '/users/password/change', {
        //         'qs': { "oldPassword": "nulla", "newPassword": "id sint dolor", "idUser": 3 },
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": commonToken },
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(401);
        //     chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
        //     chakram.expect(response).to.have.json({ "message": "error.auth.unprivileged" });
        //     return chakram.wait();
        // });

        // it('should respond 404 for "Entity not found."', function() {
        //     const response = chakram.request('get', apiUrl + '/users/password/change', {
        //         'qs': {"oldPassword":"laboris sunt est commodo","newPassword":"mollit Excepteur","idUser":26542305},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(404);
        //     chakram.expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function() {
        //     const response = chakram.request('get', apiUrl + '/users/password/change', {
        //         'qs': {"oldPassword":"ea labore enim","newPassword":"tempor ipsum quis fugiat Ut","idUser":-13924034},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(405);
        //     chakram.expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });
});