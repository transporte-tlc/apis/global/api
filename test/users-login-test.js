'use strict';
import * as chakram from "chakram";
import mocha from "mocha";
import { Users } from "../src/models";
import { AppTokenHelper, environments } from "./helpers";

const apiUrl = environments.FULL_URL;
const apiToken = AppTokenHelper.getToken();

describe('tests for /users/login', function () {
    describe('tests for get', function () {
        it('should respond 200 for "successful operation"', function () {
            const response = chakram.request('get', apiUrl + '/users/login', {
                'qs': { "email": "patricio.perpetua.arg@gmail.com", "password": "TlC2018!@" },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
                'time': true
            });

            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "user": Users ,"token": { "type": "string" }, "expDate": { "type": "string", "format": "date-time" } } });
            chakram.expect(response).to.contain.json({ "user": { id: 0 } });
            return chakram.wait();
        });


        it('should respond 400 for "Email not registered"', function () {
            const response = chakram.request('get', apiUrl + '/users/login', {
                'qs': { "email": "patricio.perpetua@gmail.com", "password": "null" },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
                'time': true
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.json({ "message": "error.email.not_registered" });
            return chakram.wait();
        });

        it('should respond 400 for "Wrong password"', function () {
            const response = chakram.request('get', apiUrl + '/users/login', {
                'qs': { "email": "patricio.perpetua.arg@gmail.com", "password": "gmail" },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
                'time': true
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.json({ "message": "error.password.not_matched" });
            return chakram.wait();
        });

        it('should respond 400 for "Email not confirmed"', function () {
            const response = chakram.request('get', apiUrl + '/users/login', {
                'qs': { "email": "main@tlc.singleton.com.ar", "password": "Editora!!2019" },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
                'time': true
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.json({ "message": "error.email.not_verified" });
            return chakram.wait();
        });

        it('should respond 400 for "Password reset"', function () {
            const response = chakram.request('get', apiUrl + '/users/login', {
                'qs': { "email": "soporte@singleton.com.ar", "password": "@!!tlc1920" },
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
                'time': true
            });

            chakram.expect(response).to.have.status(400);
            chakram.expect(response).to.have.json({ "message": "error.password.reset" });
            return chakram.wait();
        });

        it('should respond 401 for "Unauthorized"', function () {
            const response = chakram.request('get', apiUrl + '/users/login', {
                'qs': { "email": "patricio", "password": "dd" },
                'headers': { "Content-Type": "application/json", "Accept": "application/json" },
                'time': true
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.token.app" });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function () {
        //     const response = chakram.request('get', apiUrl + '/users/login', {
        //         'qs': { "email": "" },
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(404);
        //     chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function () {
        //     const response = chakram.request('get', apiUrl + '/users/login', {
        //         'qs': { "email": "", "password": "Ut" },
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(405);
        //     chakram.expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
        //     return chakram.wait();
        // });

    });
});