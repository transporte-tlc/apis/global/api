'use strict';
import * as chakram from "chakram";
import mocha from "mocha";
import { Users } from "../src/models";
import { AppTokenHelper, environments, UserTokenHelper } from "./helpers";

const apiUrl = environments.FULL_URL;
const apiToken = AppTokenHelper.getToken();
let adminToken, editorToken, viewerToken, commonToken;

before(function (done) {
    let tokens = [];
    UserTokenHelper.getAdminToken().then((tokenAdmin) => {
        adminToken = tokenAdmin;
        tokens.push(UserTokenHelper.getEditorToken().then((token) => {
            editorToken = token;
        }));
        tokens.push(UserTokenHelper.getViewerToken().then((token) => {
            viewerToken = token;
        }));
        tokens.push(UserTokenHelper.getCommonToken().then((token) => {
            commonToken = token;
        }));
        return Promise.all(tokens).then(() => {
            done();
        });
    });
});

describe('tests for /users/verifyToken', function () {
    describe('tests for get', function () {
        it('should respond 200 for "user admin"', function () {
            const response = chakram.request('get', apiUrl + '/users/verifyToken', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": adminToken },
                'time': true
            });

            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "user": Users ,"token": { "type": "string" }, "expDate": { "type": "string", "format": "date-time" } } });
            chakram.expect(response).to.contain.json({ "user": { id: 1, role: "ADMIN" } });
            return chakram.wait();
        });

        it('should respond 200 for "user editor"', function () {
            const response = chakram.request('get', apiUrl + '/users/verifyToken', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": editorToken },
                'time': true
            });

            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "user": Users ,"token": { "type": "string" }, "expDate": { "type": "string", "format": "date-time" } } });
            chakram.expect(response).to.contain.json({ "user": { id: 7, role: "EDITOR" } });
            return chakram.wait();
        });

        it('should respond 200 for "user viewer"', function () {
            const response = chakram.request('get', apiUrl + '/users/verifyToken', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": viewerToken },
                'time': true
            });

            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "user": Users ,"token": { "type": "string" }, "expDate": { "type": "string", "format": "date-time" } } });
            chakram.expect(response).to.contain.json({ "user": { id: 8, role: "VIEWER" } });
            return chakram.wait();
        });

        it('should respond 200 for "user common"', function () {
            const response = chakram.request('get', apiUrl + '/users/verifyToken', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken, "x_user_key": commonToken },
                'time': true
            });

            chakram.expect(response).to.have.status(200);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "user": Users ,"token": { "type": "string" }, "expDate": { "type": "string", "format": "date-time" } } });
            chakram.expect(response).to.contain.json({ "user": { id: 2, role: "COMMON" } });
            return chakram.wait();
        });

        // it('should respond 204 for "Response is empty."', function() {
        //     const response = chakram.request('get', apiUrl + '/users/verifyToken', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(204);
        //     chakram.expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

        it('should respond 401 for "without tokens"', function () {
            const response = chakram.request('get', apiUrl + '/users/verifyToken', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json" },
                'time': true
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.token.user" });
            return chakram.wait();
        });

        it('should respond 401 for "app token"', function () {
            const response = chakram.request('get', apiUrl + '/users/verifyToken', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_app_id": apiToken },
                'time': true
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.token.user" });
            return chakram.wait();
        });

        it('should respond 401 for "user token"', function () {
            const response = chakram.request('get', apiUrl + '/users/verifyToken', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json", "x_user_key": adminToken },
                'time': true
            });

            chakram.expect(response).to.have.status(401);
            chakram.expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            chakram.expect(response).to.have.json({ "message": "error.auth.token.app" });
            return chakram.wait();
        });

        // it('should respond 404 for "Entity not found."', function() {
        //     const response = chakram.request('get', apiUrl + '/users/verifyToken', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(404);
        //     chakram.expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

        // it('should respond 405 for "Illegal input for operation."', function() {
        //     const response = chakram.request('get', apiUrl + '/users/verifyToken', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     chakram.expect(response).to.have.status(405);
        //     chakram.expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });
});