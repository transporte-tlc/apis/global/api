'use strict';
var mocha = require('mocha');
var chakram = require('chakram');
var request = chakram.request;
var expect = chakram.expect;

const apiUrl = 'http://localhost:3000';

describe('tests for /journeys/{id}', function() {
    describe('tests for get', function() {
        it('should respond 200 for "Request OK."', function() {
            this.skip();
            var response = request('get', apiUrl + '/journeys/92000589', {
                'qs': {"deleted":"ALL","idUser":-42291394},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(200);
            expect(response).to.have.schema({"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idCarrier":{"type":"integer","format":"int64"},"carrier":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocationStart":{"type":"integer","format":"int64"},"locationStart":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocationEnd":{"type":"integer","format":"int64"},"locationEnd":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"startDate":{"type":"string","format":"date"},"endDate":{"type":"string","format":"date"},"deleted":{"type":"boolean"}}});
            return chakram.wait();
        });


        // it('should respond 204 for "Response is empty."', function() {
        //     var response = request('get', apiUrl + '/journeys/-8615934', {
        //         'qs': {"deleted":"ALL","idUser":-82218290},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(204);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        it('should respond 400 for "Some parameters are missing or badly entered."', function() {
            this.skip();
            var response = request('get', apiUrl + '/journeys/-10212129', {
                'qs': {"deleted":"DELETED","idUser":29340786},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(400);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function() {
            this.skip();
            var response = request('get', apiUrl + '/journeys/-1128991', {
                'qs': {"deleted":"ALL","idUser":54372434},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function() {
        //     var response = request('get', apiUrl + '/journeys/65817938', {
        //         'qs': {"deleted":"NOT-DELETED","idUser":62905304},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(404);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function() {
        //     var response = request('get', apiUrl + '/journeys/12276171', {
        //         'qs': {"deleted":"ALL","idUser":-76206089},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(405);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });

    describe('tests for delete', function() {
        it('should respond 200 for "Request OK."', function() {
            this.skip();
            var response = request('delete', apiUrl + '/journeys/6188772', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(200);
            expect(response).to.have.schema({"type":"object","properties":{"id":{"type":"integer","format":"int64"}}});
            return chakram.wait();
        });


        // it('should respond 204 for "Response is empty."', function() {
        //     var response = request('delete', apiUrl + '/journeys/37390343', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(204);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        it('should respond 400 for "Some parameters are missing or badly entered."', function() {
            this.skip();
            var response = request('delete', apiUrl + '/journeys/-26235724', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(400);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function() {
            this.skip();
            var response = request('delete', apiUrl + '/journeys/39055793', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function() {
        //     var response = request('delete', apiUrl + '/journeys/-96551410', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(404);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function() {
        //     var response = request('delete', apiUrl + '/journeys/51751572', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(405);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });
});