'use strict';
var mocha = require('mocha');
var chakram = require('chakram');
var request = chakram.request;
var expect = chakram.expect;

const apiUrl = 'http://localhost:3000';

describe('tests for /accounts', function () {
    describe('tests for get', function () {
        it('should respond 200 for "Request OK."', function () {
            this.skip();
            var response = request('get', apiUrl + '/accounts', {
                'qs': { "skip": 0, "limit": 5, "orderBy": null, "filter": null, "deleted": "NOT-DELETED", "metadata": true },
                'headers': { "Content-Type": "application/json", "Accept": "application/json" },
                'time': true
            });

            expect(response).to.have.status(200);
            expect(response).to.have.schema({ "properties": { "metadata": { "type": "object", "properties": { "first": { "type": "integer", "format": "int64" }, "prev": { "type": "integer", "format": "int64" }, "self": { "type": "integer", "format": "int64" }, "next": { "type": "integer", "format": "int64" }, "last": { "type": "integer", "format": "int64" } } }, "items": { "type": "array", "items": { "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } } } } });
            return chakram.wait();
        });


        it('should respond 204 for "Response is empty."', function () {
            this.skip();
            var response = request('get', apiUrl + '/accounts', {
                'qs': { "skip": -18494144, "limit": -30863253, "orderBy": "velit", "filter": "minim incididunt nisi tempor", "deleted": "ALL", "metadata": true },
                'headers': { "Content-Type": "application/json", "Accept": "application/json" },
                'time': true
            });

            expect(response).to.have.status(204);
            expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
            return chakram.wait();
        });


        it('should respond 400 for "Some parameters are missing or badly entered."', function () {
            this.skip();
            var response = request('get', apiUrl + '/accounts', {
                'qs': { "skip": -20443233, "limit": -62417706, "orderBy": "veniam ad ex nisi et", "filter": "nisi mollit pariatur", "deleted": "ALL", "metadata": true },
                'headers': { "Content-Type": "application/json", "Accept": "application/json" },
                'time': true
            });

            expect(response).to.have.status(400);
            expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function () {
            this.skip();
            var response = request('get', apiUrl + '/accounts', {
                'qs': { "skip": 97191426, "limit": 31490884, "orderBy": "magna sunt sed", "filter": "ex", "deleted": "DELETED", "metadata": false },
                'headers': { "Content-Type": "application/json", "Accept": "application/json" },
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function () {
        //     var response = request('get', apiUrl + '/accounts', {
        //         'qs': { "skip": -98273847, "limit": -54972228, "orderBy": "laboris pariatur veniam aliqua do", "filter": "consequat", "deleted": "NOT-DELETED", "metadata": false },
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json" },
        //         'time': true
        //     });

        //     expect(response).to.have.status(404);
        //     expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function () {
        //     var response = request('get', apiUrl + '/accounts', {
        //         'qs': { "skip": -57538659, "limit": 68436159, "orderBy": "irure dolore ipsum deserunt ad", "filter": "enim in Excepteur", "deleted": "ALL", "metadata": false },
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json" },
        //         'time': true
        //     });

        //     expect(response).to.have.status(405);
        //     expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
        //     return chakram.wait();
        // });

    });

    describe('tests for post', function () {
        it('should respond 200 for "Request OK."', function () {
            this.skip();
            var response = request('post', apiUrl + '/accounts', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json" },
                'time': true
            });

            expect(response).to.have.status(200);
            expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } });
            return chakram.wait();
        });


        // it('should respond 204 for "Response is empty."', function () {
        //     var response = request('post', apiUrl + '/accounts', {
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json" },
        //         'time': true
        //     });

        //     expect(response).to.have.status(204);
        //     expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
        //     return chakram.wait();
        // });


        it('should respond 400 for "Some parameters are missing or badly entered."', function () {
            this.skip();
            var response = request('post', apiUrl + '/accounts', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json" },
                'time': true
            });

            expect(response).to.have.status(400);
            expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function () {
            this.skip();
            var response = request('post', apiUrl + '/accounts', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json" },
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function () {
        //     var response = request('post', apiUrl + '/accounts', {
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json" },
        //         'time': true
        //     });

        //     expect(response).to.have.status(404);
        //     expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function () {
        //     var response = request('post', apiUrl + '/accounts', {
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json" },
        //         'time': true
        //     });

        //     expect(response).to.have.status(405);
        //     expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
        //     return chakram.wait();
        // });

    });

    describe('tests for put', function () {
        it('should respond 200 for "Request OK."', function () {
            this.skip();
            var response = request('put', apiUrl + '/accounts', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json" },
                'time': true
            });

            expect(response).to.have.status(200);
            expect(response).to.have.schema({ "type": "object", "properties": { "id": { "type": "integer", "format": "int64" }, "name": { "type": "string" }, "deleted": { "type": "boolean" } } });
            return chakram.wait();
        });


        // it('should respond 204 for "Response is empty."', function () {
        //     var response = request('put', apiUrl + '/accounts', {
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json" },
        //         'time': true
        //     });

        //     expect(response).to.have.status(204);
        //     expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
        //     return chakram.wait();
        // });


        it('should respond 400 for "Some parameters are missing or badly entered."', function () {
            this.skip();
            var response = request('put', apiUrl + '/accounts', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json" },
                'time': true
            });

            expect(response).to.have.status(400);
            expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function () {
            this.skip();
            var response = request('put', apiUrl + '/accounts', {
                'headers': { "Content-Type": "application/json", "Accept": "application/json" },
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function () {
        //     var response = request('put', apiUrl + '/accounts', {
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json" },
        //         'time': true
        //     });

        //     expect(response).to.have.status(404);
        //     expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function () {
        //     var response = request('put', apiUrl + '/accounts', {
        //         'headers': { "Content-Type": "application/json", "Accept": "application/json" },
        //         'time': true
        //     });

        //     expect(response).to.have.status(405);
        //     expect(response).to.have.schema({ "type": "object", "required": ["message"], "properties": { "code": { "type": "string" }, "message": { "type": "string" }, "data": { "type": "string" } } });
        //     return chakram.wait();
        // });

    });
});