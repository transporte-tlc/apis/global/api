'use strict';
var mocha = require('mocha');
var chakram = require('chakram');
var request = chakram.request;
var expect = chakram.expect;

const apiUrl = 'http://localhost:3000';

describe('tests for /deliveries', function() {
    describe('tests for get', function() {
        it('should respond 200 for "Request OK."', function() {
            this.skip();
            var response = request('get', apiUrl + '/deliveries', {
                'qs': {"skip":-34122192,"limit":-79308432,"orderBy":"nisi pariatur","filter":"laborum","deleted":"DELETED","metadata":false,"idUser":-20398391,"idAccount":-44818291},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(200);
            expect(response).to.have.schema({"properties":{"metadata":{"type":"object","properties":{"first":{"type":"integer","format":"int64"},"prev":{"type":"integer","format":"int64"},"self":{"type":"integer","format":"int64"},"next":{"type":"integer","format":"int64"},"last":{"type":"integer","format":"int64"}}},"items":{"type":"array","items":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idAccount":{"type":"integer","format":"int64"},"account":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocation":{"type":"integer","format":"int64"},"location":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"issuedAt":{"type":"string"},"issuedBy":{"type":"string"},"reference":{"type":"string"},"quantity":{"type":"integer","format":"int64"},"weight":{"type":"number","format":"float"},"volume":{"type":"number","format":"float"},"value":{"type":"number","format":"float"},"comment":{"type":"string"},"deleted":{"type":"boolean"}}}}}});
            return chakram.wait();
        });


        it('should respond 204 for "Response is empty."', function() {
            this.skip();
            var response = request('get', apiUrl + '/deliveries', {
                'qs': {"skip":94023044,"limit":-53450400,"orderBy":"eu sit tempor deserunt dolore","filter":"nisi consequat","deleted":"ALL","metadata":true,"idUser":45765267,"idAccount":13057121},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(204);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 400 for "Some parameters are missing or badly entered."', function() {
            this.skip();
            var response = request('get', apiUrl + '/deliveries', {
                'qs': {"skip":-84892691,"limit":-37134786,"orderBy":"sit dolore mollit adipisicing","filter":"consectetur cupidatat amet","deleted":"NOT-DELETED","metadata":false,"idUser":-2082452,"idAccount":-87268161},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(400);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function() {
            this.skip();
            var response = request('get', apiUrl + '/deliveries', {
                'qs': {"skip":-37232104,"limit":68160245,"orderBy":"magna ut mollit","filter":"est enim nisi quis","deleted":"ALL","metadata":true,"idUser":48859141,"idAccount":56471345},
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function() {
        //     var response = request('get', apiUrl + '/deliveries', {
        //         'qs': {"skip":90966924,"limit":-47884947,"orderBy":"dolore","filter":"occaecat ut","deleted":"ALL","metadata":false,"idUser":35927038,"idAccount":-89380625},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(404);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function() {
        //     var response = request('get', apiUrl + '/deliveries', {
        //         'qs': {"skip":65243946,"limit":30987901,"orderBy":"do mollit nostrud eiusmod","filter":"dolore mollit nulla anim","deleted":"DELETED","metadata":false,"idUser":-55878569,"idAccount":37207828},
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(405);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });

    describe('tests for post', function() {
        it('should respond 200 for "Request OK."', function() {
            this.skip();
            var response = request('post', apiUrl + '/deliveries', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(200);
            expect(response).to.have.schema({"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idAccount":{"type":"integer","format":"int64"},"account":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocation":{"type":"integer","format":"int64"},"location":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"issuedAt":{"type":"string"},"issuedBy":{"type":"string"},"reference":{"type":"string"},"quantity":{"type":"integer","format":"int64"},"weight":{"type":"number","format":"float"},"volume":{"type":"number","format":"float"},"value":{"type":"number","format":"float"},"comment":{"type":"string"},"deleted":{"type":"boolean"}}});
            return chakram.wait();
        });


        // it('should respond 204 for "Response is empty."', function() {
        //     var response = request('post', apiUrl + '/deliveries', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(204);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        it('should respond 400 for "Some parameters are missing or badly entered."', function() {
            this.skip();
            var response = request('post', apiUrl + '/deliveries', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(400);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function() {
            this.skip();
            var response = request('post', apiUrl + '/deliveries', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function() {
        //     var response = request('post', apiUrl + '/deliveries', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(404);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function() {
        //     var response = request('post', apiUrl + '/deliveries', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(405);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });

    describe('tests for put', function() {
        it('should respond 200 for "Request OK."', function() {
            this.skip();
            var response = request('put', apiUrl + '/deliveries', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(200);
            expect(response).to.have.schema({"type":"object","properties":{"id":{"type":"integer","format":"int64"},"idAccount":{"type":"integer","format":"int64"},"account":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"idLocation":{"type":"integer","format":"int64"},"location":{"type":"object","properties":{"id":{"type":"integer","format":"int64"},"name":{"type":"string"},"deleted":{"type":"boolean"}}},"issuedAt":{"type":"string"},"issuedBy":{"type":"string"},"reference":{"type":"string"},"quantity":{"type":"integer","format":"int64"},"weight":{"type":"number","format":"float"},"volume":{"type":"number","format":"float"},"value":{"type":"number","format":"float"},"comment":{"type":"string"},"deleted":{"type":"boolean"}}});
            return chakram.wait();
        });


        // it('should respond 204 for "Response is empty."', function() {
        //     var response = request('put', apiUrl + '/deliveries', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(204);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        it('should respond 400 for "Some parameters are missing or badly entered."', function() {
            this.skip();
            var response = request('put', apiUrl + '/deliveries', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(400);
            expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
            return chakram.wait();
        });


        it('should respond 401 for "Unauthorized"', function() {
            this.skip();
            var response = request('put', apiUrl + '/deliveries', {
                'headers': {"Content-Type":"application/json","Accept":"application/json"},
                'time': true
            });

            expect(response).to.have.status(401);
            expect(response).to.have.schema({ "type": "object", "properties": { "message": "string" } });
            return chakram.wait();
        });


        // it('should respond 404 for "Entity not found."', function() {
        //     var response = request('put', apiUrl + '/deliveries', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(404);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });


        // it('should respond 405 for "Illegal input for operation."', function() {
        //     var response = request('put', apiUrl + '/deliveries', {
        //         'headers': {"Content-Type":"application/json","Accept":"application/json"},
        //         'time': true
        //     });

        //     expect(response).to.have.status(405);
        //     expect(response).to.have.schema({"type":"object","required":["message"],"properties":{"code":{"type":"string"},"message":{"type":"string"},"data":{"type":"string"}}});
        //     return chakram.wait();
        // });

    });
});