#!/usr/bin/env bash

# Web Page of BASH best practices https://kvz.io/blog/2013/11/21/bash-best-practices/
#Exit when a command fails.
set -o errexit
#Exit when script tries to use undeclared variables.
set -o nounset
#The exit status of the last command that threw a non-zero exit code is returned.
set -o pipefail

#Trace what gets executed. Useful for debugging.
#set -o xtrace

# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"
__root="$(cd "$(dirname "${__dir}")" && pwd)"

if ! type "lftp" &> /dev/null; then
    echo "lftp is not installed. Install it and then re launch"
    exit 1
fi

function usage(){
    echo -e "First Argument: target (dev or pro)"
    echo -e "Secound Argument: domain name"
    echo -e "Third Argument: domain user"
    echo -e "Fourth Argument: domain password"
}

TAG=latest
NAME=registry.gitlab.com/transporte-tlc/api
CI_COMMIT_SHA=112233
if [ $# -ne 4 ]; then
    echo -e "Illegal number of parameters"
    echo -e "$(usage)"
    exit 1
fi

TARGET=
if [ "${1}" == "dev" ]; then
    TARGET="development"
else
    if [ "${1}" == "pro" ]; then
        TARGET="latest"
    else
        echo -e "Invalid first argument"
        echo -e "$(usage)"
        exit 1
    fi
fi

DOMAIN_NAME=${2}
DOMAIN_USER=${3}
DOMAIN_PASS=${4}

DATE="$(date '+%Y-%m-%d--%H:%M:%S')"

# TODO: add env variable, database.json and keys.
lftp -e "set ftps:initial-prot ""; set ftp:ssl-allow false; set ftp:ssl-protect-data true; set ssl:verify-certificate no; mv ${TARGET} old${DATE}; mkdir -p ${TARGET}; cd ${TARGET}; put package.json; lcd src; mkdir src; cd src; mirror --reverse; quit;" \
    -u ${DOMAIN_USER},${DOMAIN_PASS} ${DOMAIN_NAME}