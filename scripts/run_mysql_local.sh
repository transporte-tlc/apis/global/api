#!/usr/bin/env bash

# Web Page of BASH best practices https://kvz.io/blog/2013/11/21/bash-best-practices/
#Exit when a command fails.
set -o errexit
#Exit when script tries to use undeclared variables.
set -o nounset
#The exit status of the last command that threw a non-zero exit code is returned.
set -o pipefail

#Trace what gets executed. Useful for debugging.
#set -o xtrace

# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"
__root="$(cd "$(dirname "${__dir}")" && pwd)"

if ! type "docker" &> /dev/null; then
  echo -ne "Docker is not installed. Install it and then re launch"
  exit 1
fi

HOST=
PORT=3306
PASSWORD=PassWord
CONTAINER_NAME=tcl-mysql-test
IMAGE_NAME=registry.gitlab.com/transporte-tlc/database/mysql:test-develop

function usage(){
    echo "ERROR: no arguments provided."
    echo "-h|--host: configurated host. "
    echo "-p|--password: MySQL password. "
    echo "-P|--port: MySQL port. "
    echo "-n|--name: MySQL image name. "
    exit 1
}

while [[ $# -gt 0 ]]
do
    key="$1"
    case $key in
        -h|--host)
            HOST="${2}"
            shift # past argument
            shift # past value
        ;;
        -p|--password)
            HOST="${2}"
            shift # past argument
            shift # past value
        ;;
        -P|--port)
            HOST="${2}"
            shift # past argument
            shift # past value
        ;;
        -n|--name)
            IMAGE_NAME="${2}"
            CONTAINER_NAME=mysql-custom
            shift # past argument
            shift # past value
        ;;
        *)    # unknown option
            usage
            exit 1
        ;;
    esac
done



if [ "$(docker ps -aq -f name="${CONTAINER_NAME}")" ]; then
    if [ "$(docker ps -aq -f status=exited -f name=${CONTAINER_NAME})" ]; then
        docker start "${CONTAINER_NAME}" > /dev/null 2>&1
    fi
else
    docker run -e "MYSQL_ROOT_PASSWORD=${PASSWORD}" -p ${PORT}:3306 -d \
        --name ${CONTAINER_NAME} ${IMAGE_NAME}
fi


CONTAINER_IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${CONTAINER_NAME})
echo "Waiting port to be open ${CONTAINER_IP}:3306..."
while ! nc -z ${CONTAINER_IP} 3306; do
    sleep 0.5 # wait for 1/10 of the second before check again
done
echo "Container ready"

PORT=$(docker port ${CONTAINER_NAME} | sed 's/^.*://')
PASSWORD=$(docker inspect -f "{{ .Config.Env }}" ${CONTAINER_NAME} | sed 's/^.MYSQL_ROOT_PASSWORD=//' | cut -d ' ' -f1)

echo "Mysql Container running. Port:${PORT}, Name:${CONTAINER_NAME}, Password:${PASSWORD}, IP:${CONTAINER_IP}"