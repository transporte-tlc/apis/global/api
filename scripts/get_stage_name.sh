#!/usr/bin/env bash

# Web Page of BASH best practices https://kvz.io/blog/2013/11/21/bash-best-practices/
#Exit when a command fails.
set -o errexit
#Exit when script tries to use undeclared variables.
set -o nounset
#The exit status of the last command that threw a non-zero exit code is returned.
set -o pipefail

case ${1} in
    master)
        echo "p"
    ;;
    develop)
        echo "s"
    ;;
    *)
        exit 1;
    ;;
esac